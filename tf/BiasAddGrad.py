import os, sys
import json
import tensorflow as tf

I = int(sys.argv[1])
K = int(sys.argv[2])


with tf.device('/gpu:0'):
    IN = tf.constant(value=[(i % 10) + 1 for i in range(0, K*I)], shape=(K, I), dtype=tf.float32, name='IN')
    with tf.profiler.experimental.Profile('../results/tf/BiasAddGrad/profiling_data',
                                          tf.profiler.experimental.ProfilerOptions(
                                              host_tracer_level=0,
                                              python_tracer_level=0,
                                              device_tracer_level=1
                                          )) as profiler:
        # warm ups
        for i in range(0, 10):
            tf.raw_ops.BiasAddGrad(out_backprop=IN)
        # evaluations
        for i in range(0, 200):
            tf.raw_ops.BiasAddGrad(out_backprop=IN)

# read min runtime from trace
timestamp = next(os.walk('../results/tf/BiasAddGrad/profiling_data/plugins/profile/'))[1][0]
trace_file_name = [f for f in os.listdir('../results/tf/BiasAddGrad/profiling_data/plugins/profile/{}'.format(timestamp)) if f.endswith('.trace.json.gz')][0][:-3]
os.system('gunzip ../results/tf/BiasAddGrad/profiling_data/plugins/profile/{}/{}.gz'.format(timestamp, trace_file_name))
min_times = dict()
gpu_tid = None
with open('../results/tf/BiasAddGrad/profiling_data/plugins/profile/{}/{}'.format(timestamp, trace_file_name), 'r') as trace_file:
    trace_json = json.load(trace_file)

    # get execution times and gpu tid
    for event in trace_json['traceEvents']:
        if 'ph' not in event:
            continue

        if event['ph'] == 'M' and event['name'] == 'thread_name' and 'Compute' in event['args']['name']:
            gpu_tid = event['tid']
        if event['ph'] == 'X' and (gpu_tid is None or event['tid'] == gpu_tid):
            if event['tid'] not in min_times.keys():
                min_times[event['tid']] = dict()
            if event['name'] not in min_times[event['tid']].keys():
                min_times[event['tid']][event['name']] = []
            min_times[event['tid']][event['name']].append(int(event['dur'] * 1000))

if gpu_tid is None:
    print('no kernel execution found in GPU trace')
else:
    min_runtime = 0
    for name in min_times[gpu_tid].keys():
        if len(min_times[gpu_tid][name]) > 1:
            min_runtime += min(min_times[gpu_tid][name])

    with open('../results/tf/BiasAddGrad/runtimes_min', 'w') as runtime_file:
        runtime_file.write(str(min_runtime))
