WARNING: Logging before InitGoogleLogging() is written to STDERR
I0822 10:04:25.053920 99230 cuda_tc_executor.cc:105] generatedCuda: 
template<typename T> inline __device__ T floord(T n, T d) {
  return n < 0 ? - (-n + d - 1)/d : n / d;
}
#define if_then_else(cond,a,b) ((cond) ? (a) : (b))

// Halide type handling
typedef int int32;
typedef long int64;
typedef float float32;
typedef double float64;

#define inff __int_as_float(0x7f800000)
#define inf __longlong_as_double(0x7ff0000000000000LL)

// Before CUDA 9, syncwarp is a noop since warps are always synchronized.
#if __CUDACC_VER_MAJOR__ < 9
__device__ void __syncwarp(unsigned mask = 0xFFFFFFFF) {}
#endif

extern "C" {
__global__ void batch_matmul_16_12_384_64_384(int32 B_1, int32 B_2, int32 I, int32 J, int32 K, float32* pC, const float32* pA, const float32* pB) {
  int b0 = blockIdx.x; int b1 = blockIdx.y; int b2 = blockIdx.z;
  int t0 = threadIdx.x; int t1 = threadIdx.y; int t2 = threadIdx.z;
  float32 (*C)[12][384][64] = reinterpret_cast<float32 (*)[12][384][64]>(pC);
  const float32 (*A)[12][384][384] = reinterpret_cast<const float32 (*)[12][384][384]>(pA);
  const float32 (*B)[12][384][64] = reinterpret_cast<const float32 (*)[12][384][64]>(pB);
  __shared__ float32 _C_0[1][1][8][65];
  __shared__ float32 _A_0[1][1][8][385];
  for (int c0 = b0; c0 <= 15; c0 += 4) {
    for (int c1 = b1; c1 <= 11; c1 += 2) {
      __syncthreads();
      _C_0[0][0][0][t0] = C[c0][c1][8 * b2][t0];
      _C_0[0][0][1][t0] = C[c0][c1][8 * b2 + 1][t0];
      _C_0[0][0][2][t0] = C[c0][c1][8 * b2 + 2][t0];
      _C_0[0][0][3][t0] = C[c0][c1][8 * b2 + 3][t0];
      _C_0[0][0][4][t0] = C[c0][c1][8 * b2 + 4][t0];
      _C_0[0][0][5][t0] = C[c0][c1][8 * b2 + 5][t0];
      _C_0[0][0][6][t0] = C[c0][c1][8 * b2 + 6][t0];
      _C_0[0][0][7][t0] = C[c0][c1][8 * b2 + 7][t0];
      for (int c3 = 0; c3 <= 7; c3 += 1) {
        _A_0[0][0][c3][t0] = A[c0][c1][8 * b2 + c3][t0];
        _A_0[0][0][c3][t0 + 64] = A[c0][c1][8 * b2 + c3][t0 + 64];
        _A_0[0][0][c3][t0 + 128] = A[c0][c1][8 * b2 + c3][t0 + 128];
        _A_0[0][0][c3][t0 + 192] = A[c0][c1][8 * b2 + c3][t0 + 192];
        _A_0[0][0][c3][t0 + 256] = A[c0][c1][8 * b2 + c3][t0 + 256];
        _A_0[0][0][c3][t0 + 320] = A[c0][c1][8 * b2 + c3][t0 + 320];
      }
      __syncthreads();
      _C_0[0][0][0][t0] = 0.000000f;
      _C_0[0][0][1][t0] = 0.000000f;
      _C_0[0][0][2][t0] = 0.000000f;
      _C_0[0][0][3][t0] = 0.000000f;
      _C_0[0][0][4][t0] = 0.000000f;
      _C_0[0][0][5][t0] = 0.000000f;
      _C_0[0][0][6][t0] = 0.000000f;
      _C_0[0][0][7][t0] = 0.000000f;
      for (int c4 = 0; c4 <= 383; c4 += 32) {
        for (int c7 = 0; c7 <= 7; c7 += 1) {
          for (int c9 = 0; c9 <= 31; c9 += 1) {
            _C_0[0][0][c7][t0] = (_C_0[0][0][c7][t0] + (_A_0[0][0][c7][c4 + c9]*B[c0][c1][(c4 + c9)][t0]));
          }
        }
      }
      __syncthreads();
      C[c0][c1][8 * b2][t0] = _C_0[0][0][0][t0];
      C[c0][c1][8 * b2 + 1][t0] = _C_0[0][0][1][t0];
      C[c0][c1][8 * b2 + 2][t0] = _C_0[0][0][2][t0];
      C[c0][c1][8 * b2 + 3][t0] = _C_0[0][0][3][t0];
      C[c0][c1][8 * b2 + 4][t0] = _C_0[0][0][4][t0];
      C[c0][c1][8 * b2 + 5][t0] = _C_0[0][0][5][t0];
      C[c0][c1][8 * b2 + 6][t0] = _C_0[0][0][6][t0];
      C[c0][c1][8 * b2 + 7][t0] = _C_0[0][0][7][t0];
      __syncthreads();
    }
  }
}
}

grid: CudaDim(4, 2, 48) @0x7fff3b601ae0 block: CudaDim(64, 1, 1) @0x7fff3b601b20
time:  4467711
