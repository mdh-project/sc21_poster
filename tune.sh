#!/usr/bin/env bash

# change this line, if you want to change the tuning time per framework and routine
TUNING_TIME_IN_MINUTES=1440

: ${ARTIFACT_ROOT?"Please execute \"source environment.sh\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.sh\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

cd $ARTIFACT_ROOT || { echo "Please execute \"source environment.sh\" in the root dir of the artifact (the directory containing the scripts folder) first."; exit 1; }

# MDH (BiasAddGrad)
(
  echo "Tuning MDH BiasAddGrad"
  rm -rf $ARTIFACT_ROOT/results/mdh/BiasAddGrad &> /dev/null
  cd $ARTIFACT_ROOT/mdh/BiasAddGrad/build &&
  ./tune_md_hom_strided_v3_bias_add_grad --path $ARTIFACT_ROOT/results/mdh/BiasAddGrad -d 0 -r no-pp-l-1 -r no-pp-r-1 -r in-cache-l-1 -r in-cache-p-1 -r l-cb-res-p -r p-cb-res-p -r md_poly --input-size-l-1 768 --input-size-r-1 6144 -t OpenTuner -a minutes -v $TUNING_TIME_IN_MINUTES --wrapper-type local --wrapper-command "./tune_md_hom_strided_v3_bias_add_grad [flags]" -g ../gold/md_hom/strided_v3/bias_add_grad/768x6144/gold.tsv --check-type all --kernel-file-1 ../kernel/md_hom/strided_v3/bias_add_grad/static/bias_add_grad_static_1.cuda &&
  source ../scripts/preprocess.sh $ARTIFACT_ROOT/results/mdh/BiasAddGrad/tuned_configuration ../kernel/md_hom/strided_v3/bias_add_grad/static/bias_add_grad_static_1.cuda &&
  sed -i '\;^//;d' ../kernel/md_hom/strided_v3/bias_add_grad/static/bias_add_grad_static_1.cuda_preprocessed.cl &&
  mv ../kernel/md_hom/strided_v3/bias_add_grad/static/bias_add_grad_static_1.cuda_preprocessed.cl $ARTIFACT_ROOT/results/mdh/BiasAddGrad/kernel.cu
)

# MDH (BatchMatMul)
(
  echo "Tuning MDH BatchMatMul"
  rm -rf $ARTIFACT_ROOT/results/mdh/BatchMatMul &> /dev/null
  cd $ARTIFACT_ROOT/mdh/BatchMatMul/build &&
  ./tune_md_hom_strided_v3_batch_matmul --path $ARTIFACT_ROOT/results/mdh/BatchMatMul -d 0 -r no-pp-l-1 -r no-pp-l-2 -r no-pp-l-3 -r no-pp-l-4 -r no-pp-r-1 -r a-cache-l-1 -r b-cache-l-1 -r a-cache-p-1 -r b-cache-p-1 -r l-cb-res-p -r p-cb-res-p -r md_poly --input-size-l-1 16 --input-size-l-2 12 --input-size-l-3 384 --input-size-l-4 64 --input-size-r-1 384 -t OpenTuner -a minutes -v $TUNING_TIME_IN_MINUTES --wrapper-type local --wrapper-command "./tune_md_hom_strided_v3_batch_matmul [flags]" -g ../gold/md_hom/strided_v3/batch_matmul/16x12x384x64x384/gold.tsv --check-type all &&
  source ../scripts/preprocess.sh $ARTIFACT_ROOT/results/mdh/BatchMatMul/tuned_configuration ../kernel/md_hom/strided_v3/batch_matmul/static/batch_matmul_skip_pp_l1_l2_l3_l4_r1_static_1.cuda &&
  sed -i '\;^//;d' ../kernel/md_hom/strided_v3/batch_matmul/static/batch_matmul_skip_pp_l1_l2_l3_l4_r1_static_1.cuda_preprocessed.cl &&
  mv ../kernel/md_hom/strided_v3/batch_matmul/static/batch_matmul_skip_pp_l1_l2_l3_l4_r1_static_1.cuda_preprocessed.cl $ARTIFACT_ROOT/results/mdh/BatchMatMul/kernel.cu
)

# MDH (subgraph)
(
  echo "Tuning MDH subgraph"
  rm -rf $ARTIFACT_ROOT/results/mdh/subgraph &> /dev/null
  cd $ARTIFACT_ROOT/mdh/subgraph/build &&
  ./tune_md_hom_strided_v3_subgraph2 --path $ARTIFACT_ROOT/results/mdh/subgraph -d 0 -r pow2 -r no-pp-l-1 -r no-pp-l-2 --input-size-l-1 16 --input-size-l-2 1 --input-size-l-3 384 --input-size-l-4 384 -t OpenTuner -a minutes -v $TUNING_TIME_IN_MINUTES --wrapper-type local --wrapper-command "./tune_md_hom_strided_v3_subgraph2 [flags]" -g ../gold/md_hom/strided_v3/subgraph2/16x1x384x384/gold.tsv --check-type all &&
  source ../scripts/preprocess.sh $ARTIFACT_ROOT/results/mdh/subgraph/tuned_configuration ../kernel/md_hom/strided_v3/subgraph2/static/subgraph2_skip_pp_l1_l2_static_1.cuda &&
  sed -i '\;^//;d' ../kernel/md_hom/strided_v3/subgraph2/static/subgraph2_skip_pp_l1_l2_static_1.cuda_preprocessed.cl &&
  mv ../kernel/md_hom/strided_v3/subgraph2/static/subgraph2_skip_pp_l1_l2_static_1.cuda_preprocessed.cl $ARTIFACT_ROOT/results/mdh/subgraph/kernel.cu
)

# TC (BiasAddGrad)
(
  echo "Tuning TC BiasAddGrad"
  mkdir -p $ARTIFACT_ROOT/results/tc/BiasAddGrad
  rm $ARTIFACT_ROOT/results/tc/BiasAddGrad/* &> /dev/null
  cd $ARTIFACT_ROOT/tc/BiasAddGrad &&
  START=`date +%s`
  while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
    python tune.py 0 768 6144
  done
  python extract_kernel.py 0 768 6144 &> $ARTIFACT_ROOT/results/tc/BiasAddGrad/kernel_info.txt
)

# TC (BatchMatMul)
(
  echo "Tuning TC BatchMatMul"
  mkdir -p $ARTIFACT_ROOT/results/tc/BatchMatMul
  rm $ARTIFACT_ROOT/results/tc/BatchMatMul/* &> /dev/null
  cd $ARTIFACT_ROOT/tc/BatchMatMul &&
  START=`date +%s`
  while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
    python tune.py 0 16 12 384 64 384
  done
  python extract_kernel.py 0 16 12 384 64 384 &> $ARTIFACT_ROOT/results/tc/BatchMatMul/kernel_info.txt
)

# TC (subgraph)
(
  echo "Tuning TC subgraph"
  mkdir -p $ARTIFACT_ROOT/results/tc/subgraph
  rm $ARTIFACT_ROOT/results/tc/subgraph/* &> /dev/null
  cd $ARTIFACT_ROOT/tc/subgraph &&
  START=`date +%s`
  while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
    python tune.py 0 16 1 384 384
  done
  python extract_kernel.py 0 16 1 384 384 &> $ARTIFACT_ROOT/results/tc/subgraph/kernel_info.txt
)

# TVM (BiasAddGrad)
(
  echo "Tuning TVM BiasAddGrad"
  mkdir -p $ARTIFACT_ROOT/results/tvm/BiasAddGrad
  rm $ARTIFACT_ROOT/results/tvm/BiasAddGrad/* &> /dev/null
  cd $ARTIFACT_ROOT/tvm &&
  START=`date +%s`
  while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
    python BiasAddGrad.py tune 768 6144
  done
)

# TVM (BatchMatMul)
(
  echo "Tuning TVM BatchMatMul"
  mkdir -p $ARTIFACT_ROOT/results/tvm/BatchMatMul
  rm $ARTIFACT_ROOT/results/tvm/BatchMatMul/* &> /dev/null
  cd $ARTIFACT_ROOT/tvm &&
  START=`date +%s`
  while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
    python BatchMatMul.py tune 16 12 384 64 384
  done
)

# TVM (subgraph)
(
  echo "Tuning TVM subgraph"
  mkdir -p $ARTIFACT_ROOT/results/tvm/subgraph
  rm $ARTIFACT_ROOT/results/tvm/subgraph/* &> /dev/null
  cd $ARTIFACT_ROOT/tvm &&
  START=`date +%s`
  while [ $(( $(date +%s) - $TUNING_TIME_IN_MINUTES * 60 )) -lt $START ]; do
    python subgraph.py tune 16 1 384 384
  done
)
