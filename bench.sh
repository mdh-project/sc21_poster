#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.sh\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.sh\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

cd $ARTIFACT_ROOT || { echo "Please execute \"source environment.sh\" in the root dir of the artifact (the directory containing the scripts folder) first."; exit 1; }

# MDH (BiasAddGrad)
(
  echo "Benchmarking MDH BiasAddGrad"
  cd $ARTIFACT_ROOT/mdh/BiasAddGrad/build &&
  ./bench_md_hom_strided_v3_bias_add_grad --path $ARTIFACT_ROOT/results/mdh/BiasAddGrad -d 0 --input-size-l-1 768 --input-size-r-1 6144 -g ../gold/md_hom/strided_v3/bias_add_grad/768x6144/gold.tsv --kernel-file-1 $ARTIFACT_ROOT/results/mdh/BiasAddGrad/kernel.cu --kernel-file-2 $ARTIFACT_ROOT/results/mdh/BiasAddGrad/kernel.cu
)

# MDH (BatchMatMul)
(
  echo "Benchmarking MDH BatchMatMul"
  cd $ARTIFACT_ROOT/mdh/BatchMatMul/build &&
  ./bench_md_hom_strided_v3_batch_matmul --path $ARTIFACT_ROOT/results/mdh/BatchMatMul -d 0 --input-size-l-1 16 --input-size-l-2 12 --input-size-l-3 384 --input-size-l-4 64 --input-size-r-1 384 -g ../gold/md_hom/strided_v3/batch_matmul/16x12x384x64x384/gold.tsv --kernel-file-1 $ARTIFACT_ROOT/results/mdh/BatchMatMul/kernel.cu --kernel-file-2 $ARTIFACT_ROOT/results/mdh/BatchMatMul/kernel.cu
)

# MDH (subgraph)
(
  echo "Benchmarking MDH subgraph"
  cd $ARTIFACT_ROOT/mdh/subgraph/build &&
  ./bench_md_hom_strided_v3_subgraph2 --path $ARTIFACT_ROOT/results/mdh/subgraph -d 0 --input-size-l-1 16 --input-size-l-2 1 --input-size-l-3 384 --input-size-l-4 384 -g ../gold/md_hom/strided_v3/subgraph2/16x1x384x384/gold.tsv --kernel-file-1 $ARTIFACT_ROOT/results/mdh/subgraph/kernel.cu --kernel-file-2 $ARTIFACT_ROOT/results/mdh/subgraph/kernel.cu
)

# TC (BiasAddGrad)
(
  echo "Benchmarking TC BiasAddGrad"
  cd $ARTIFACT_ROOT/tc/BiasAddGrad &&
  ./bench $ARTIFACT_ROOT/results/tc/BiasAddGrad/kernel_info.txt 0 768 6144 | grep -oP "(?<=min runtime: )[0-9]+" > $ARTIFACT_ROOT/results/tc/BiasAddGrad/runtimes_min
)

# TC (BatchMatMul)
(
  echo "Benchmarking TC BatchMatMul"
  cd $ARTIFACT_ROOT/tc/BatchMatMul &&
  ./bench $ARTIFACT_ROOT/results/tc/BatchMatMul/kernel_info.txt 0 16 12 384 64 384 | grep -oP "(?<=min runtime: )[0-9]+" > $ARTIFACT_ROOT/results/tc/BatchMatMul/runtimes_min
)

# TC (subgraph)
(
  echo "Benchmarking TC subgraph"
  cd $ARTIFACT_ROOT/tc/subgraph &&
  ./bench $ARTIFACT_ROOT/results/tc/subgraph/kernel_info.txt 0 16 1 384 384 | grep -oP "(?<=min runtime: )[0-9]+" > $ARTIFACT_ROOT/results/tc/subgraph/runtimes_min
)

# TVM (BiasAddGrad)
(
  echo "Benchmarking TVM BiasAddGrad"
  cd $ARTIFACT_ROOT/tvm &&
  python BiasAddGrad.py bench 768 6144
)

# TVM (BatchMatMul)
(
  echo "Benchmarking TVM BatchMatMul"
  cd $ARTIFACT_ROOT/tvm &&
  python BatchMatMul.py bench 16 12 384 64 384
)

# TVM (subgraph)
(
  echo "Benchmarking TVM subgraph"
  cd $ARTIFACT_ROOT/tvm &&
  python subgraph.py bench 16 1 384 384
)

# TF (BiasAddGrad)
(
  echo "Benchmarking TF BiasAddGrad"
  mkdir -p $ARTIFACT_ROOT/results/tf/BiasAddGrad
  rm -rf $ARTIFACT_ROOT/results/tf/BiasAddGrad/profiling_data &> /dev/null
  cd $ARTIFACT_ROOT/tf &&
  python BiasAddGrad.py 768 6144
)

# TF (subgraph)
(
  echo "Benchmarking TF subgraph"
  mkdir -p $ARTIFACT_ROOT/results/tf/subgraph
  rm -rf $ARTIFACT_ROOT/results/tf/subgraph/profiling_data &> /dev/null
  cd $ARTIFACT_ROOT/tf &&
  python subgraph.py 16 1 384 384
)


# print results
python collect_results.py | tee results/summary.txt