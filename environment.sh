#!/usr/bin/env bash

# artifact settings
export ARTIFACT_ROOT=`pwd`

# Tensor Comprehensions path (set TC_HOME and uncomment these lines, if you built Tensor Comprehensions from source)
# export TC_HOME=
# export PYTHONPATH=$TC_HOME/:$PYTHONPATH
# export LD_LIBRARY_PATH=$TC_HOME/third-party-install/lib:$TC_HOME/third-party-install/lib64:$LD_LIBRARY_PATH

# TVM path (set TVM_HOME and uncomment these lines, if you built TVM from source)
# export TVM_HOME=
# export PYTHONPATH=$TVM_HOME/python:$PYTHONPATH