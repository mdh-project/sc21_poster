import os

import tabulate as tabulate

routines = ['BiasAddGrad', 'BatchMatMul', 'subgraph']
competitors = ['TF', 'TVM', 'TC']

table = []

# header
table.append(
    [None]
)
for routine in routines:
    table[-1].append(routine)
    table[-1].append(None)
table.append(
    [None] + ['Runtime (us)', 'Speedup'] * len(competitors)
)

# MDH row
mdh_row = ['MDH']
mdh_runtimes = dict()
for routine in routines:
    runtime_file_name = 'results/mdh/{routine}/runtimes_min'.format(routine=routine)
    if os.path.isfile(runtime_file_name):
        with open(runtime_file_name, 'r') as runtime_file:
            mdh_runtimes[routine] = round(float(runtime_file.read()) / 1000, 3)
        mdh_row.append(mdh_runtimes[routine])
        mdh_row.append(1.0)
    else:
        mdh_row.append(None)
        mdh_row.append(None)
table.append(mdh_row)

# competitor rows
for competitor in competitors:
    competitor_row = [competitor]
    for routine in routines:
        runtime_file_name = 'results/{competitor}/{routine}/runtimes_min'.format(competitor=competitor.lower(),
                                                                                 routine=routine)
        if os.path.isfile(runtime_file_name):
            with open(runtime_file_name, 'r') as runtime_file:
                runtime = round(float(runtime_file.read()) / 1000, 3)

            competitor_row.append(runtime)
            if routine in mdh_runtimes.keys():
                competitor_row.append(round(runtime / mdh_runtimes[routine], 1))
            else:
                competitor_row.append(None)
        else:
            competitor_row.append(None)
            competitor_row.append(None)
    table.append(competitor_row)

print(tabulate.tabulate(table))