//
//  return_proxy.hpp
//  ocal
//
//  Created by Ari Rasch on 10.10.17.
//  Copyright © 2017 Ari Rasch. All rights reserved.
//

#ifndef return_proxy_hpp
#define return_proxy_hpp

namespace ocal
{

namespace common
{


template< typename buffer_t, typename T >
class return_proxy
{
  public:
    // buffer: buffer that returned the proxy; position: the argument of operator[], i.e., the position that is proxied
    return_proxy( buffer_t& buffer, size_t position )
      : _according_buffer( buffer ), _position( position )
    {}

    // write access: each call of this function has to set the dirty flags for the device buffers
    return_proxy operator=( const T& val )
    {
      // set value in host buffer
      _according_buffer.write_in_host_memory( _position, val );
        
      return *this;
    }
    
    // required for correction
    return_proxy operator=( const return_proxy< buffer_t,T >& other )
    {
      return this->operator=( static_cast<T>( other ) );
    }

    // read access
    operator T() const
    {
      // read value from host memory
      T val;
      _according_buffer.read_from_host_memory( _position, val );
      
      return val;
    }


  private:
    buffer_t& _according_buffer;
    size_t    _position;
};
  
  
} // namespace "common"

} // namespace "ocal"

#endif /* return_proxy_hpp */
