//
//  cuda_device.hpp
//  ocal_cuda
//
//  Created by Ari Rasch on 20.09.17.
//  Copyright © 2017 Ari Rasch. All rights reserved.
//

#ifndef cuda_device_hpp
#define cuda_device_hpp


#include "cuda_helper.hpp"

namespace ocal
{

namespace core
{

namespace cuda
{


class device : public abstract::abstract_device< device, CUfunction_CUmodule_wrapper, cuda_event_manager >
{
  // friend class parent
  friend class abstract::abstract_device< device, CUfunction_CUmodule_wrapper, cuda_event_manager >;

  private:
    // enabling easier access to parent's members
    using abstract::abstract_device< device, CUfunction_CUmodule_wrapper, cuda_event_manager >::_platform_id;
    using abstract::abstract_device< device, CUfunction_CUmodule_wrapper, cuda_event_manager >::_device_id;
    using abstract::abstract_device< device, CUfunction_CUmodule_wrapper, cuda_event_manager >::_kernel;
    using abstract::abstract_device< device, CUfunction_CUmodule_wrapper, cuda_event_manager >::_read_buffer_references_to_wait_for;
    using abstract::abstract_device< device, CUfunction_CUmodule_wrapper, cuda_event_manager >::_write_buffer_references_to_wait_for;
    using abstract::abstract_device< device, CUfunction_CUmodule_wrapper, cuda_event_manager >::_thread_configuration;

  public:
    device( const std::string& device_name )
      : abstract_device< device, CUfunction_CUmodule_wrapper, cuda_event_manager >(), _kernel_arguments_ptr(), _shared_memory_size( 0 )
    {
      CUdevice device = 0; // Note: "= 0" silences warning that device may be uninitialized

      // get device
      int num_devices;
      cuDeviceGetCount( &num_devices );

      for( int i = 0 ; i < num_devices ; ++i )
      {
        // get device
        cuDeviceGet( &device, i );

        // get device name
        char* actual_device_name_as_c_str = new char[ LENGTH_DEVICE_NAME ];
        cuDeviceGetName( actual_device_name_as_c_str, LENGTH_DEVICE_NAME, device );

        std::string actual_device_name( actual_device_name_as_c_str );

        if( actual_device_name == device_name )
        {
          // set device id and break for-loop
          _device_id = i;
          break;
        }
        else if( i == num_devices - 1 )
        {
          std::cout << "Device with name " << device_name << " not found." << std::endl;
          exit( EXIT_FAILURE );
        }
      }

      // if device does not exist then create device
      if( unique_device_id_to_cuda_device.find( this->unique_id() ) == unique_device_id_to_cuda_device.end() )
        unique_device_id_to_cuda_device[ this->unique_id() ] = device;

      // create context
      auto& context = unique_device_id_to_cuda_context[ this->unique_id() ];
      CUDA_SAFE_CALL( cuCtxCreate( &context, 0, device ) );
      current_context_to_unique_device_id[ std::this_thread::get_id() ] = this->unique_id();

      // create cuda streams
      for( int i = 0 ; i < NUM_CUDA_STREAMS_PER_DEVICE ; ++i )
      {
        auto& stream = unique_device_id_to_cuda_streams[ this->unique_id() ][ i ];
        CUDA_SAFE_CALL( cuStreamCreate( &stream, ::CU_STREAM_NON_BLOCKING ) );
      }

      // initialize unique_device_id_to_actual_cuda_stream_id if not already done
      if( unique_device_id_to_actual_cuda_stream_id.find( this->unique_id() ) == unique_device_id_to_actual_cuda_stream_id.end() )
        unique_device_id_to_actual_cuda_stream_id[ this->unique_id() ] = 0;
    }


    device( int device_id = 0 )
      : abstract_device< device, CUfunction_CUmodule_wrapper, cuda_event_manager >(), _kernel_arguments_ptr(), _shared_memory_size( 0 )
    {
      // set device id
      _device_id = device_id;

      // if device alread exists then do nothing
      if( unique_device_id_to_cuda_device.find( this->unique_id() ) != unique_device_id_to_cuda_device.end() )
        return;

      // create device
      CUdevice& device = unique_device_id_to_cuda_device[ this->unique_id() ];

      // get device
      CUDA_SAFE_CALL( cuDeviceGet( &device, device_id ) );

      // create context
      auto& context = unique_device_id_to_cuda_context[ this->unique_id() ];
      CUDA_SAFE_CALL( cuCtxCreate( &context, 0, device ) );
      current_context_to_unique_device_id[ std::this_thread::get_id() ] = this->unique_id();

      // create cuda streams
      for( int i = 0 ; i < NUM_CUDA_STREAMS_PER_DEVICE ; ++i )
      {
        auto& stream = unique_device_id_to_cuda_streams[ this->unique_id() ][ i ];
        CUDA_SAFE_CALL( cuStreamCreate( &stream, ::CU_STREAM_NON_BLOCKING ) );
      }

      // initialize unique_device_id_to_actual_cuda_stream_id if not already done
      if( unique_device_id_to_actual_cuda_stream_id.find( this->unique_id() ) == unique_device_id_to_actual_cuda_stream_id.end() )
        unique_device_id_to_actual_cuda_stream_id[ this->unique_id() ] = 0;
    }


    template< size_t N, typename device_info_t, typename... device_info_val_t >
    device( common::info_class< N, device_info_t, device_info_val_t... > device_infos )
    : abstract_device< device, CUfunction_CUmodule_wrapper, cuda_event_manager >(), _kernel_arguments_ptr(), _shared_memory_size( 0 )
    {
      CUdevice device = 0; // Note: "= 0" silences warning that device may be uninitialized

      // get device
      int num_devices;
      cuDeviceGetCount( &num_devices );

      for( int i = 0 ; i < num_devices ; ++i )
      {
        // get device
        cuDeviceGet( &device, i );

        // get device name
        auto scalar_checker = [&]( CUdevice_attribute info, auto info_val ) // Note: "auto" always resolves to type "int" in case of CUDA
                              {
                                // get device info
                                using info_val_t = decltype( info_val ) ;

                                info_val_t actual_info_val;
                                CUDA_SAFE_CALL( cuDeviceGetAttribute( &actual_info_val, info, device ) );

                                return actual_info_val == info_val;
                              };
        if( !device_infos.check( scalar_checker ) )
          continue; // Note: goes to next possible device

        // set device id and break for-loop
        _device_id = i;

        break; // Note: exits for-loop
      }

      // if device does not exist then create device
      if( unique_device_id_to_cuda_device.find( this->unique_id() ) == unique_device_id_to_cuda_device.end() )
        unique_device_id_to_cuda_device[ this->unique_id() ] = device;

      // create context
      auto& context = unique_device_id_to_cuda_context[ this->unique_id() ];
      CUDA_SAFE_CALL( cuCtxCreate( &context, 0, device ) );
      current_context_to_unique_device_id[ std::this_thread::get_id() ] = this->unique_id();

      // create cuda streams
      for( int i = 0 ; i < NUM_CUDA_STREAMS_PER_DEVICE ; ++i )
      {
        auto& stream = unique_device_id_to_cuda_streams[ this->unique_id() ][ i ];
        CUDA_SAFE_CALL( cuStreamCreate( &stream, ::CU_STREAM_NON_BLOCKING ) );
      }

      // initialize unique_device_id_to_actual_cuda_stream_id if not already done
      if( unique_device_id_to_actual_cuda_stream_id.find( this->unique_id() ) == unique_device_id_to_actual_cuda_stream_id.end() )
        unique_device_id_to_actual_cuda_stream_id[ this->unique_id() ] = 0;
    }
  
  
    // copy ctor instantiates shallow copy
    device( const device& ) = default;
  

    ~device()
    {
    // different ocal::devices can point to the same physical device ( -> the ocal:devices have the same unique_id)
//      // erase device's cuda streams
//      auto streams = unique_device_id_to_cuda_streams.at( this->unique_id() );
//      
//      for( auto& stream : streams )
//        CUDA_SAFE_CALL( cuStreamDestroy( stream.second ) );
//      
//      unique_device_id_to_cuda_streams.erase( this->unique_id() );
//      
//      // erase context
//      auto& context = unique_device_id_to_cuda_context.at( this->unique_id() ];
//      CUDA_SAFE_CALL( cuCtxDestroy( context ) );
//      unique_device_id_to_cuda_context.erase( this->unique_id() );
    }

    // set kernel: case "kernel"
    template< typename T, typename... Ts, typename = std::enable_if_t< std::is_same< kernel, typename ::ocal::common::remove_cv_and_references< T >::type >::value > >
    auto& operator()( T& kernel_object )
    {
      auto& cuda_kernel = kernel_object;
      
      auto& abstract_kernel = static_cast< abstract::abstract_kernel< kernel, CUfunction_CUmodule_wrapper > & >( cuda_kernel );
      abstract::abstract_device< device, CUfunction_CUmodule_wrapper, cuda_event_manager >::operator()( abstract_kernel );
      
      return *this;
    }
  
  
    // set kernel: case "kernel_representation"
    template< typename T, typename... Ts, typename = std::enable_if_t< std::is_base_of< cuda::kernel_representation, typename ::ocal::common::remove_cv_and_references< T >::type >::value ||
                                                                       std::is_base_of< ocl::kernel_representation, typename ::ocal::common::remove_cv_and_references< T >::type >::value
                                                                     > >
    auto& operator()( const T& kernel_representation, const Ts&... args )
    {
      auto cuda_kernel = kernel( kernel_representation, args... );
      
      auto& abstract_kernel = static_cast< abstract::abstract_kernel< kernel, CUfunction_CUmodule_wrapper > & >( cuda_kernel );
      abstract::abstract_device< device, CUfunction_CUmodule_wrapper, cuda_event_manager >::operator()( abstract_kernel );
      
      return *this;
    }
  
  
    // set nd_range: case CUDA
    auto& operator()( dim3 gs, dim3 bs )
    {
      _thread_configuration[ 0 ] = { gs.x(), gs.y(), gs.z() };
      _thread_configuration[ 1 ] = { bs.x(), bs.y(), bs.z() };
      
      return *this;
    }
  
  
    // set nd_range: case OpenCL
    auto& operator()( ocl::nd_range gs, ocl::nd_range ls  )
    {
      this->operator()( core::cuda::dim3{ gs.x() / ls.x(), gs.y() / ls.y(), gs.z() / ls.z() },
                        core::cuda::dim3{ ls.x()         , ls.y()         , ls.z()          }
                      );
      
      return *this;
    }
  
  
    // set kernel's input
    template< typename... Ts >
    auto& operator()( const Ts&... args )
    {
      abstract::abstract_device< device, CUfunction_CUmodule_wrapper, cuda_event_manager >::operator()( args... );  
      
      return *this;
    }


    // set kernel's input -- CUdeviceptr (not managed by OCAL)
    template< typename... Ts >
    auto& operator()( const CUdeviceptr &ptr, const Ts&... args )
    {

      this->set_arg( ptr );

      abstract::abstract_device< device, CUfunction_CUmodule_wrapper, cuda_event_manager >::operator()( args... );

      return *this;
    }
  
  
    void information( CUdevice_attribute info, int* info_val  )
    {
      CUDA_SAFE_CALL( cuDeviceGetAttribute( info_val, info, unique_device_id_to_cuda_device.at( this->unique_id() ) ) );
    }

    void synchronize()
    {
      // synchronize cuda streams
      for( int i = 0 ; i < NUM_CUDA_STREAMS_PER_DEVICE ; ++i )
      {
        auto& stream = unique_device_id_to_cuda_streams[ this->unique_id() ][ i ];
        CUDA_SAFE_CALL( cuStreamSynchronize( stream ) );
      }
    }

    unsigned long long last_runtime()
    {
      CUDA_SAFE_CALL(cuEventSynchronize(_last_event.get()));
      float runtime;
      CUDA_SAFE_CALL(cuEventElapsedTime(&runtime, _last_event_before_kernel_execution.get(), _last_event.get()));
      return runtime * 1000000;
    }


    std::string name()
    {
      char* device_name_as_c_str = new char[ LENGTH_DEVICE_NAME ];
      cuDeviceGetName( device_name_as_c_str, LENGTH_DEVICE_NAME, unique_device_id_to_cuda_device.at( this->unique_id() ) );
      std::string device_name = device_name_as_c_str;
      delete[] device_name_as_c_str;
      return device_name;
    }


    size_t max_threads_per_group()
    {
      int val;
      information(CU_DEVICE_ATTRIBUTE_MAX_THREADS_PER_BLOCK, &val);
      return static_cast<size_t>(val);
    }


    std::vector<size_t> max_group_size()
    {
      std::vector<size_t> vals(3);
      int val;
      information(CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_X, &val);
      vals[0] = static_cast<size_t>(val);
      information(CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_Y, &val);
      vals[1] = static_cast<size_t>(val);
      information(CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_Z, &val);
      vals[2] = static_cast<size_t>(val);
      return vals;
    }


    size_t max_local_or_shared_memory()
    {
      int val;
      information(CU_DEVICE_ATTRIBUTE_MAX_SHARED_MEMORY_PER_BLOCK, &val);
      return static_cast<size_t>(val);
    }
  
  private:
    // ----------------------------------------------------------
    //   static polymorphism
    // ----------------------------------------------------------

    int& unique_device_id_to_actual_command_queue_or_stream_id( int unique_device_id )
    {
      return unique_device_id_to_actual_cuda_stream_id.at( unique_device_id );
    }


    void start_kernel( int command_queue_id, CUevent_wrapper &event )
    {
      // set cuda context
      set_cuda_context( this->unique_id() ); // TODO: really necessary?
      
      auto cuda_stream = unique_device_id_to_cuda_streams.at( this->unique_id() ).at( command_queue_id );
      
      const dim3& gs = _thread_configuration[ 0 ];
      const dim3& bs = _thread_configuration[ 1 ];
      
      
      // start kernel
      _last_event_before_kernel_execution = CUevent_wrapper(this->unique_id(), unique_device_id_to_actual_command_queue_or_stream_id(this->unique_id()), false, true);
      CUDA_SAFE_CALL
      (
        cuLaunchKernel( _kernel,
                        gs.x(), gs.y(), gs.z(),    // grid dim
                        bs.x(), bs.y(), bs.z(),    // block dim
                        _shared_memory_size,       // shared mem and stream
                        cuda_stream,
                        _kernel_arguments_ptr.data(), // arguments
                        0
                      );
      );
      CUDA_SAFE_CALL(cuEventRecord(event.get(), cuda_stream));
      
      // reset shared memory
      _shared_memory_size = 0;
      
      // free allocated memory for fundamental types
      for( auto callback : _kernel_arguments_ptr_delete_callbacks )
        callback();
      
      // clean arugment vectors
      _kernel_arguments_ptr.clear();
      _kernel_arguments_ptr_delete_callbacks.clear();
    }


    template< typename T >
    void set_arg( T arg )
    {
      auto arg_ptr = new T(arg);
      
      _kernel_arguments_ptr.push_back( static_cast<void*>( arg_ptr ) );
      
      _kernel_arguments_ptr_delete_callbacks.push_back( [=](){ delete arg_ptr;} );
    }


    template< typename T >
    void set_arg( shared_buffer<T> shared_buffer ) // TODO: refac "shared_buffer" -> "shared_buffer" for CUDA
    {
      _shared_memory_size += shared_buffer.size();
    }
  
  
    int num_command_queues_or_streams()
    {
      return NUM_CUDA_STREAMS_PER_DEVICE;
    }

     
  private:
    dim3                                 _gs;
    dim3                                 _bs;
    std::vector< void* >                 _kernel_arguments_ptr;
    std::vector< std::function<void()> > _kernel_arguments_ptr_delete_callbacks;
    int                                  _shared_memory_size;
    CUevent_wrapper                      _last_event_before_kernel_execution;
};


} // namespace "cuda"

} // namespace "core"

} // namespace "ocal"



#endif /* cuda_device_hpp */
