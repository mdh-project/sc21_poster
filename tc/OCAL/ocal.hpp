//
//  ocal.hpp
//  
//
//  Created by Ari Rasch on 17/10/16.
//
//

#ifndef ocal_hpp
#define ocal_hpp

//#include <vector>
//#include <array>
//#include <string>
//#include <type_traits>
//#include <unordered_map>
//#include <iostream>
//#include <regex>
//#include <sstream>
//#include <cmath>
//#include <numeric>
//
//#include "assert.h"

#include <string>

// set ocl and cuda part of OCAL as activated
#define OCAL_OCL
#define OCAL_CUDA


#include <cstdlib>
const std::string PATH_TO_OCAL_KERNEL_DB           = (std::getenv( "PATH_TO_OCAL_KERNEL_DB" )          != NULL) ? std::getenv( "PATH_TO_OCAL_KERNEL_DB" )          : ".";
const std::string PATH_TO_OCAL_AUTO_TUNING_SCRIPTS = (std::getenv( "PATH_TO_OCAL_AUTO_TUNING_SCRIPTS") != NULL) ? std::getenv( "PATH_TO_OCAL_AUTO_TUNING_SCRIPTS") : ".";
const std::string PATH_TO_OCAL_KERNEL_CACHE        = (std::getenv( "PATH_TO_OCAL_KERNEL_CACHE")        != NULL) ? std::getenv( "PATH_TO_OCAL_KERNEL_CACHE"       ) : ".";


#include "include/core/ocl/ocl.hpp"
#include "include/core/cuda/cuda.hpp"

#include "include/ocal/ocal.hpp"


// ocal::ocal header
#include "include/ocal/ocal_kernel.hpp"
#include "include/ocal/ocal_buffer.hpp"
#include "include/ocal/ocal_device.hpp"


using namespace ocal::core;

using ocal::common::scalar;
using ocal::common::read;
using ocal::common::write;
using ocal::common::read_write;
using ocal::common::static_parameters;

using OCL  = ocal::core::ocl::device;
using CUDA = ocal::core::cuda::device;

using ocal::core::ocl::nd_range;
using ocal::core::cuda::dim3;


namespace ocal
{
  namespace ocl
  {
    template< typename T >
    size_t NUM_DEVICES( const T& arg )
    {
      return ::ocal::core::ocl::NUM_DEVICES( arg );
    }
  }

  namespace cuda
  {
    const auto NUM_DEVICES = ::ocal::core::cuda::NUM_DEVICES();
  }

} // namespace "ocal"



//using ocal::core::ocl::info; // TODO: was muss hier hin?

#endif /* ocal_hpp */
