#ifndef bash_cf_h
#define bash_cf_h

#include <string>
#include <sstream>
#include <fstream>
#include <chrono>
#include <iostream>

namespace atf
{

namespace cf
{

auto bash(const std::string &script, const std::string &costfile) {
    return [=](configuration &configuration, unsigned long long *compile_time = nullptr, int *error_code = nullptr) {
        for (auto &tp : configuration) {
            std::stringstream ss;
            ss << tp.second;
            setenv(tp.first.c_str(), ss.str().c_str(), 1);
        }
        auto ret = system(script.c_str());
        for (auto &tp : configuration) {
            unsetenv(tp.first.c_str());
        }
        if (ret != 0) {
            if (error_code != nullptr) *error_code = ret;
            throw std::exception();
        }

        std::ifstream cost_in;
        cost_in.open(costfile, std::ifstream::in);
        size_t runtime = 0;
        if (!(cost_in >> runtime)) {
            std::cerr << "could not read runtime from costfile: " << strerror(errno) << std::endl;
            exit(EXIT_FAILURE);
        }
        cost_in.close();

        return runtime;
    };
}

} // namespace cf

} // namespace atf

#endif /* bash_cf_h */
