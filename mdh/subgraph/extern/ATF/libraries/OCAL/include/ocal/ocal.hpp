//
//  ocal.hpp
//  ocal
//
//  Created by Ari Rasch on 04.10.17.
//  Copyright © 2017 Ari Rasch. All rights reserved.
//

#ifndef ocal_h
#define ocal_h


#include <vector>
#include <array>
#include <string>
#include <type_traits>
#include <unordered_map>
#include <iostream>
#include <regex>
#include <sstream>
#include <fstream>
#include <cmath>
#include <numeric>
#include <cstdlib>
#include <memory> 


// helper
#include "../common/helper.hpp"
#include "../common/kernel_argument_specifiers.hpp"
#include "../common/return_proxy.hpp"
#include "../common/static_parameters.hpp"


#endif /* ocal_h */
