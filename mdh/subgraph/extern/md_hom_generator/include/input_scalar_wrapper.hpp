//
// Created by Richard Schulze on 06.11.17.
//

#ifndef MD_BLAS_INPUT_SCALAR_WRAPPER_HPP
#define MD_BLAS_INPUT_SCALAR_WRAPPER_HPP


#include "input_wrapper.hpp"
#include "types.hpp"
#include "macros.hpp"
#include "input_scalar.hpp"

namespace md_hom {
namespace generator {

template<unsigned int L_DIMS, unsigned int R_DIMS>
class input_scalar_wrapper : public input_wrapper {
public:
    input_scalar_wrapper(const input_scalar_class &input_scalar, const macros <L_DIMS, R_DIMS> &macros)
            : _input_scalar(input_scalar), _macros(macros) {

    }

    const std::vector<std::vector<std::string>> &index_functions() const override {
        return _index_functions;
    }

    const std::vector<std::vector<dimension_t>> &used_dimensions() const {
        return _used_dimensions;
    }

    const std::vector<dimension_t> &distinct_used_dimensions() const {
        return _distinct_used_dimensions;
    }

    std::string input_name() const {
        return _input_scalar.name();
    }
    std::string macro_name(unsigned int kernel, int value_id, LEVEL level, char *phase) const {
        return _input_scalar.name();
    }
    std::string call_macro(unsigned int kernel, int value_id, LEVEL level, char *phase,
                           const std::vector<std::string> &parameters) const {
        return _input_scalar.name();
    }
    std::string fu_macro_name(unsigned int kernel, int value_id, LEVEL level, char *phase) const {
        return _input_scalar.name();
    }
    std::string call_fu_macro(unsigned int kernel, int value_id, LEVEL level, char *phase,
                              const std::vector<std::string> &parameters) const {
        return _input_scalar.name();
    }
    bool has_definitions(unsigned int kernel) const {
        return false;
    }
    std::string definitions(unsigned int kernel) const {
        return "";
    }
    bool supports_caching() const {
        return false;
    }

private:
    const input_scalar_class &_input_scalar;
    const macros<L_DIMS, R_DIMS> &_macros;
    const std::vector<std::vector<std::string>> _index_functions;
    const std::vector<std::vector<dimension_t>> _used_dimensions;
    const std::vector<dimension_t> _distinct_used_dimensions;
};

}
}

#endif //MD_BLAS_INPUT_SCALAR_WRAPPER_HPP
