#include "data_util.hpp"

#include <iostream>
#include <sys/stat.h>
#include <cstring>

#include <algorithm>
#include <cctype>
#include <locale>

// trim from start (in place)
static inline void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
        return !std::isspace(ch);
    }));
}

// trim from end (in place)
static inline void rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}

// trim from both ends (in place)
static inline void trim(std::string &s) {
    ltrim(s);
    rtrim(s);
}

// trim from start (copying)
static inline std::string ltrim_copy(std::string s) {
    ltrim(s);
    return s;
}

// trim from end (copying)
static inline std::string rtrim_copy(std::string s) {
    rtrim(s);
}

// trim from both ends (copying)
static inline std::string trim_copy(std::string s) {
    trim(s);
    return s;
}

std::string data_directory(const std::vector<std::string> &parts) {
    auto to_simple_string = [] (const std::string &str) {
        std::string simple_str = trim_copy(str);
        for (char &i : simple_str) {
            if ((i < 48 || i > 57)
                && (i < 65 || i > 90)
                && (i < 97 || i > 122)) {
                i = '_';
            }
        }
        return simple_str;
    };
    std::string path;
    for (const auto &part : parts) {
        if (!path.empty())
            path += "/";
        path += to_simple_string(part);
    }
    return path;
}

void prepare_data_directory(const std::string &path, bool allow_existing) {
    int error = 0;
    size_t start_pos = 0, end_pos;
    while ((end_pos = path.find('/', start_pos)) != std::string::npos) {
        if (start_pos != end_pos) {
            error = mkdir(path.substr(0, end_pos).c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
            if (error != 0 && errno != EEXIST) {
                std::cerr << "error while creating directory \"" + path.substr(0, end_pos) + "\": " << std::strerror(errno) << std::endl;
                exit(EXIT_FAILURE);
            }
        }
        start_pos = end_pos + 1;
    }
    error = mkdir(path.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    if (error != 0 && (!allow_existing || errno != EEXIST)) {
        std::cerr << "error while creating directory \"" + path + "\": " << std::strerror(errno) << std::endl;
        exit(EXIT_FAILURE);
    }
}

bool starts_with(const std::string &value, const std::string &prefix) {
    if (prefix.length() > value.length()) return false;
    for (int i = 0; i < prefix.length(); ++i) {
        if (value[i] != prefix[i]) return false;
    }
    return true;
}

bool ends_with(const std::string &value, const std::string &suffix) {
    if (suffix.length() > value.length()) return false;
    for (int i = 0; i < suffix.length(); ++i) {
        if (value[value.length() - suffix.length() + i] != suffix[i]) return false;
    }
    return true;
}