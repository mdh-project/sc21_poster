//
// Created by Richard Schulze on 06.11.17.
//

#ifndef MD_BLAS_INPUT_BUFFER_WRAPPER_HPP
#define MD_BLAS_INPUT_BUFFER_WRAPPER_HPP

#include "input_wrapper.hpp"
#include "macros.hpp"
#include "types.hpp"

namespace md_hom {
namespace generator {


template<unsigned int L_DIMS, unsigned int R_DIMS>
class input_buffer_wrapper : public input_wrapper {
public:
    input_buffer_wrapper(const input_buffer_class &input_buffer, const macros <L_DIMS, R_DIMS> &macros, bool runtime_inputs, bool cuda)
            : _input_buffer(input_buffer), _macros(macros), _runtime_inputs(runtime_inputs), _cuda(cuda) {
        _simple_buffer = index_functions().size() == 3;
        std::regex naive_pattern("^[lr][1-9][0-9]*$");
        for (int i = 0; i < used_dimensions().size(); ++i) {
            if (!_simple_buffer || used_dimensions()[i].size() != 1 || !std::regex_match(index_functions()[0][i], naive_pattern)) _simple_buffer = false;
            for (const auto dim : used_dimensions()[i]) {
                bool new_dim = true;
                for (const auto used_dim : _distinct_used_dimensions) {
                    if (used_dim.type == dim.type && used_dim.nr == dim.nr) {
                        new_dim = false;
                        break;
                    }
                }
                if (new_dim)
                    _distinct_used_dimensions.push_back(dim);
                if (!new_dim) _simple_buffer = false;
            }
        }
        _distinct_used_dimensions_sorted = sort<L_DIMS, R_DIMS>(_distinct_used_dimensions);

        _macro_name_pattern = "K%d%s_%c_MEM_";
        _macro_name_pattern.append(upper_case(input_buffer.name()));
        _macro_name_pattern.append("_%d");
        _fu_macro_name_pattern = "K%d_FU%s_%c_MEM_";
        _fu_macro_name_pattern.append(upper_case(input_buffer.name()));
        _fu_macro_name_pattern.append("_%d");

        _macro_call_pattern = _macro_name_pattern;
        _macro_call_pattern.append("(");
        for (int i = 0; i < L_DIMS + R_DIMS; ++i) {
            if (i > 0) _macro_call_pattern.append(", ");
            _macro_call_pattern.append("%s");
        }
        _macro_call_pattern.append(")");
        _fu_macro_call_pattern = _fu_macro_name_pattern;
        _fu_macro_call_pattern.append("(");
        for (int i = 0; i < L_DIMS + R_DIMS; ++i) {
            if (i > 0) _fu_macro_call_pattern.append(", ");
            _fu_macro_call_pattern.append("%s");
        }
        _fu_macro_call_pattern.append(")");
    }

    std::string input_name() const {
        return _input_buffer.name();
    }

    std::string replace_vars_in_index_function(unsigned int value_id, unsigned int dim_id, const std::vector<std::string> &params) const {
        std::string index = index_functions()[value_id][dim_id];
        std::smatch match;
        const std::regex dim_regex("([lr])([1-9][0-9]*)");
        while (std::regex_search(index, match, dim_regex)) {
            index = match.prefix().str() + params[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(match[1].str() == "l" ? L(std::atoi(match[2].str().c_str())) : R(std::atoi(match[2].str().c_str())))] + match.suffix().str();
        }
        const std::regex dim_size_regex("([LR])([1-9][0-9]*)");
        while (std::regex_search(index, match, dim_size_regex)) {
            index = match.prefix().str() + stringf("INPUT_SIZE_%s_%s", match[1].str(), match[2].str()) + match.suffix().str();
        }
        return index;
    }

    std::vector<std::string> replace_vars_in_index_function(const std::vector<unsigned int> &value_id, const std::vector<unsigned int> &dim_id, const std::vector<std::vector<std::string>> &params) const {
        std::vector<std::string> indices;
        if (value_id.empty() || dim_id.empty() || params.empty()) return indices;
        for (size_t i = 0; i < std::max(value_id.size(), std::max(dim_id.size(), params.size())); ++i) {
            indices.push_back(replace_vars_in_index_function(value_id[std::min(i, value_id.size() - 1)], dim_id[std::min(i, dim_id.size() - 1)], params[std::min(i, params.size() - 1)]));
        }
        return indices;
    }

    std::string replace_vars_in_global_index_function(unsigned int value_id, unsigned int dim_id, const std::vector<std::string> &params) const {
        std::string index = _input_buffer.global_index_functions()[value_id][dim_id];
        std::smatch match;
        const std::regex dim_index_regex("([lr])([1-9][0-9]*)");
        while (std::regex_search(index, match, dim_index_regex)) {
            index = match.prefix().str() + params[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(match[1].str() == "l" ? L(std::atoi(match[2].str().c_str())) : R(std::atoi(match[2].str().c_str())))] + match.suffix().str();
        }
        const std::regex dim_size_regex("([LR])([1-9][0-9]*)");
        while (std::regex_search(index, match, dim_size_regex)) {
            index = match.prefix().str() + stringf("INPUT_SIZE_%s_%s", match[1].str(), match[2].str()) + match.suffix().str();
        }
        return index;
    }

    std::vector<std::string> replace_vars_in_global_index_function(const std::vector<unsigned int> &value_id, const std::vector<unsigned int> &dim_id, const std::vector<std::vector<std::string>> &params) const {
        std::vector<std::string> indices;
        if (value_id.empty() || dim_id.empty() || params.empty()) return indices;
        for (size_t i = 0; i < std::max(value_id.size(), std::max(dim_id.size(), params.size())); ++i) {
            indices.push_back(replace_vars_in_global_index_function(value_id[std::min(i, value_id.size() - 1)], dim_id[std::min(i, dim_id.size() - 1)], params[std::min(i, params.size() - 1)]));
        }
        return indices;
    }

    std::string replace_vars_in_size_function(unsigned int dim_id, const std::vector<std::string> &params) const {
        std::string size = _input_buffer.global_dimension_sizes()[dim_id];
        std::smatch match;
        const std::regex dim_index_regex("([lr])([1-9][0-9]*)");
        while (std::regex_search(size, match, dim_index_regex)) {
            size = match.prefix().str() + params[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(match[1].str() == "l" ? L(std::atoi(match[2].str().c_str())) : R(std::atoi(match[2].str().c_str())))] + match.suffix().str();
        }
        const std::regex dim_size_regex("([LR])([1-9][0-9]*)");
        while (std::regex_search(size, match, dim_size_regex)) {
            size = match.prefix().str() + stringf("INPUT_SIZE_%s_%s", match[1].str(), match[2].str()) + match.suffix().str();
        }
        return size;
    }

    const std::vector<std::vector<std::string>> &index_functions() const override {
        return _input_buffer.index_functions();
    }

    const std::vector<std::vector<dimension_t>> &used_dimensions() const override {
        return _input_buffer.used_dimensions();
    }

    const std::vector<dimension_t> &distinct_used_dimensions() const override {
        return _distinct_used_dimensions;
    }

    std::string macro_name(unsigned int kernel, int value_id, LEVEL level, char *phase) const {
        std::string p3_prefix = "";
        for (const auto &dim : _distinct_used_dimensions_sorted) {
            bool dim_in_phase3 = false;
            bool my_level_in_phase3 = false;
            for (LEVEL p_level : parent_levels(level, true)) {
                if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(p_level, dim)] == 3) {
                    dim_in_phase3 = true;
                    break;
                }
            }
            my_level_in_phase3 = phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(level, dim)] == 3;

            if (dim_in_phase3) {
                if (my_level_in_phase3) {
                    p3_prefix += "_P3";
                } else {
                    p3_prefix += "_PP3";
                }
                if (level == LEVEL::PRIVATE && phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dim)] == 2) {
                    p3_prefix += stringf("_IN_COMPLETE_%c_CB", parent_level(parent_level(level)));
                }
                p3_prefix += stringf("_%c_%d", dim.type, dim.nr);
            }
        }
        return stringf(_macro_name_pattern, kernel, p3_prefix, level, value_id);
    }

    std::string call_macro(unsigned int kernel, int value_id, LEVEL level, char *phase,
                           const std::vector<std::string> &parameters) const {
        std::string p3_prefix = "";
        for (const auto &dim : _distinct_used_dimensions_sorted) {
            bool dim_in_phase3 = false;
            bool my_level_in_phase3 = false;
            for (LEVEL p_level : parent_levels(level, true)) {
                if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(p_level, dim)] == 3) {
                    dim_in_phase3 = true;
                    break;
                }
            }
            my_level_in_phase3 = phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(level, dim)] == 3;

            if (dim_in_phase3) {
                if (my_level_in_phase3) {
                    p3_prefix += "_P3";
                } else {
                    p3_prefix += "_PP3";
                }
                if (level == LEVEL::PRIVATE && phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dim)] == 2) {
                    p3_prefix += stringf("_IN_COMPLETE_%c_CB", parent_level(parent_level(level)));
                }
                p3_prefix += stringf("_%c_%d", dim.type, dim.nr);
            }
        }
        std::string call = stringf_p(_macro_call_pattern, kernel, p3_prefix, level, value_id);
        for (auto &param : parameters) {
            call = stringf_p(call, param);
        }
        return call;
    }

    std::string fu_macro_name(unsigned int kernel, int value_id, LEVEL level, char *phase) const {
        std::string p3_prefix = "";
        for (const auto &dim : _distinct_used_dimensions_sorted) {
            bool dim_in_phase3 = false;
            bool my_level_in_phase3 = false;
            for (LEVEL p_level : parent_levels(level, true)) {
                if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(p_level, dim)] == 3) {
                    dim_in_phase3 = true;
                    break;
                }
            }
            my_level_in_phase3 = phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(level, dim)] == 3;

            if (dim_in_phase3) {
                if (my_level_in_phase3) {
                    p3_prefix += "_P3";
                } else {
                    p3_prefix += "_PP3";
                }
                if (level == LEVEL::PRIVATE && phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dim)] == 2) {
                    p3_prefix += stringf("_IN_COMPLETE_%c_CB", parent_level(parent_level(level)));
                }
                p3_prefix += stringf("_%c_%d", dim.type, dim.nr);
            }
        }
        return stringf(_fu_macro_name_pattern, kernel, p3_prefix, level, value_id);
    }

    std::string call_fu_macro(unsigned int kernel, int value_id, LEVEL level, char *phase,
                              const std::vector<std::string> &parameters) const {
        std::string p3_prefix = "";
        for (const auto &dim : _distinct_used_dimensions_sorted) {
            bool dim_in_phase3 = false;
            bool my_level_in_phase3 = false;
            for (LEVEL p_level : parent_levels(level, true)) {
                if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(p_level, dim)] == 3) {
                    dim_in_phase3 = true;
                    break;
                }
            }
            my_level_in_phase3 = phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(level, dim)] == 3;

            if (dim_in_phase3) {
                if (my_level_in_phase3) {
                    p3_prefix += "_P3";
                } else {
                    p3_prefix += "_PP3";
                }
                if (level == LEVEL::PRIVATE && phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dim)] == 2) {
                    p3_prefix += stringf("_IN_COMPLETE_%c_CB", parent_level(parent_level(level)));
                }
                p3_prefix += stringf("_%c_%d", dim.type, dim.nr);
            }
        }
        std::string call = stringf_p(_fu_macro_call_pattern, kernel, p3_prefix, level, value_id);
        for (auto &param : parameters) {
            call = stringf_p(call, param);
        }
        return call;
    }

    bool has_definitions(unsigned int kernel) const {
        return true;
    }

    std::string definitions(unsigned int kernel) const {
        std::stringstream ss;
        ss << stringf("// -------------------- buffer %s --------------------",
                      upper_case(_input_buffer.name())).c_str() << std::endl;
        ss << std::endl;
        if (kernel == 2) {
            ss << concat(multi_stringf("#define BUFFER_%s_DIM_%d_ORDER OCL_DIM_%c_%d\n", V(upper_case(_input_buffer.name())), uint_range(0, L_DIMS + R_DIMS - 1), dim_range_types(L_DIMS, R_DIMS), dim_range_nrs(L_DIMS, R_DIMS))) << std::endl;
        }
        ss << "// buffer abstraction" << std::endl;
        for (int i = 0; i < used_dimensions().size(); ++i) {
            if (used_dimensions()[i].empty()) continue;
            ss << permutation_cases<dimension_t, unsigned int>(
                    used_dimensions()[i],
                    [&](const dimension_t &variable) -> std::string {
                        return stringf("OCL_DIM_%c_%d", variable.type, variable.nr);
                    },
                    uint_range(0, L_DIMS + R_DIMS - 1),
                    [&](const unsigned int &option) -> std::string {
                        return std::to_string(option);
                    },
                    [&](unsigned int nesting_level, const dimension_t &variable, const unsigned int &option, bool shortcut_macro, const std::vector<std::pair<dimension_t, unsigned int>> &path) -> std::string {
                        if (shortcut_macro) {
                            return stringf(
                                    "#define %s_DIM_%d_OCL_DIM_ORDER_%d(%s) (%s)",
                                    upper_case(_input_buffer.name()), i, option,
                                    make_standard_params(used_dimensions()[i], L_DIMS, R_DIMS),
                                    make_standard_indices(V(variable), L_DIMS, R_DIMS).front()
                            );
                        } else {
                            return stringf(
                                    "#define %s_DIM_%d_OCL_DIM_ORDER_%d(%s) (%s)",
                                    upper_case(_input_buffer.name()), i, used_dimensions()[i].size() - 1 - path.size(),
                                    make_standard_params(used_dimensions()[i], L_DIMS, R_DIMS),
                                    make_standard_indices(V(variable), L_DIMS, R_DIMS).front()
                            );
                        }
                    }
            ).c_str() << std::endl;
        }
        int num_flat_indexing_components = 0;
        for (int i = 0; i < used_dimensions().size(); ++i) {
            num_flat_indexing_components += used_dimensions()[i].size() + !_simple_buffer;
        }
        std::vector<std::string> flat_indexing_params = make_standard_indices(num_flat_indexing_components);
        ss << permutation_cases<unsigned int, unsigned int>(
                uint_range(0, used_dimensions().size() - 1),
                [&](const unsigned int &variable) -> std::string {
                    return stringf("BUFFER_%s_DIM_%d_ORDER", upper_case(_input_buffer.name()), variable);
                },
                uint_range(0, _simple_buffer ? L_DIMS + R_DIMS - 1 : used_dimensions().size() - 1),
                [&](const unsigned int &option) -> std::string {
                    return std::to_string(option);
                },
                [&](unsigned int nesting_level, const unsigned int &variable, const unsigned int &option, bool shortcut_macro, const std::vector<std::pair<unsigned int, unsigned int>> &path) -> std::string {
                    unsigned int macro_id = used_dimensions().size() - 1 - path.size();
                    if (shortcut_macro)
                        macro_id = option;
                    std::string inner_code = stringf(
                            "#define BUFFER_%s_INDEX_%d(%s) (%s)",
                            upper_case(_input_buffer.name()), macro_id,
                            make_standard_params(used_dimensions().size()),
                            make_standard_indices(used_dimensions().size())[variable]);
                    for (LEVEL level : {LEVEL::GLOBAL, LEVEL::LOCAL, LEVEL::PRIVATE}) {
                        inner_code.append(stringf(
                                "\n#define BUFFER_%s_%c_SIZE_%d (%s)",
                                upper_case(_input_buffer.name()), level, macro_id,
                                !_simple_buffer
                                ? stringf("((%s) - (%s) + 1)%s%s",
                                          replace_vars_in_index_function(index_functions().size() - 1,
                                                                         variable,
                                                                         level == LEVEL::PRIVATE
                                                                         ? repeat<std::string>("(1 - 1)", L_DIMS + R_DIMS)
                                                                         : multi_stringf("(%s - 1)",
                                                                                         multi_stringf("GET_%s_SIZE_%c_%d", V(upper_case(long_level(level))), dim_range_types(L_DIMS, R_DIMS), dim_range_nrs(L_DIMS, R_DIMS))
                                                                         )
                                          ),
                                          replace_vars_in_index_function(0, variable, repeat<std::string>("0", L_DIMS + R_DIMS)),
                                          used_dimensions()[variable].empty() ? "" : " * ",
                                          concat(_macros.num_cached_iterations(V(kernel), V(level), used_dimensions()[variable]), " * ")
                                )
                                : ((kernel == 2 && level == LEVEL::GLOBAL && distinct_used_dimensions()[variable].type == DIM_TYPE::R) ? stringf("NUM_WG_R_%d", distinct_used_dimensions()[variable].nr) : _macros.cb_size(kernel, level, distinct_used_dimensions()[variable]))));
                    }

                    int start_index = 0;
                    std::vector<std::string> flat_indexing_sub_params;
                    for (int i = 0; i < variable; ++i) {
                        start_index += used_dimensions()[i].size() + !_simple_buffer;
                    }
                    for (int i = start_index; i < start_index + used_dimensions()[variable].size() + !_simple_buffer; ++i) {
                        flat_indexing_sub_params.push_back(flat_indexing_params[i]);
                    }
                    int num_flat_indexing_components_start = num_flat_indexing_components;
                    if (shortcut_macro) {
                        num_flat_indexing_components_start = option + 1;
                    } else {
                        for (const auto &val : path) {
                            num_flat_indexing_components_start -= used_dimensions()[val.first].size() + !_simple_buffer;
                        }
                    }
                    for (int i = 0; i < used_dimensions()[variable].size() - _simple_buffer; ++i) {
                        inner_code.append(stringf(
                                "\n#define BUFFER_%s_FLAT_INDEXING_%d(%s) %s_DIM_%d_OCL_DIM_ORDER_%d(%s)",
                                upper_case(_input_buffer.name()), num_flat_indexing_components_start - 1 - i,
                                concat(flat_indexing_params, ", "),
                                upper_case(_input_buffer.name()), variable, used_dimensions()[variable].size() - 1 - i,
                                concat(std::vector<std::string>(flat_indexing_sub_params.begin(), flat_indexing_sub_params.begin() + flat_indexing_sub_params.size() - 1), ", ")
                        ));
                    }
                    inner_code.append(stringf(
                            "\n#define BUFFER_%s_FLAT_INDEXING_%d(%s) %s",
                            upper_case(_input_buffer.name()), num_flat_indexing_components_start - 1 - used_dimensions()[variable].size() + _simple_buffer,
                            concat(flat_indexing_params, ", "),
                            flat_indexing_sub_params.back()
                    ));

                    return inner_code;
                }, _simple_buffer).c_str() << std::endl;
        for (LEVEL level : {LEVEL::GLOBAL, LEVEL::LOCAL, LEVEL::PRIVATE}) {
            for (int dbl_buf = (level == LEVEL::GLOBAL ? 0 : 1); dbl_buf >= 0; --dbl_buf) {
                if (level != LEVEL::GLOBAL) {
                    if (dbl_buf) {
                        ss << stringf("#if (defined(%s_CACHE_%c_CB) && %s_CACHE_%c_CB == 2) || (!defined(%s_CACHE_%c_CB) && CACHE_%c_CB == 2)",
                                      upper_case(_input_buffer.name()), upper_case(level),
                                      upper_case(_input_buffer.name()), upper_case(level),
                                      upper_case(_input_buffer.name()), upper_case(level),
                                      upper_case(level)) << std::endl;
                    } else {
                        ss << "#else" << std::endl;
                    }
                }

                for (const auto& suffix : dbl_buf ? V<std::string>("_WRITE", "_READ") : V<std::string>("")) {
                    ss << "#define "
                       << stringf("K%d_%c_BUFFER_%s%s(%s)", kernel, level, upper_case(_input_buffer.name()), suffix,
                                  make_standard_params(used_dimensions().size()))
                       << " " << (level == LEVEL::GLOBAL ? _input_buffer.name() + "_buf" : stringf("cb_%c_%s%s", lower_case(level), _input_buffer.name(), lower_case(suffix)));

                    if (kernel == 1 && level == LEVEL::GLOBAL) {
                        if (_cuda) {
                            ss << concat(wrap(make_standard_indices(used_dimensions().size()), "[", "]"));
                        } else {
                            std::vector<std::string> sizes;
                            for (int i = 0; i < used_dimensions().size(); ++i) {
                                if (_input_buffer.global_dimension_sizes().size() > i && !_input_buffer.global_dimension_sizes()[i].empty()) {
                                    sizes.emplace_back(stringf("%s", replace_vars_in_size_function(i, _macros.cb_size(V(kernel), V(LEVEL::GLOBAL), dim_range(L_DIMS, R_DIMS)))));
                                } else if (!_simple_buffer) {
                                    sizes.emplace_back(stringf("(%s) - (%s) + 1",
                                                               replace_vars_in_global_index_function(index_functions().size() - 1, i, multi_stringf("(%s - 1)", _macros.cb_size(V(kernel), V(LEVEL::GLOBAL), dim_range(L_DIMS, R_DIMS)))),
                                                               replace_vars_in_global_index_function(0, i, repeat<std::string>("0", L_DIMS + R_DIMS))));
                                } else {
                                    sizes.emplace_back(_macros.cb_size(kernel, LEVEL::GLOBAL, used_dimensions()[i][0]));
                                }
                                sizes.back() = stringf("(%s)", sizes.back());
                            }
                            ss << wrap({make_flat_index(make_standard_indices(used_dimensions().size()), sizes)}, "[", "]")[0];
                        }
                    } else if (kernel == 2 && level == LEVEL::GLOBAL) {
                        if (_cuda)
                            ss << concat(wrap(multi_stringf("BUFFER_%s_INDEX_%d(%s)", V(upper_case(_input_buffer.name())), uint_range(used_dimensions().size() - 1, 0), V(make_standard_params(used_dimensions().size()))), "[", "]"));
                        else
                            ss << wrap({make_flat_index(multi_stringf("BUFFER_%s_INDEX_%d(%s)", V(upper_case(_input_buffer.name())), uint_range(used_dimensions().size() - 1, 0), V(make_standard_params(used_dimensions().size()))),
                                                        multi_stringf("BUFFER_%s_%c_SIZE_%d", V(upper_case(_input_buffer.name())), V(level), uint_range(used_dimensions().size() - 1, 0))
                            )}, "[", "]")[0];
                    } else {
                        ss << stringf(concat(wrap(multi_stringf("BUFFER_%s_INDEX_%d(%s)", V(upper_case(_input_buffer.name())), uint_range(used_dimensions().size() - 1, 0), V(make_standard_params(used_dimensions().size()))), "[(", ")]")));
                    }
                    ss << std::endl;
                }
            }
            if (level != LEVEL::GLOBAL) {
                ss << "#endif" << std::endl;
            }
        }
        ss << std::endl;
        ss << "// partitioning and cache usage" << std::endl;
        for (int value_id = 1; value_id < index_functions().size() - 1; ++value_id) {
            for (LEVEL level : {LEVEL::GLOBAL, LEVEL::LOCAL, LEVEL::PRIVATE}) {
                for (int buffering_type = (level == LEVEL::GLOBAL ? 0 : 2); buffering_type >= 0; --buffering_type) {
                    if (level != LEVEL::GLOBAL) {
                        ss << stringf("%s (defined(%s_CACHE_%c_CB) && %s_CACHE_%c_CB == %d) || (!defined(%s_CACHE_%c_CB) && CACHE_%c_CB == %d)",
                                      buffering_type == 2 ? "#if" : "#elif",
                                      upper_case(_input_buffer.name()), upper_case(level),
                                      upper_case(_input_buffer.name()), upper_case(level),
                                      buffering_type,
                                      upper_case(_input_buffer.name()), upper_case(level),
                                      upper_case(level),
                                      buffering_type) << std::endl;
                    }

                    if (buffering_type == 0) {
                        auto complete_dim_range = dim_range(L_DIMS, R_DIMS);
                        iterate_all_phase_combinations_for_index_conversion<L_DIMS, R_DIMS>(
                                level,
                                distinct_used_dimensions(),
                                [&](char *phase) {
                                    if (level == LEVEL::GLOBAL) {
                                        ss << stringf("#define %s(%s) ",
                                                      macro_name(kernel, value_id, level, phase),
                                                      make_standard_params(L_DIMS + R_DIMS)).c_str()
                                           << stringf("K%d_%c_BUFFER_%s(%s)", kernel, level, upper_case(_input_buffer.name()),
                                                      concat(replace_vars_in_global_index_function(V<unsigned int>(value_id), uint_range(0, _input_buffer.global_index_functions()[1].size() - 1), V(make_standard_indices(L_DIMS + R_DIMS))), ", "))
                                           << std::endl;
                                        ss << stringf("#define %s(%s) ",
                                                      fu_macro_name(kernel, value_id, level, phase),
                                                      make_standard_params(L_DIMS + R_DIMS)).c_str()
                                           << stringf("K%d_%c_BUFFER_%s(%s)", kernel, level, upper_case(_input_buffer.name()),
                                                      concat(replace_vars_in_global_index_function(V<unsigned int>(value_id), uint_range(0, _input_buffer.global_index_functions()[1].size() - 1), V(make_standard_indices(L_DIMS + R_DIMS))), ", "))
                                           << std::endl;
                                    } else {
                                        ss << stringf("#define %s(%s) ",
                                                      macro_name(kernel, value_id, level, phase),
                                                      make_standard_params(L_DIMS + R_DIMS)).c_str()
                                           << stringf("%s(%s)",
                                                      macro_name(kernel, value_id, parent_level(level), phase),
                                                      concat(_macros.index_conversion(V(kernel),
                                                                                      V(level),
                                                                                      complete_dim_range,
                                                                                      make_standard_indices(L_DIMS + R_DIMS),
                                                                                      V(phase)
                                                      ), ", "))
                                           << std::endl;
                                        ss << stringf("#define %s(%s) ",
                                                      fu_macro_name(kernel, value_id, level, phase),
                                                      make_standard_params(L_DIMS + R_DIMS)).c_str()
                                           << stringf("%s(%s)",
                                                      fu_macro_name(kernel, value_id, parent_level(level), phase),
                                                      concat(_macros.fu_index_conversion(V(kernel),
                                                                                         V(level),
                                                                                         complete_dim_range,
                                                                                         make_standard_indices(L_DIMS + R_DIMS),
                                                                                         V(phase)
                                                      ), ", "))
                                           << std::endl;
                                    }
                                });
                    } else {
                        for (const auto& suffix : buffering_type == 2 ? V<std::string>("_READ") : V<std::string>("")) {
                            std::vector<std::string> fu_sizes;
                            if (level == LEVEL::PRIVATE) {
                                fu_sizes = repeat<std::string>("1", L_DIMS + R_DIMS);
                            } else {
                                fu_sizes = multi_stringf("GET_%s_SIZE_%c_%d", V(upper_case(long_level(level))), dim_range_types(L_DIMS, R_DIMS), dim_range_nrs(L_DIMS, R_DIMS));
                            }
                            std::vector<std::string> offsets;
                            for (int i = 0; i < used_dimensions().size(); ++i) {
                                if (used_dimensions()[i].empty()) {
                                    offsets.push_back("0");
                                } else {
                                    offsets.push_back(make_flat_index(
                                            multi_stringf("%s_DIM_%d_OCL_DIM_ORDER_%d(%s)",
                                                          V(upper_case(_input_buffer.name())), V(i), uint_range(used_dimensions()[i].size() - 1, 0),
                                                          V(concat(make_standard_indices(used_dimensions()[i], L_DIMS, R_DIMS, "(%%val%% / %s)",
                                                                                         level == LEVEL::PRIVATE
                                                                                         ? repeat<std::string>("1", L_DIMS + R_DIMS)
                                                                                         : multi_stringf("GET_%s_SIZE_%c_%d", V(upper_case(long_level(level))), dim_range_types(L_DIMS, R_DIMS), dim_range_nrs(L_DIMS, R_DIMS))
                                                          ), ", "))),
                                            multi_stringf("%s_DIM_%d_OCL_DIM_ORDER_%d(%s)",
                                                          V(upper_case(_input_buffer.name())), V(i), uint_range(used_dimensions()[i].size() - 1, 0),
                                                          V(concat(_macros.num_cached_iterations(V(kernel), V(level), sort<L_DIMS, R_DIMS>(used_dimensions()[i])), ", "))
                                            )
                                    ));
                                }
                            }
                            iterate_all_phase_combinations_for_index_conversion<L_DIMS, R_DIMS>(
                                    level,
                                    distinct_used_dimensions(),
                                    [&](char *phase) {
                                        ss << stringf("#define %s(%s) ",
                                                      macro_name(kernel, value_id, level, phase),
                                                      make_standard_params(L_DIMS + R_DIMS)).c_str()
                                           << stringf("K%d_%c_BUFFER_%s%s(%s)", kernel, level, upper_case(_input_buffer.name()), suffix,
                                                      _simple_buffer
                                                      ? concat(replace_vars_in_index_function(V<unsigned int>(value_id), uint_range(0, used_dimensions().size() - 1), V(make_standard_indices(L_DIMS + R_DIMS))), ", ")
                                                      : concat(multi_stringf("((%s) * ((%s) - (%s) + 1)) + ((%s) - (%s))",
                                                                             offsets,
                                                                             replace_vars_in_index_function(V<unsigned int>(index_functions().size() - 1), uint_range(0, used_dimensions().size() - 1), V(multi_stringf("(%s - 1)", fu_sizes))),
                                                                             replace_vars_in_index_function(V<unsigned int>(0), uint_range(0, used_dimensions().size() - 1), V(repeat<std::string>("0", L_DIMS + R_DIMS))),
                                                                             replace_vars_in_index_function(V<unsigned int>(value_id), uint_range(0, used_dimensions().size() - 1), V(make_standard_indices(L_DIMS + R_DIMS, "(%%val%% %% %s)", fu_sizes))),
                                                                             replace_vars_in_index_function(V<unsigned int>(0), uint_range(0, used_dimensions().size() - 1), V(repeat<std::string>("0", L_DIMS + R_DIMS)))
                                                      ), ", "))
                                           << std::endl;
                                        ss << stringf("#define %s(%s) ",
                                                      fu_macro_name(kernel, value_id, level, phase),
                                                      make_standard_params(L_DIMS + R_DIMS)).c_str()
                                           << stringf("K%d_%c_BUFFER_%s%s(%s)", kernel, level, upper_case(_input_buffer.name()), suffix,
                                                      _simple_buffer
                                                      ? concat(replace_vars_in_index_function(V<unsigned int>(value_id), uint_range(0, used_dimensions().size() - 1), V(make_standard_indices(L_DIMS + R_DIMS))), ", ")
                                                      : concat(multi_stringf("((%s) * ((%s) - (%s) + 1)) + ((%s) - (%s))",
                                                                             offsets,
                                                                             replace_vars_in_index_function(V<unsigned int>(index_functions().size() - 1), uint_range(0, used_dimensions().size() - 1), V(multi_stringf("(%s - 1)", fu_sizes))),
                                                                             replace_vars_in_index_function(V<unsigned int>(0), uint_range(0, used_dimensions().size() - 1), V(repeat<std::string>("0", L_DIMS + R_DIMS))),
                                                                             replace_vars_in_index_function(V<unsigned int>(value_id), uint_range(0, used_dimensions().size() - 1), V(make_standard_indices(L_DIMS + R_DIMS, "(%%val%% %% %s)", fu_sizes))),
                                                                             replace_vars_in_index_function(V<unsigned int>(0), uint_range(0, used_dimensions().size() - 1), V(repeat<std::string>("0", L_DIMS + R_DIMS)))
                                                      ), ", "))
                                           << std::endl;
                                    });
                        }
                    }
                }
                if (level != LEVEL::GLOBAL) {
                    ss << "#endif" << std::endl;
                }
            }
        }

        return ss.str();
    }

    bool supports_caching() const {
        return true;
    }

    std::string caching_variable_declaration(unsigned int kernel, LEVEL level) const {
        std::stringstream ss;

        if (level == LEVEL::GLOBAL) {
            if (_cuda) {
                if (kernel == 1) {
                    std::vector<std::string> sizes;
                    for (int i = 1; i < used_dimensions().size(); ++i) {
                        if (_input_buffer.global_dimension_sizes().size() > i && !_input_buffer.global_dimension_sizes()[i].empty()) {
                            sizes.emplace_back(stringf("%s", replace_vars_in_size_function(i, _macros.cb_size(V(kernel), V(LEVEL::GLOBAL), dim_range(L_DIMS, R_DIMS)))));
                        } else if (!_simple_buffer) {
                            sizes.emplace_back(stringf("(%s) - (%s) + 1",
                                                       replace_vars_in_global_index_function(index_functions().size() - 1, i, multi_stringf("(%s - 1)", _macros.cb_size(V(kernel), V(LEVEL::GLOBAL), dim_range(L_DIMS, R_DIMS)))),
                                                       replace_vars_in_global_index_function(0, i, repeat<std::string>("0", L_DIMS + R_DIMS))));
                        } else {
                            sizes.emplace_back(_macros.cb_size(kernel, LEVEL::GLOBAL, used_dimensions()[i][0]));
                        }
                    }
                    ss << stringf(
                            "%s const (*%s_buf)%s = reinterpret_cast<%s const (*)%s>(%s_buf_raw);",
                            _input_buffer.data_type(),
                            _input_buffer.name(),
                            concat(wrap(sizes, "[", "]")),
                            _input_buffer.data_type(),
                            concat(wrap(sizes, "[", "]")),
                            _input_buffer.name()
                    ) << std::endl;
                } else {
                    ss << stringf(
                            "%s const (*%s_buf)%s = reinterpret_cast<%s const (*)%s>(%s_buf_raw);",
                            _input_buffer.data_type(),
                            _input_buffer.name(),
                            used_dimensions().size() > 1 ? concat(wrap(multi_stringf("BUFFER_%s_%c_SIZE_%d", V(upper_case(_input_buffer.name())), V(level), uint_range(used_dimensions().size() - 2, 0)), "[", "]")) : "",
                            _input_buffer.data_type(),
                            used_dimensions().size() > 1 ? concat(wrap(multi_stringf("BUFFER_%s_%c_SIZE_%d", V(upper_case(_input_buffer.name())), V(level), uint_range(used_dimensions().size() - 2, 0)), "[", "]")) : "",
                            _input_buffer.name()
                    ) << std::endl;
                }
            } else {
                ss << stringf(
                        "__global %s const * const restrict %s_buf = %s_buf_raw;",
                        _input_buffer.data_type(),
                        _input_buffer.name(),
                        _input_buffer.name()
                ) << std::endl;
            }
        } else {
            for (int dbl_buf = 1; dbl_buf >= 0; --dbl_buf) {
                if (dbl_buf) {
                    ss << stringf("#if (defined(%s_CACHE_%c_CB) && %s_CACHE_%c_CB == 2) || (!defined(%s_CACHE_%c_CB) && CACHE_%c_CB == 2)",
                                  upper_case(_input_buffer.name()), upper_case(level),
                                  upper_case(_input_buffer.name()), upper_case(level),
                                  upper_case(_input_buffer.name()), upper_case(level),
                                  upper_case(level)) << std::endl;
                } else {
                    ss << "#else" << std::endl;
                }

                for (const auto& suffix : dbl_buf ? V<std::string>("_0", "_1") : V<std::string>("")) {
                    ss << stringf(
                            "%s%s cb_%c_%s%s%s;",
                            level == LEVEL::LOCAL ? (_cuda ? "__shared__ " : "__local ") : (_cuda ? "" : "__private "),
                            _input_buffer.data_type(),
                            lower_case(level),
                            _input_buffer.name(),
                            suffix,
                            concat(wrap(multi_stringf("BUFFER_%s_%c_SIZE_%d", V(upper_case(_input_buffer.name())), V(level), uint_range(used_dimensions().size() - 1, 0)), "[", "]"))
                    ) << std::endl;
                }
                if (dbl_buf) {
                    ss << stringf(
                            "%s%s (*cb_%c_%s_write)%s;",
                            level == LEVEL::LOCAL ? (_cuda ? "__shared__ " : "__local ") : (_cuda ? "" : "__private "),
                            _input_buffer.data_type(),
                            lower_case(level),
                            _input_buffer.name(),
                            used_dimensions().size() < 2 ? "" : concat(wrap(multi_stringf("BUFFER_%s_%c_SIZE_%d", V(upper_case(_input_buffer.name())), V(level), uint_range(used_dimensions().size() - 2, 0)), "[", "]"))
                    ) << std::endl;
                    ss << stringf(
                            "cb_%c_%s_write = cb_%c_%s_1;",
                            lower_case(level),
                            _input_buffer.name(),
                            lower_case(level),
                            _input_buffer.name()
                    ) << std::endl;
                    ss << stringf(
                            "%s%s (*cb_%c_%s_read)%s;",
                            level == LEVEL::LOCAL ? (_cuda ? "__shared__ " : "__local ") : (_cuda ? "" : "__private "),
                            _input_buffer.data_type(),
                            lower_case(level),
                            _input_buffer.name(),
                            used_dimensions().size() < 2 ? "" : concat(wrap(multi_stringf("BUFFER_%s_%c_SIZE_%d", V(upper_case(_input_buffer.name())), V(level), uint_range(used_dimensions().size() - 2, 0)), "[", "]"))
                    ) << std::endl;
                    ss << stringf(
                            "cb_%c_%s_read = cb_%c_%s_0;",
                            lower_case(level),
                            _input_buffer.name(),
                            lower_case(level),
                            _input_buffer.name()
                    ) << std::endl;
                }
            }
            ss << "#endif" << std::endl;
        }

        return ss.str();
    }

    std::string caching_copy_code(unsigned int kernel, LEVEL level, char *phase, std::map<dimension_t, std::vector<LEVEL>> &skipped_loops, bool dbl_buffering) const {
        std::vector<std::string> loop_indices = make_standard_indices(used_dimensions().size());
        std::string copy_body;
        if (_simple_buffer) {
            std::vector<std::string> indices = multi_stringf("%c_dim_%d_index_%c_%d", V(lower_case(level)), uint_range(0, used_dimensions().size() - 1), lower_case(split_dim_range(_distinct_used_dimensions).first), split_dim_range(_distinct_used_dimensions).second);
            std::vector<std::string> filled_offset_indices;
            for (const auto &dim : dim_range(L_DIMS, R_DIMS)) {
                if (contains(distinct_used_dimensions(), dim)) {
                    int i = 0;
                    for (; i < used_dimensions().size(); ++i) if (contains(used_dimensions()[i], dim)) break;
                    filled_offset_indices.push_back(_macros.index_conversion(kernel, level, {dim}, stringf("%c_dim_%d_index_%c_%d", lower_case(level), i, lower_case(dim.type), dim.nr), phase));
                } else {
                    filled_offset_indices.push_back("0");
                }
            }

            // assemble copy code
            copy_body = stringf("\nK%d_%c_BUFFER_%s%s(%s) = %s;",
                                kernel, level, upper_case(_input_buffer.name()), dbl_buffering ? "_WRITE" : "", concat(indices, ", "),
                                call_macro(kernel, 1, parent_level(level), phase, filled_offset_indices)
            );
#ifdef PRINT_CACHING_DEBUG_INDIVIDUAL_ELEMENTS
            std::string printf_code;
            auto buffer_offset_indices = filled_offset_indices;
            for (int i = buffer_offset_indices.size() - 1; i >= 0; --i) {
                if (!contains(distinct_used_dimensions(), dim_range(L_DIMS, R_DIMS)[i])) {
                    buffer_offset_indices.erase(buffer_offset_indices.begin() + i);
                }
            }
            printf_code = stringf(
                    "\nDEBUG_PRINT(\"cb_%c_%s%%s(=%%%%16p)%s = %%s(=%%%%16p)%s (== %%%%4.0f)\\n\", cb_%c_%s%%s, %s, %%s, %s, %s);",
                    lower_case(level), lower_case(_input_buffer.name()),
                    concat(wrap(repeat(std::string("%%2lu"), indices.size()), "[", "]")),
                    concat(wrap(repeat(std::string("%%2lu"), buffer_offset_indices.size()), "[", "]")),
                    lower_case(level), lower_case(_input_buffer.name()),
                    concat(multi_stringf("BUFFER_%s_INDEX_%d(%s)", V(upper_case(_input_buffer.name())), uint_range(used_dimensions().size() - 1, 0), V(concat(indices, ", "))), ", "),
                    concat(buffer_offset_indices, ", "),
                    call_macro(kernel, 1, parent_level(level), phase, filled_offset_indices)
            );
            std::string debug_code;
            if (level > LEVEL::LOCAL) {
                for (const auto p_level : level_range(parent_level(level), LEVEL::LOCAL)) {
                    debug_code.append(stringf("\n%s (defined(%s_CACHE_%c_CB) && %s_CACHE_%c_CB == 2) || (!defined(%s_CACHE_%c_CB) && CACHE_%c_CB == 2)",
                                              debug_code.empty() ? "#if" : "#elif",
                                              upper_case(_input_buffer.name()), p_level, upper_case(_input_buffer.name()), p_level,
                                              upper_case(_input_buffer.name()), p_level, p_level
                    ));
                    debug_code.append(stringf(printf_code, dbl_buffering ? "_write" : "", stringf("cb_%c_%s_read", lower_case(p_level), lower_case(_input_buffer.name())), dbl_buffering ? "_write" : "", stringf("cb_%c_%s_read", lower_case(p_level), lower_case(_input_buffer.name()))));
                    debug_code.append(stringf("\n%s (defined(%s_CACHE_%c_CB) && %s_CACHE_%c_CB == 1) || (!defined(%s_CACHE_%c_CB) && CACHE_%c_CB == 1)",
                                              debug_code.empty() ? "#if" : "#elif",
                                              upper_case(_input_buffer.name()), p_level, upper_case(_input_buffer.name()), p_level,
                                              upper_case(_input_buffer.name()), p_level, p_level
                    ));
                    debug_code.append(stringf(printf_code, dbl_buffering ? "_write" : "", stringf("cb_%c_%s", lower_case(p_level), lower_case(_input_buffer.name())), dbl_buffering ? "_write" : "", stringf("cb_%c_%s", lower_case(p_level), lower_case(_input_buffer.name()))));
                }
                debug_code.append("\n#else");
            }
            debug_code.append(stringf(printf_code, dbl_buffering ? "_write" : "", stringf("      %s_buf", lower_case(_input_buffer.name())), dbl_buffering ? "_write" : "", stringf("%s_buf", lower_case(_input_buffer.name()))));
            if (level > LEVEL::LOCAL)
                debug_code.append("\n#endif");
            copy_body.append("\n\n// debug printing").append(debug_code);
#endif
        } else {
            std::vector<std::string> fu_sizes;
            if (level == LEVEL::PRIVATE) {
                fu_sizes = make_standard_indices(L_DIMS + R_DIMS, "1");
            } else {
                fu_sizes = multi_stringf("GET_%s_SIZE_%c_%d", V(upper_case(long_level(level))), dim_range_types(L_DIMS, R_DIMS), dim_range_nrs(L_DIMS, R_DIMS));
            }
            copy_body = stringf("\nK%d_%c_BUFFER_%s%s(%s) =",
                                 kernel, level, upper_case(_input_buffer.name()), dbl_buffering ? "_WRITE" : "",
                                 concat(multi_stringf("%c_mem_offset_dim_%d + %s", V(lower_case(level)), uint_range(0, used_dimensions().size() - 1), loop_indices), ", ")
            );
            for (const auto p_level : parent_levels(level, false, ASCENDING)) {
                if (p_level == LEVEL::GLOBAL) {
                    copy_body.append(stringf("%s\n    K%d_%c_BUFFER_%s(%s);",
                                             level == LEVEL::LOCAL ? "" : "\n#else",
                                             kernel, p_level, upper_case(_input_buffer.name()),
                                             concat(multi_stringf("%c_mem_offset_dim_%d + %s", V(lower_case(p_level)), uint_range(0, used_dimensions().size() - 1), loop_indices), ", ")));
                } else {
                    copy_body.append(stringf("%s\n    K%d_%c_BUFFER_%s_READ(%s);",
                                             level == LEVEL::LOCAL ? "" : stringf("\n#%s (defined(%s_CACHE_%c_CB) && %s_CACHE_%c_CB == 2) || (!defined(%s_CACHE_%c_CB) && CACHE_%c_CB == 2)",
                                                                                  p_level == parent_level(level) ? "if" : "elif",
                                                                                  upper_case(_input_buffer.name()), p_level, upper_case(_input_buffer.name()), p_level,
                                                                                  upper_case(_input_buffer.name()), p_level, p_level),
                                             kernel, p_level, upper_case(_input_buffer.name()),
                                             concat(multi_stringf("%c_mem_offset_dim_%d + %s", V(lower_case(p_level)), uint_range(0, used_dimensions().size() - 1), loop_indices), ", ")
                    ));
                    copy_body.append(stringf("%s\n    K%d_%c_BUFFER_%s(%s);",
                                             level == LEVEL::LOCAL ? "" : stringf("\n#elif (defined(%s_CACHE_%c_CB) && %s_CACHE_%c_CB == 1) || (!defined(%s_CACHE_%c_CB) && CACHE_%c_CB == 1)",
                                                                                  upper_case(_input_buffer.name()), p_level, upper_case(_input_buffer.name()), p_level,
                                                                                  upper_case(_input_buffer.name()), p_level, p_level),
                                             kernel, p_level, upper_case(_input_buffer.name()),
                                             concat(multi_stringf("%c_mem_offset_dim_%d + %s", V(lower_case(p_level)), uint_range(0, used_dimensions().size() - 1), loop_indices), ", ")
                    ));
                }
            }
            if (level != LEVEL::LOCAL) copy_body.append("\n#endif");
#ifdef PRINT_CACHING_DEBUG_INDIVIDUAL_ELEMENTS
            copy_body.append("\n\n// debug printing");
            std::string printf_code = stringf(
                    "DEBUG_PRINT(\"cb_%c_%s%%s(=%%%%16p)%s = %%s(=%%%%16p)%s (== %%%%4.0f)\\n\", cb_%c_%s%%s, %s, %%s, %%s, K%d_%%c_BUFFER_%s%%s(%%s));",
                    lower_case(level), lower_case(_input_buffer.name()),
                    concat(wrap(repeat(std::string("%%2lu"), used_dimensions().size()), "[", "]")),
                    concat(wrap(repeat(std::string("%%2lu"), used_dimensions().size()), "[", "]")),
                    lower_case(level), lower_case(_input_buffer.name()),
                    concat(multi_stringf("%c_mem_offset_dim_%d + %s", V(lower_case(level)), uint_range(0, used_dimensions().size() - 1), loop_indices), ", "),
                    kernel, upper_case(_input_buffer.name())
            );
            for (const auto p_level : parent_levels(level, false, ASCENDING)) {
                if (p_level == LEVEL::GLOBAL) {
                    copy_body.append(stringf("%s\n    %s",
                                             level == LEVEL::LOCAL ? "" : "\n#else",
                                             stringf(printf_code,
                                                     dbl_buffering ? "_write" : "",
                                                     stringf("%s_buf", lower_case(_input_buffer.name())),
                                                     dbl_buffering ? "_write" : "",
                                                     stringf("%s_buf", lower_case(_input_buffer.name())),
                                                     concat(multi_stringf("%c_mem_offset_dim_%d + %s", V(lower_case(p_level)), uint_range(0, used_dimensions().size() - 1), loop_indices), ", "),
                                                     p_level, "", concat(multi_stringf("%c_mem_offset_dim_%d + %s", V(lower_case(p_level)), uint_range(0, used_dimensions().size() - 1), loop_indices), ", "))));
                } else {
                    copy_body.append(stringf("%s\n    %s",
                                             level == LEVEL::LOCAL ? "" : stringf("\n#%s (defined(%s_CACHE_%c_CB) && %s_CACHE_%c_CB == 2) || (!defined(%s_CACHE_%c_CB) && CACHE_%c_CB == 2)",
                                                                                  p_level == parent_level(level) ? "if" : "elif",
                                                                                  upper_case(_input_buffer.name()), p_level, upper_case(_input_buffer.name()), p_level,
                                                                                  upper_case(_input_buffer.name()), p_level, p_level),
                                             stringf(printf_code,
                                                     dbl_buffering ? "_write" : "",
                                                     stringf("cb_%c_%s_read", lower_case(p_level), lower_case(_input_buffer.name())),
                                                     dbl_buffering ? "_write" : "",
                                                     stringf("cb_%c_%s_read", lower_case(p_level), lower_case(_input_buffer.name())),
                                                     concat(multi_stringf("%c_mem_offset_dim_%d + %s", V(lower_case(p_level)), uint_range(0, used_dimensions().size() - 1), loop_indices), ", "),
                                                     p_level, "_READ", concat(multi_stringf("%c_mem_offset_dim_%d + %s", V(lower_case(p_level)), uint_range(0, used_dimensions().size() - 1), loop_indices), ", "))));
                    copy_body.append(stringf("%s\n    %s",
                                             level == LEVEL::LOCAL ? "" : stringf("\n#elif (defined(%s_CACHE_%c_CB) && %s_CACHE_%c_CB == 1) || (!defined(%s_CACHE_%c_CB) && CACHE_%c_CB == 1)",
                                                                                  upper_case(_input_buffer.name()), p_level, upper_case(_input_buffer.name()), p_level,
                                                                                  upper_case(_input_buffer.name()), p_level, p_level),
                                             stringf(printf_code,
                                                     dbl_buffering ? "_write" : "",
                                                     stringf("cb_%c_%s", lower_case(p_level), lower_case(_input_buffer.name())),
                                                     dbl_buffering ? "_write" : "",
                                                     stringf("cb_%c_%s", lower_case(p_level), lower_case(_input_buffer.name())),
                                                     concat(multi_stringf("%c_mem_offset_dim_%d + %s", V(lower_case(p_level)), uint_range(0, used_dimensions().size() - 1), loop_indices), ", "),
                                                     p_level, "", concat(multi_stringf("%c_mem_offset_dim_%d + %s", V(lower_case(p_level)), uint_range(0, used_dimensions().size() - 1), loop_indices), ", "))));
                }
            }
            if (level != LEVEL::LOCAL) copy_body.append("\n#endif");
#endif

            std::string offset_code;
            for (int i = 0; i < used_dimensions().size(); ++i) {
                if (used_dimensions()[i].empty()) {
                    offset_code.append(stringf("size_t %c_mem_offset_dim_%d = 0;\n",
                                               lower_case(level), i
                    ));
                } else {
                    offset_code.append(stringf("size_t %c_mem_offset_dim_%d = (%s) * ((%s) - (%s) + 1);\n",
                                               lower_case(level), i,
                                               make_flat_index(
                                                       multi_stringf("%s_DIM_%d_OCL_DIM_ORDER_%d(%s)",
                                                                     V(upper_case(_input_buffer.name())), V(i), uint_range(used_dimensions()[i].size() - 1, 0),
                                                                     V(concat(multi_stringf("%c_dim_%d_index_%c_%d", V(lower_case(level)), V(i), lower_case(split_dim_range(sort<L_DIMS, R_DIMS>(used_dimensions()[i])).first), split_dim_range(sort<L_DIMS, R_DIMS>(used_dimensions()[i])).second), ", "))),
                                                       multi_stringf("%s_DIM_%d_OCL_DIM_ORDER_%d(%s)",
                                                                     V(upper_case(_input_buffer.name())), V(i), uint_range(used_dimensions()[i].size() - 1, 0),
                                                                     V(concat(_macros.num_cached_iterations(V(kernel), V(level), sort<L_DIMS, R_DIMS>(used_dimensions()[i])), ", "))
                                                       )
                                               ),
                                               replace_vars_in_index_function(index_functions().size() - 1, i, multi_stringf("(%s - 1)", fu_sizes)),
                                               replace_vars_in_index_function(0, i, repeat<std::string>("0", L_DIMS + R_DIMS))
                    ));
                }
            }
            offset_code.append("#if defined(CACHE_CB_OFFSET) && CACHE_CB_OFFSET != 0\n");
            for (const auto p_level : parent_levels(level, false, ASCENDING)) {
                if (level != LEVEL::LOCAL) {
                    if (p_level == LEVEL::GLOBAL) {
                        offset_code.append("#else\n");
                    } else {
                        offset_code.append(stringf("%s (defined(%s_CACHE_%c_CB) && %s_CACHE_%c_CB != 0) || (!defined(%s_CACHE_%c_CB) && CACHE_%c_CB != 0)\n",
                                                   p_level == parent_level(level) ? "#if" : "#elif",
                                                   upper_case(_input_buffer.name()), p_level, upper_case(_input_buffer.name()), p_level,
                                                   upper_case(_input_buffer.name()), p_level, p_level
                        ));
                    }
                }
                std::vector<std::vector<std::string>> indices(_input_buffer.global_index_functions()[0].size());
                for (int i = 0; i < indices.size(); ++i) {
                    indices[i] = multi_stringf("%c_dim_%d_index_%c_%d * %s", V(lower_case(level)), V(i), lower_case(dim_range_types(L_DIMS, R_DIMS)), dim_range_nrs(L_DIMS, R_DIMS), _macros.num_fu(V(kernel), V(level), dim_range(L_DIMS, R_DIMS)));
                    for (const auto c_level : level_range(level, sub_level(p_level))) {
                        indices[i] = _macros.index_conversion(V(kernel), V(c_level), dim_range(L_DIMS, R_DIMS), indices[i], V(phase));
                    }
                }
                for (int i = 0; i < indices.size(); ++i) {
                    offset_code.append(stringf("size_t %c_mem_offset_dim_%d = %s;\n",
                                               lower_case(p_level), i,
                                               replace_vars_in_global_index_function(0, i, indices[i])
                    ));
                }
            }
            if (level != LEVEL::LOCAL) offset_code.append("#endif\n");
            offset_code.append("#else\n");
            for (const auto p_level : parent_levels(level, false, ASCENDING)) {
                for (int p_level_dbl_buf = level == LEVEL::LOCAL || p_level == LEVEL::GLOBAL ? 0 : 1; p_level_dbl_buf >= 0; --p_level_dbl_buf) {
                    if (level != LEVEL::LOCAL) {
                        if (p_level == LEVEL::GLOBAL) {
                            offset_code.append("#else\n");
                        } else {
                            offset_code.append(stringf("%s (defined(%s_CACHE_%c_CB) && %s_CACHE_%c_CB == %d) || (!defined(%s_CACHE_%c_CB) && CACHE_%c_CB == %d)\n",
                                                       p_level == LEVEL::LOCAL && p_level_dbl_buf ? "#if" : "#elif",
                                                       upper_case(_input_buffer.name()), p_level, upper_case(_input_buffer.name()), p_level,
                                                       p_level_dbl_buf ? 2 : 1,
                                                       upper_case(_input_buffer.name()), p_level, p_level,
                                                       p_level_dbl_buf ? 2 : 1
                            ));
                        }
                    }
                    if (p_level == LEVEL::GLOBAL) {
                        std::vector<std::vector<std::string>> indices(_input_buffer.global_index_functions()[0].size());
                        for (int i = 0; i < indices.size(); ++i) {
                            indices[i] = multi_stringf("%c_dim_%d_index_%c_%d * %s", V(lower_case(level)), V(i), lower_case(dim_range_types(L_DIMS, R_DIMS)), dim_range_nrs(L_DIMS, R_DIMS), _macros.num_fu(V(kernel), V(level), dim_range(L_DIMS, R_DIMS)));
                            for (const auto c_level : level_range(level, sub_level(p_level))) {
                                indices[i] = _macros.index_conversion(V(kernel), V(c_level), dim_range(L_DIMS, R_DIMS), indices[i], V(phase));
                            }
                        }
                        for (int i = 0; i < indices.size(); ++i) {
                            offset_code.append(stringf("size_t %c_mem_offset_dim_%d = %s;\n",
                                                       lower_case(p_level), i,
                                                       replace_vars_in_global_index_function(0, i, indices[i])
                            ));
                        }
                    } else {
                        for (int i = 0; i < used_dimensions().size(); ++i) {
                            if (used_dimensions()[i].empty()) {
                                offset_code.append(stringf("size_t %c_mem_offset_dim_%d = 0;\n",
                                                           lower_case(p_level), i
                                ));
                            } else {
                                std::map<dimension_t, std::string> offsets;
                                for (const auto &dim : sort<L_DIMS, R_DIMS>(used_dimensions()[i])) {
                                    if (skipped_loops.find(dim) == skipped_loops.end() || std::find(skipped_loops[dim].begin(), skipped_loops[dim].end(), level) == skipped_loops[dim].end()) {
                                        if (dbl_buffering) {
                                            offsets[dim] = "0";
                                            for (const auto c_level : level_range(level, sub_level(p_level))) {
                                                offsets[dim] = _macros.index_conversion(kernel, c_level, dim, offsets[dim], phase);
                                            }
                                            offsets[dim] = stringf("%s / %s", offsets[dim], _macros.num_fu(kernel, parent_level(level), dim));
                                        } else {
                                            offsets[dim] = stringf("%c_cb_offset_%c_%d / %s", lower_case(level), lower_case(dim.type), dim.nr, _macros.num_fu(kernel, parent_level(level), dim));
                                        }
                                    } else {
                                        offsets[dim] = "0";
                                    }
                                }
                                std::vector<std::string> iterations;
                                for (const auto &dim : sort<L_DIMS, R_DIMS>(used_dimensions()[i])) {
                                    iterations.push_back(stringf("(%s + %c_dim_%d_index_%c_%d)",
                                                                 offsets[dim],
                                                                 lower_case(level), i, lower_case(dim.type), dim.nr
                                    ));
                                }
                                offset_code.append(stringf("size_t %c_mem_offset_dim_%d = (%s) * ((%s) - (%s) + 1) + (%s);\n",
                                                           lower_case(p_level), i,
                                                           make_flat_index(
                                                                   multi_stringf("%s_DIM_%d_OCL_DIM_ORDER_%d(%s)",
                                                                                 V(upper_case(_input_buffer.name())), V(i), uint_range(used_dimensions()[i].size() - 1, 0),
                                                                                 V(concat(iterations, ", "))),
                                                                   multi_stringf("%s_DIM_%d_OCL_DIM_ORDER_%d(%s)",
                                                                                 V(upper_case(_input_buffer.name())), V(i), uint_range(used_dimensions()[i].size() - 1, 0),
                                                                                 V(concat(_macros.num_cached_iterations(V(kernel), V(parent_level(level)), sort<L_DIMS, R_DIMS>(used_dimensions()[i])), ", "))
                                                                   )
                                                           ),
                                                           replace_vars_in_index_function(index_functions().size() - 1, i, multi_stringf("(GET_%s_SIZE_%c_%d - 1)", V(upper_case(long_level(p_level))), dim_range_types(L_DIMS, R_DIMS), dim_range_nrs(L_DIMS, R_DIMS))),
                                                           replace_vars_in_index_function(0, i, repeat<std::string>("0", L_DIMS + R_DIMS)),
                                                           replace_vars_in_index_function(0, i, multi_stringf("GET_%s_ID_%c_%d", V(upper_case(long_level(p_level))), dim_range_types(L_DIMS, R_DIMS), dim_range_nrs(L_DIMS, R_DIMS)))
                                ));
                            }
                        }
                    }
                }
            }
            if (level != LEVEL::LOCAL) offset_code.append("#endif\n");
            offset_code.append("#endif\n");
            copy_body = offset_code + copy_body;
        }

        // determine whether this code loads the first cache block
        bool loading_first_cb = true;
        for (const auto dim : _distinct_used_dimensions) {
            if (skipped_loops.find(dim) == skipped_loops.end() || !contains(skipped_loops[dim], level)) {
                loading_first_cb = false;
                break;
            }
        }

        // determine whether this code loads post processing cache block
        bool post_processing = false;
        for (const auto dim : _distinct_used_dimensions) {
            if ((skipped_loops.find(dim) == skipped_loops.end() || !contains(skipped_loops[dim], level)) && phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(level, dim)] != 1) {
                post_processing = true;
                break;
            }
        }

        // determine cache block size
        std::vector<std::string> flat_index_vars;
        std::vector<std::string> flat_index_sizes;
        std::stringstream cache_block_size_code;
        std::stringstream undef_cache_block_size_code;
        cache_block_size_code << std::endl << "#define STATIC_CB_SIZE";
        for (int i = 0; i < used_dimensions().size(); ++i) {
            std::vector<std::string> static_prefix;
            std::vector<std::string> dynamic_prefix;
            for (const auto &dim : used_dimensions()[i]) {
                flat_index_vars.push_back(stringf("%c_dim_%d_index_%c_%d", lower_case(level), i, lower_case(dim.type), dim.nr));
                flat_index_sizes.push_back(stringf("%s_CB_SIZE_DIM_%d_%c_%d", upper_case(_input_buffer.name()), i, dim.type, dim.nr));

                auto def_code = [&] (auto inner_phase, bool dynamic) {
                    std::string active_fu_condition;
                    // TODO check active FU on every skipped level
                    // TODO add condition to simple and complex buffer sizes

                    if (_simple_buffer) {
                        cache_block_size_code << std::endl
                                                       << stringf("%s%s_CB_SIZE_DIM_%d_%c_%d%s(%s%s)%s",
                                                                  dynamic ? "" : "#define ",
                                                                  upper_case(_input_buffer.name()), i, dim.type, dim.nr,
                                                                  dynamic ? " = " : " ",
                                                                  level == LEVEL::PRIVATE && (inner_phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dim)] == 3 || inner_phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(level, dim)] == 3) && (skipped_loops.find(dim) != skipped_loops.end() && std::find(skipped_loops[dim].begin(), skipped_loops[dim].end(), level) != skipped_loops[dim].end())
                                                                  ? stringf("(%s < %s - %s * %s) * ",
                                                                            _macros.fu_id(kernel, parent_level(level), dim), // TODO: should this be GET_LOCAL_ID?
                                                                            _macros.num_extra_elements(kernel, parent_level(level), dim, phase),
                                                                            _macros.fu_id(kernel, parent_level(parent_level(level)), dim),
                                                                            _macros.num_fu(kernel, parent_level(level), dim)
                                                                  )
                                                                  : "",
                                                                  _macros.num_processed_elements(kernel, level, dim, inner_phase),
                                                                  dynamic ? ";" : ""
                                                       );
                    } else {
                        std::string tmp;
                        if (level == LEVEL::LOCAL && inner_phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(level, dim)] == 3) {
                            tmp = "1";
                        } else {
                            tmp = stringf("(%s / %s)",
                                          _macros.num_processed_elements(kernel, level, dim, inner_phase),
                                          level == LEVEL::PRIVATE
                                          ? "1"
                                          : stringf("GET_%s_SIZE_%c_%d", upper_case(long_level(level)), dim.type, dim.nr));
                        }
                        if (dynamic) {
                            cache_block_size_code << std::endl << stringf("%s_CB_SIZE_DIM_%d_%c_%d = %s;", upper_case(_input_buffer.name()), i, dim.type, dim.nr, tmp);
                        } else {
                            cache_block_size_code << std::endl << stringf("#define %s_CB_SIZE_DIM_%d_%c_%d %s", upper_case(_input_buffer.name()), i, dim.type, dim.nr, tmp);
                        }
                        std::string cb_size_prefix;
                        if (level == LEVEL::PRIVATE) {
                            tmp = "1";
                        } else {
                            if (inner_phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(level, dim)] == 3) {
                                tmp = stringf("((%s < (%s + %s - 1) / %s) * ((%s - %s * %s <= %s) * (%s - %s * %s) + (%s - %s * %s > %s) * (%s)))",
                                              _macros.fu_id(kernel, parent_level(LEVEL::LOCAL), dim),
                                              _macros.num_extra_elements(kernel, LEVEL::LOCAL, dim),
                                              _macros.num_fu(kernel, LEVEL::LOCAL, dim),
                                              _macros.num_fu(kernel, LEVEL::LOCAL, dim),
                                              _macros.num_extra_elements(kernel, LEVEL::LOCAL, dim),
                                              _macros.fu_id(kernel, parent_level(LEVEL::LOCAL), dim),
                                              _macros.num_fu(kernel, LEVEL::LOCAL, dim),
                                              _macros.num_fu(kernel, LEVEL::LOCAL, dim),
                                              _macros.num_extra_elements(kernel, LEVEL::LOCAL, dim),
                                              _macros.fu_id(kernel, parent_level(LEVEL::LOCAL), dim),
                                              _macros.num_fu(kernel, LEVEL::LOCAL, dim),
                                              _macros.num_extra_elements(kernel, LEVEL::LOCAL, dim),
                                              _macros.fu_id(kernel, parent_level(LEVEL::LOCAL), dim),
                                              _macros.num_fu(kernel, LEVEL::LOCAL, dim),
                                              _macros.num_fu(kernel, LEVEL::LOCAL, dim),
                                              _macros.num_fu(kernel, LEVEL::LOCAL, dim)
                                );
                                cb_size_prefix = stringf("(%s < (%s + %s - 1) / %s) * ",
                                                         _macros.fu_id(kernel, parent_level(LEVEL::LOCAL), dim),
                                                         _macros.num_extra_elements(kernel, LEVEL::LOCAL, dim),
                                                         _macros.num_fu(kernel, LEVEL::LOCAL, dim),
                                                         _macros.num_fu(kernel, LEVEL::LOCAL, dim));
                            } else {
                                tmp = stringf("GET_%s_SIZE_%c_%d", upper_case(long_level(level)), dim.type, dim.nr);
                            }
                        }
                        if (dynamic) {
                            cache_block_size_code << std::endl << stringf("%s_CB_SIZE_DIM_%d_PREFIX_%c_%d = %s;", upper_case(_input_buffer.name()), i, dim.type, dim.nr, cb_size_prefix.empty() ? "1" : cb_size_prefix.substr(0, cb_size_prefix.length() - 3));
                            cache_block_size_code << std::endl << stringf("%s_CB_SIZE_DIM_%d_NUM_FU_%c_%d = %s;", upper_case(_input_buffer.name()), i, dim.type, dim.nr, tmp);
                        } else {
                            cache_block_size_code << std::endl << stringf("#define %s_CB_SIZE_DIM_%d_PREFIX_%c_%d() %s", upper_case(_input_buffer.name()), i, dim.type, dim.nr, cb_size_prefix);
                            cache_block_size_code << std::endl << stringf("#define %s_CB_SIZE_DIM_%d_NUM_FU_%c_%d %s", upper_case(_input_buffer.name()), i, dim.type, dim.nr, tmp);
                        }
                    }

                    if (dynamic ||
                        ((level == LEVEL::LOCAL && inner_phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(LEVEL::LOCAL, dim)] == 3) || (_runtime_inputs && inner_phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(LEVEL::LOCAL, dim)] == 2)) ||
                        (level == LEVEL::PRIVATE && (inner_phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dim)] == 3 || inner_phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(level, dim)] == 3) && (skipped_loops.find(dim) != skipped_loops.end() && std::find(skipped_loops[dim].begin(), skipped_loops[dim].end(), level) != skipped_loops[dim].end()))) {
                        cache_block_size_code << std::endl << "#ifdef STATIC_CB_SIZE";
                        cache_block_size_code << std::endl << "#undef STATIC_CB_SIZE";
                        cache_block_size_code << std::endl << "#endif";
                    }
                };

                bool undef = true;
                if (skipped_loops.find(dim) == skipped_loops.end()) {
                    def_code(phase, false);
                    static_prefix.push_back(stringf("%s_CB_SIZE_DIM_%d_PREFIX_%c_%d()", upper_case(_input_buffer.name()), i, dim.type, dim.nr));
                } else {
                    if (skipped_loops[dim].size() == 1) {
                        // only loops on this level have been skipped
                        char tmp_phase[3 * (L_DIMS + R_DIMS)];
                        for (int k = 0; k < 3 * (L_DIMS + R_DIMS); ++k) {
                            tmp_phase[k] = phase != nullptr ? phase[k] : 1;
                        }
                        bool dynamic_if = _runtime_inputs && (level == LEVEL::LOCAL || phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(LEVEL::LOCAL, dim)] != 1);
                        undef = !dynamic_if;
                        if (dynamic_if) {
                            cache_block_size_code << std::endl << stringf("size_t %s_CB_SIZE_DIM_%d_%c_%d;", upper_case(_input_buffer.name()), i, dim.type, dim.nr);
                            if (!_simple_buffer) {
                                cache_block_size_code << std::endl << stringf("size_t %s_CB_SIZE_DIM_%d_PREFIX_%c_%d;", upper_case(_input_buffer.name()), i, dim.type, dim.nr);
                                cache_block_size_code << std::endl << stringf("size_t %s_CB_SIZE_DIM_%d_NUM_FU_%c_%d;", upper_case(_input_buffer.name()), i, dim.type, dim.nr);
                            }
                        }
                        // phase 1
                        cache_block_size_code << std::endl << stringf("%s (%s >= 1)%s", dynamic_if ? "if" : "#if", _macros.num_steps(kernel, skipped_loops[dim][0], dim, tmp_phase), dynamic_if ? " {" : "");
                        tmp_phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(skipped_loops[dim][0], dim)] = 1;
                        def_code(tmp_phase, dynamic_if);
                        // phase 2
                        cache_block_size_code << std::endl << stringf("%s (%s > 0)%s", dynamic_if ? "} else if" : "#elif", _macros.num_extra_cached_iterations(kernel, skipped_loops[dim][0], dim, tmp_phase), dynamic_if ? " {" : "");
                        tmp_phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(skipped_loops[dim][0], dim)] = 2;
                        def_code(tmp_phase, dynamic_if);
                        // phase 3
                        if (level == LEVEL::LOCAL) {
                            cache_block_size_code << std::endl << stringf("%s (%s > 0)%s", dynamic_if ? "} else if" : "#elif", _macros.num_extra_elements(kernel, skipped_loops[dim][0], dim, tmp_phase), dynamic_if ? " {" : "");
                            tmp_phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(skipped_loops[dim][0], dim)] = 3;
                            def_code(tmp_phase, dynamic_if);
                        }
                        cache_block_size_code << std::endl << (dynamic_if ? "}" : "#endif");
                        if (dynamic_if) {
                            dynamic_prefix.push_back(stringf("%s_CB_SIZE_DIM_%d_PREFIX_%c_%d", upper_case(_input_buffer.name()), i, dim.type, dim.nr));
                        } else {
                            static_prefix.push_back(stringf("%s_CB_SIZE_DIM_%d_PREFIX_%c_%d()", upper_case(_input_buffer.name()), i, dim.type, dim.nr));
                        }
                    } else {
                        // loops on this level and parent level have been skipped
                        char tmp_phase[3 * (L_DIMS + R_DIMS)];
                        for (int k = 0; k < 3 * (L_DIMS + R_DIMS); ++k) {
                            tmp_phase[k] = phase != nullptr ? phase[k] : 1;
                        }
                        bool dynamic_if = _runtime_inputs;
                        undef = !dynamic_if;
                        if (dynamic_if) {
                            cache_block_size_code << std::endl << stringf("size_t %s_CB_SIZE_DIM_%d_%c_%d;", upper_case(_input_buffer.name()), i, dim.type, dim.nr);
                            if (!_simple_buffer) {
                                cache_block_size_code << std::endl << stringf("size_t %s_CB_SIZE_DIM_%d_PREFIX_%c_%d;", upper_case(_input_buffer.name()), i, dim.type, dim.nr);
                                cache_block_size_code << std::endl << stringf("size_t %s_CB_SIZE_DIM_%d_NUM_FU_%c_%d;", upper_case(_input_buffer.name()), i, dim.type, dim.nr);
                            }
                        }
                        // parent phase 1
                        cache_block_size_code << std::endl << stringf("%s (%s >= 1)%s", dynamic_if ? "if" : "#if", _macros.num_steps(kernel, skipped_loops[dim][0], dim, tmp_phase), dynamic_if ? " {" : "");
                        tmp_phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(skipped_loops[dim][0], dim)] = 1;
                        // phase 1
                        cache_block_size_code << std::endl << stringf("%s (%s >= 1)%s", dynamic_if ? "if" : "#if", _macros.num_steps(kernel, skipped_loops[dim][1], dim, tmp_phase), dynamic_if ? " {" : "");
                        tmp_phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(skipped_loops[dim][1], dim)] = 1;
                        def_code(tmp_phase, dynamic_if);
                        // phase 2
                        cache_block_size_code << std::endl << stringf("%s (%s > 0)%s", dynamic_if ? "} else if" : "#elif", _macros.num_extra_cached_iterations(kernel, skipped_loops[dim][1], dim, tmp_phase), dynamic_if ? " {" : "");
                        tmp_phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(skipped_loops[dim][1], dim)] = 2;
                        def_code(tmp_phase, dynamic_if);
                        cache_block_size_code << std::endl << (dynamic_if ? "}" : "#endif");
                        // parent phase 2
                        cache_block_size_code << std::endl << stringf("%s (%s > 0)%s", dynamic_if ? "} else if" : "#elif", _macros.num_extra_cached_iterations(kernel, skipped_loops[dim][0], dim, tmp_phase), dynamic_if ? " {" : "");
                        tmp_phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(skipped_loops[dim][0], dim)] = 2;
                        // phase 1
                        cache_block_size_code << std::endl << stringf("%s (%s >= 1)%s", dynamic_if ? "if" : "#if", _macros.num_steps(kernel, skipped_loops[dim][1], dim, tmp_phase), dynamic_if ? " {" : "");
                        tmp_phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(skipped_loops[dim][1], dim)] = 1;
                        def_code(tmp_phase, dynamic_if);
                        // phase 2
                        cache_block_size_code << std::endl << stringf("%s (%s > 0)%s", dynamic_if ? "} else if" : "#elif", _macros.num_extra_cached_iterations(kernel, skipped_loops[dim][1], dim, tmp_phase), dynamic_if ? " {" : "");
                        tmp_phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(skipped_loops[dim][1], dim)] = 2;
                        def_code(tmp_phase, dynamic_if);
                        cache_block_size_code << std::endl << (dynamic_if ? "}" : "#endif");
                        // parent phase 3
                        cache_block_size_code << std::endl << stringf("%s (%s > 0)%s", dynamic_if ? "} else if" : "#elif", _macros.num_extra_elements(kernel, skipped_loops[dim][0], dim, tmp_phase), dynamic_if ? " {" : "");
                        tmp_phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(skipped_loops[dim][0], dim)] = 3;
                        tmp_phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(skipped_loops[dim][1], dim)] = 3;
                        def_code(tmp_phase, dynamic_if);
                        cache_block_size_code << std::endl << (dynamic_if ? "}" : "#endif");
                        if (dynamic_if) {
                            dynamic_prefix.push_back(stringf("%s_CB_SIZE_DIM_%d_PREFIX_%c_%d", upper_case(_input_buffer.name()), i, dim.type, dim.nr));
                        } else {
                            static_prefix.push_back(stringf("%s_CB_SIZE_DIM_%d_PREFIX_%c_%d()", upper_case(_input_buffer.name()), i, dim.type, dim.nr));
                        }
                    }
                }

                if (undef) {
                    undef_cache_block_size_code << std::endl << stringf("#undef %s_CB_SIZE_DIM_%d_%c_%d", upper_case(_input_buffer.name()), i, dim.type, dim.nr);
                    if (!_simple_buffer) {
                        undef_cache_block_size_code << std::endl << stringf("#undef %s_CB_SIZE_DIM_%d_NUM_FU_%c_%d", upper_case(_input_buffer.name()), i, dim.type, dim.nr);
                        undef_cache_block_size_code << std::endl << stringf("#undef %s_CB_SIZE_DIM_%d_PREFIX_%c_%d", upper_case(_input_buffer.name()), i, dim.type, dim.nr);
                    }
                }
            }
            if (!_simple_buffer) {
                flat_index_vars.push_back(loop_indices[i]);
                flat_index_sizes.push_back(stringf("%s_CB_SIZE_DIM_%d", upper_case(_input_buffer.name()), i));

                cache_block_size_code << std::endl << stringf("#define %s_CB_SIZE_DIM_%d %s",
                                                              upper_case(_input_buffer.name()), i,
                                                              stringf("(%s%s%s((%s) - (%s) + 1))",
                                                                      concat(static_prefix),
                                                                      concat(dynamic_prefix, " * "),
                                                                      dynamic_prefix.empty() ? "" : " * ",
                                                                      replace_vars_in_index_function(index_functions().size() - 1, i, multi_stringf("(%s_CB_SIZE_DIM_%d_NUM_FU_%c_%d - 1)", V(upper_case(_input_buffer.name())), V(i), dim_range_types(L_DIMS, R_DIMS), dim_range_nrs(L_DIMS, R_DIMS))),
                                                                      replace_vars_in_index_function(0, i, repeat<std::string>("0", L_DIMS + R_DIMS)))
                );
                undef_cache_block_size_code << std::endl << stringf("#undef %s_CB_SIZE_DIM_%d", upper_case(_input_buffer.name()), i);
            }
        }
        undef_cache_block_size_code << std::endl << "#ifdef STATIC_CB_SIZE";
        undef_cache_block_size_code << std::endl << "#undef STATIC_CB_SIZE";
        undef_cache_block_size_code << std::endl << "#endif";

        auto flat_index_vars_descending = multi_stringf("BUFFER_%s_FLAT_INDEXING_%d(%s)", V(upper_case(_input_buffer.name())), uint_range(flat_index_vars.size() - 1, 0), V(concat(flat_index_vars, ", ")));
        auto flat_index_sizes_descending = multi_stringf("BUFFER_%s_FLAT_INDEXING_%d(%s)", V(upper_case(_input_buffer.name())), uint_range(flat_index_sizes.size() - 1, 0), V(concat(flat_index_sizes, ", ")));

        std::string cache_block_size = concat(flat_index_sizes, " * ");

        std::string debug_print_code;
#ifdef PRINT_CACHING_DEBUG_LOAD_CB
        debug_print_code.append("\n\n");
        debug_print_code.append(stringf("DEBUG_PRINT(\"loading %7s %s cache block %s with size %s into %%16p\\n\", %s, %s, cb_%c_%s%s);\n",
                                        lower_case(long_level(level)),
                                        lower_case(_input_buffer.name()),
                                        concat(multi_stringf("%c_%d: %%lu", split_dim_range(_distinct_used_dimensions_sorted).first, split_dim_range(_distinct_used_dimensions_sorted).second), ", "),
                                        concat(repeat(std::string("%lu"), flat_index_sizes.size()), "x"),
                                        concat(multi_stringf("%c_step_%c_%d", V(lower_case(level)), lower_case(split_dim_range(_distinct_used_dimensions_sorted).first), split_dim_range(_distinct_used_dimensions_sorted).second), ", "),
                                        concat(flat_index_sizes, ", "),
                                        lower_case(level), lower_case(_input_buffer.name()), dbl_buffering ? "_write" : ""
        ));
#endif

        return stringf(R"(%s%s

%sfor (size_t step = 0; step < %s / %s; ++step) {
  const size_t flat_index = %s + step * %s;
%s
%s
}
%s%sif (%s < %s %% %s) {
  const size_t flat_index = %s + (%s / %s) * %s;
%s
%s
}
%s%s

)",
                       cache_block_size_code.str(),
                       debug_print_code,
                       stringf("#if !defined(STATIC_CB_SIZE) || (%s / %s > 0)\n", cache_block_size, _macros.flat_num_wi(kernel, level)),
                       cache_block_size, _macros.flat_num_wi(kernel, level),
                       _macros.flat_wi_id(kernel, level), _macros.flat_num_wi(kernel, level),
                       level == LEVEL::LOCAL
                       ? indent(resolve_flat_index("flat_index", flat_index_vars, flat_index_sizes), 1)
                       : indent(stringf("#if %s\n%s\n#else\n%s\n#endif",
                                        concat(multi_stringf("((defined(%s_CACHE_%c_CB) && %s_CACHE_%c_CB != 0) || (!defined(%s_CACHE_%c_CB) && CACHE_%c_CB != 0))",
                                                             V(upper_case(_input_buffer.name())),
                                                             level_range(LEVEL::LOCAL, parent_level(level)),
                                                             V(upper_case(_input_buffer.name())),
                                                             level_range(LEVEL::LOCAL, parent_level(level)),
                                                             V(upper_case(_input_buffer.name())),
                                                             level_range(LEVEL::LOCAL, parent_level(level)),
                                                             level_range(LEVEL::LOCAL, parent_level(level))), " || "),
                                        resolve_flat_index("flat_index", flat_index_vars_descending, flat_index_sizes_descending),
                                        resolve_flat_index("flat_index", flat_index_vars, flat_index_sizes)
                       ), 1),
                       indent(copy_body, 1),
                       "#endif\n",
                       stringf("#if !defined(STATIC_CB_SIZE) || (%s %% %s > 0)\n", cache_block_size, _macros.flat_num_wi(kernel, level)),
                       _macros.flat_wi_id(kernel, level), cache_block_size, _macros.flat_num_wi(kernel, level),
                       _macros.flat_wi_id(kernel, level), cache_block_size, _macros.flat_num_wi(kernel, level),
                       _macros.flat_num_wi(kernel, level),
                       level == LEVEL::LOCAL
                       ? indent(resolve_flat_index("flat_index", flat_index_vars, flat_index_sizes), 1)
                       : indent(stringf("#if %s\n%s\n#else\n%s\n#endif",
                                        concat(multi_stringf("((defined(%s_CACHE_%c_CB) && %s_CACHE_%c_CB != 0) || (!defined(%s_CACHE_%c_CB) && CACHE_%c_CB != 0))",
                                                             V(upper_case(_input_buffer.name())),
                                                             level_range(LEVEL::LOCAL, parent_level(level)),
                                                             V(upper_case(_input_buffer.name())),
                                                             level_range(LEVEL::LOCAL, parent_level(level)),
                                                             V(upper_case(_input_buffer.name())),
                                                             level_range(LEVEL::LOCAL, parent_level(level)),
                                                             level_range(LEVEL::LOCAL, parent_level(level))), " || "),
                                        resolve_flat_index("flat_index", flat_index_vars_descending, flat_index_sizes_descending),
                                        resolve_flat_index("flat_index", flat_index_vars, flat_index_sizes)
                       ), 1),
                       indent(copy_body, 1),
                       "#endif\n",
                       undef_cache_block_size_code.str()
        );
    }

private:
    const input_buffer_class &_input_buffer;
    std::vector<dimension_t> _distinct_used_dimensions;
    std::vector<dimension_t> _distinct_used_dimensions_sorted;
    bool                     _simple_buffer;
    const macros <L_DIMS, R_DIMS> &_macros;
    const bool                     _runtime_inputs;
    const bool                     _cuda;

    std::string _macro_name_pattern;
    std::string _fu_macro_name_pattern;
    std::string _macro_call_pattern;
    std::string _fu_macro_call_pattern;
};

}
}

#endif //MD_BLAS_INPUT_BUFFER_WRAPPER_HPP
