//
// Created by Richard Schulze on 30.01.18.
//

#ifndef MD_BLAS_INPUT_SCALAR_HPP
#define MD_BLAS_INPUT_SCALAR_HPP

#include <string>

namespace md_hom {

class input_scalar_class {
public:
    input_scalar_class(const std::string &data_type, const std::string &name);

    const std::string &name() const;
    const std::string &data_type() const;
private:
    const std::string _name;
    const std::string _data_type;
};

input_scalar_class input_scalar(const std::string &data_type, const std::string &name);


}

#endif //MD_BLAS_INPUT_SCALAR_HPP
