//
// Created by Richard Schulze on 30.10.2017.
//
#include <types.hpp>
#include "helper.hpp"
#include "../md_hom_generator.hpp"

std::string search_and_replace(const std::string& str,
                               const std::string& oldStr,
                               const std::string& newStr) {
    if (str == oldStr) return newStr;

    std::vector<std::string::size_type> positions;
    std::string::size_type pos = 0u;
    while((pos = str.find(oldStr, pos)) != std::string::npos){
        positions.push_back(pos);
        pos += oldStr.length();
    }
    if (positions.empty()) return str;

    std::string result;
    result.reserve(static_cast<long long>(str.length()) + static_cast<long long>(positions.size()) * (static_cast<long long>(newStr.length()) - static_cast<long long>(oldStr.length())));
    pos = 0u;
    for (const auto &match_pos : positions) {
        result.append(str.substr(pos, match_pos - pos));
        result.append(newStr);
        pos = match_pos + oldStr.length();
    }
    if (pos < str.length()) {
        result.append(str.substr(pos));
    }
    return result;
}
void search_and_replace_inplace(std::string& str,
                                const std::string& oldStr,
                                const std::string& newStr) {
    if (str == oldStr) {
        str = newStr;
        return;
    }

    if (oldStr.length() == newStr.length()) {
        std::string::size_type pos = 0u;
        while((pos = str.find(oldStr, pos)) != std::string::npos) {
            str.replace(pos, oldStr.length(), newStr);
            pos += oldStr.length();
        }
    } else if (oldStr.length() > newStr.length()) {
        std::string::size_type pos = 0u;
        while((pos = str.find(oldStr, pos)) != std::string::npos) {
            str.replace(pos, oldStr.length(), newStr);
            pos += newStr.length();
        }
        str.shrink_to_fit();
    } else {
        std::vector<std::string::size_type> positions;
        std::string::size_type pos = 0u;
        while((pos = str.find(oldStr, pos)) != std::string::npos){
            positions.push_back(pos);
            pos += oldStr.length();
        }
        if (positions.empty()) return;
        long long prefix_length = static_cast<long long>(positions.size()) * (static_cast<long long>(newStr.length()) - static_cast<long long>(oldStr.length()));
        long long prefix = static_cast<long long>(positions.size()) * (static_cast<long long>(newStr.length()) - static_cast<long long>(oldStr.length()));
        long long read_pos = prefix_length;
        long long write_pos = 0;
        str.insert(0, prefix_length, 0);
        for (const auto &match_pos : positions) {
            // copy until match position
            while (prefix_length + match_pos - read_pos >= prefix) {
                long long chunk = std::min(static_cast<long long>(prefix_length + match_pos) - read_pos, prefix);
                str.replace(write_pos, chunk, str, read_pos, chunk);
                read_pos += chunk;
                write_pos += chunk;
            }
            if (read_pos < prefix_length + match_pos) {
                str.replace(write_pos, prefix_length + match_pos - read_pos, str, read_pos, prefix_length + match_pos - read_pos);
                write_pos += prefix_length + match_pos - read_pos;
                read_pos += prefix_length + match_pos - read_pos;
            }
            // replace match
            str.replace(write_pos, newStr.length(), newStr);
            read_pos += oldStr.length();
            write_pos += newStr.length();

            prefix -= (static_cast<long long>(newStr.length()) - static_cast<long long>(oldStr.length()));
        }
    }
}

std::string concat(const std::vector<std::string> &elements, const std::string &concatenator) {
    if (elements.empty()) return "";
    std::string::size_type new_size = 0;
    for (const auto &elem : elements) {
        new_size += elem.size();
    }
    new_size += (elements.size() - 1) * concatenator.size();
    std::string result;
    result.reserve(new_size);
    bool first = true;
    for (const auto& elem : elements) {
        if (!first) result.append(concatenator);
        result.append(elem);

        first = false;
    }
    return result;
}
std::vector<std::string> wrap(const std::vector<std::string> &elements, const std::string &wrapper_prefix, const std::string &wrapper_suffix) {
    std::vector<std::string> wrapped_elements;
    for (const auto& elem : elements) {
        wrapped_elements.emplace_back();
        wrapped_elements.back().reserve(wrapper_prefix.size() + elem.size() + wrapper_suffix.size());
        wrapped_elements.back().append(wrapper_prefix).append(elem).append(wrapper_suffix);
    }
    return wrapped_elements;
}
std::string indent(const std::string &text, unsigned int indentation) {
    std::string prefix;
    for (auto i = 0; i < indentation; ++i) {
        prefix += "  ";
    }
    if (text.empty()) return prefix;
    if (prefix.empty()) return text;
    std::string result = text;
    result.insert(0, prefix);
    search_and_replace_inplace(result, "\n", std::string("\n") + prefix);
    return result;
}
void indent_inplace(std::string &text, unsigned int indentation) {
    std::string prefix;
    for (auto i = 0; i < indentation; ++i) {
        prefix += "  ";
    }
    if (text.empty()) {
        text = prefix;
    } else if (prefix.empty()) {
        return;
    } else {
        text.insert(0, prefix);
        search_and_replace_inplace(text, "\n", std::string("\n") + prefix);
    }
}

namespace md_hom {

dimension_t L(unsigned int nr) {
    return {DIM_TYPE::L, nr};
}
dimension_t R(unsigned int nr) {
    return {DIM_TYPE::R, nr};
}

bool operator==(const dimension_t& lhs, const dimension_t& rhs) {
    return lhs.type == rhs.type && lhs.nr == rhs.nr;
}
bool operator!=(const dimension_t& lhs, const dimension_t& rhs) {
    return lhs.type != rhs.type || lhs.nr != rhs.nr;
}
bool operator<(const dimension_t& lhs, const dimension_t& rhs) {
    return (lhs.type == DIM_TYPE::L && rhs.type == DIM_TYPE::R)
           || (lhs.type == rhs.type && lhs.nr < rhs.nr);
}


namespace generator {

const char* stringf_std_string_helper(const std::string& arg) {
    return arg.c_str();
}

LEVEL parent_level(LEVEL level) {
    switch (level) {
        case LEVEL::GLOBAL:
            // TODO
            return LEVEL::GLOBAL;
        case LEVEL::LOCAL:
            return LEVEL::GLOBAL;
        case LEVEL::PRIVATE:
            return LEVEL::LOCAL;
    }
}

std::vector<LEVEL> parent_levels(LEVEL level, bool inclusive, ORDER order) {
    std::vector<LEVEL> ret;
    if (inclusive) {
        ret.push_back(level);
    }
    switch (level) {
        case LEVEL::GLOBAL:
            break;
        case LEVEL::LOCAL:
            ret.push_back(LEVEL::GLOBAL);
            break;
        case LEVEL::PRIVATE:
            ret.push_back(LEVEL::LOCAL);
            ret.push_back(LEVEL::GLOBAL);
            break;
    }
    if (order == DESCENDING) {
        std::reverse(ret.begin(), ret.end());
    }
    return ret;
}

LEVEL sub_level(LEVEL level) {
    switch (level) {
        case LEVEL::GLOBAL:
            return LEVEL::LOCAL;
        case LEVEL::LOCAL:
            return LEVEL::PRIVATE;
        case LEVEL::PRIVATE:
            // TODO
            return LEVEL::PRIVATE;
    }
}

std::vector<LEVEL> sub_levels(LEVEL level, bool inclusive, ORDER order) {
    std::vector<LEVEL> ret;
    if (inclusive) {
        ret.push_back(level);
    }
    switch (level) {
        case LEVEL::GLOBAL:
            ret.push_back(LEVEL::LOCAL);
            ret.push_back(LEVEL::PRIVATE);
            break;
        case LEVEL::LOCAL:
            ret.push_back(LEVEL::PRIVATE);
            break;
        case LEVEL::PRIVATE:
            break;
    }
    if (order == ASCENDING) {
        std::reverse(ret.begin(), ret.end());
    }
    return ret;
}

unsigned int count(LEVEL start, LEVEL end) {
    std::vector<LEVEL> levels_to_check;
    unsigned int count = 1;
    if (start == end) return count;
    else if (start < end) {
        levels_to_check = sub_levels(start, false, ORDER::DESCENDING);
    } else {
        levels_to_check = parent_levels(start, false, ORDER::ASCENDING);
    }
    for (const auto& level : levels_to_check) {
        ++count;
        if (level == end) {
            return count;
        }
    }
}

std::vector<LEVEL> level_range(LEVEL start, LEVEL end) {
    std::vector<LEVEL> range;
    range.push_back(start);
    std::vector<LEVEL> levels_to_check;
    if (start == end) {
        return range;
    } else if (start < end) {
        levels_to_check = sub_levels(start, false, ORDER::DESCENDING);
    } else {
        levels_to_check = parent_levels(start, false, ORDER::ASCENDING);
    }
    for (const auto& level : levels_to_check) {
        range.push_back(level);
        if (level == end) {
            return range;
        }
    }
}

unsigned int DIM_TYPE_ID(DIM_TYPE dt) {
    switch (dt) {
        case DIM_TYPE::L: return 0;
        case DIM_TYPE::R: return 1;
    }
}

std::pair<std::vector<DIM_TYPE>, std::vector<unsigned int>> split_dim_range(const std::vector<dimension_t>& dim_range) {
    std::pair<std::vector<DIM_TYPE>, std::vector<unsigned int>> result;
    for (const auto& dim : dim_range) {
        result.first.push_back(dim.type);
        result.second.push_back(dim.nr);
    }
    return result;
}

std::vector<dimension_t> dim_range(unsigned int l_dims, unsigned int r_dims) {
    std::vector<dimension_t> result;
    for (unsigned int l_dim = 1; l_dim <= l_dims; ++l_dim) {
        result.push_back(L(l_dim));
    }
    for (unsigned int r_dim = 1; r_dim <= r_dims; ++r_dim) {
        result.push_back(R(r_dim));
    }
    return result;
}
std::vector<DIM_TYPE> dim_range_types(unsigned int l_dims, unsigned int r_dims) {
    return split_dim_range(dim_range(l_dims, r_dims)).first;
}
std::vector<unsigned int> dim_range_nrs(unsigned int l_dims, unsigned int r_dims) {
    return split_dim_range(dim_range(l_dims, r_dims)).second;
}
std::vector<unsigned int> uint_range(unsigned int start, unsigned int end) {
    std::vector<unsigned int> result;
    if (start <= end) {
        for (long i = start; i <= end; ++i)
            result.push_back(static_cast<unsigned int>(i));
    } else {
        for (long i = start; i >= end; --i)
            result.push_back(static_cast<unsigned int>(i));
    }

    return result;
}

unsigned int LEVEL_ID(LEVEL l) {
    switch (l) {
        case LEVEL::PRIVATE: return 0;
        case LEVEL::LOCAL:   return 1;
        case LEVEL::GLOBAL:  return 2;
    }
}

std::string long_level(LEVEL l) {
    switch (l) {
        case LEVEL::PRIVATE: return "PRIVATE";
        case LEVEL::LOCAL:   return "LOCAL";
        case LEVEL::GLOBAL:  return "GLOBAL";
    }
}
std::vector<std::string> long_level(const std::vector<LEVEL> &level) {
    std::vector<std::string> str_long_level;
    for (const auto& l : level) {
        str_long_level.push_back(long_level(l));
    }
    return str_long_level;
}

char lower_case(char letter) {
    assert((letter >= 48 && letter <= 57) || (letter >= 65 && letter <= 90) || (letter >= 97 && letter <= 122));
    if ((letter >= 48 && letter <= 57) || (letter >= 97 && letter <= 122)) {
        return letter;
    } else {
        return static_cast<char>(letter + 32);
    }
}
char lower_case(DIM_TYPE dim_type) {
    return lower_case(static_cast<char>(dim_type));
}
std::vector<char> lower_case(const std::vector<DIM_TYPE> &dim_type) {
    std::vector<char> lc_dim_types;
    for (const auto& type : dim_type) {
        lc_dim_types.push_back(lower_case(type));
    }
    return lc_dim_types;
}
char lower_case(LEVEL level) {
    return lower_case(static_cast<char>(level));
}
std::vector<char> lower_case(const std::vector<LEVEL> &level) {
    std::vector<char> lc_level;
    for (const auto& l : level) {
        lc_level.push_back(lower_case(l));
    }
    return lc_level;
}
std::string lower_case(const std::string &str) {
    std::string ret = str;
    for (int i = 0; i < str.length(); ++i) {
        if (ret[i] >= 65 && ret[i] <= 90) {
            ret[i] = lower_case(ret[i]);
        }
    }
    return ret;
}

char upper_case(char letter) {
    assert((letter >= 65 && letter <= 90) || (letter >= 97 && letter <= 122));
    if (letter >= 65 && letter <= 90) {
        return letter;
    } else {
        return static_cast<char>(letter - 32);
    }
}
char upper_case(DIM_TYPE dim_type) {
    return upper_case(static_cast<char>(dim_type));
}
char upper_case(LEVEL level) {
    return upper_case(static_cast<char>(level));
}
std::string upper_case(const std::string &str) {
    std::string ret = str;
    for (int i = 0; i < str.length(); ++i) {
        if (ret[i] >= 97 && ret[i] <= 122) {
            ret[i] = upper_case(ret[i]);
        }
    }
    return ret;
}
std::vector<std::string> upper_case(const std::vector<std::string> &strs) {
    std::vector<std::string> uc_strs(strs.size());
    for (int i = 0; i < strs.size(); ++i) {
        uc_strs[i] = upper_case(strs[i]);
    }
    return uc_strs;
}

std::string make_flat_index(const std::vector<std::string> &indices, const std::vector<std::string> &dim_sizes) {
    if (indices.empty()) return "0";
    std::stringstream flat_addressing;
    for (int k = 0; k < indices.size(); ++k) {
        if (k > 0) {
            flat_addressing << " + ";
        }
        std::stringstream factor;
        for (int j = k + 1; j < dim_sizes.size(); ++j) {
            factor << " * " << dim_sizes[j].c_str();
        }
        flat_addressing << "(" << indices[k].c_str() << ")" << factor.str().c_str();
    }
    return flat_addressing.str();
}

std::string resolve_flat_index(const std::string &flat_index_name,
                               const std::vector<std::string> &names,
                               const std::vector<std::string> &dim_sizes) {
    std::string result;
    if (names.size() == 1) {
        result.append(stringf("const size_t %s = %s;", names[0].c_str(), flat_index_name.c_str()));
        return result;
    }
    for (int i = 0; i < names.size(); ++i) {
        std::string divisor = "(";
        for (int k = i + 1; k < dim_sizes.size(); ++k) {
            divisor.append(stringf("%s%s", k > i + 1 ? " * " : "", dim_sizes[k].c_str()));
        }
        divisor.append(")");
        std::string modulator = dim_sizes[i];
        if (i == 0) {
            result.append(stringf("const size_t %s = %s / %s;\n", names[i].c_str(), flat_index_name.c_str(), divisor.c_str()));
        } else if (i == names.size() - 1) {
            result.append(stringf("const size_t %s = %s %% %s;", names[i].c_str(), flat_index_name.c_str(), modulator.c_str()));
        } else {
            result.append(stringf("const size_t %s = (%s / %s) %% %s;\n", names[i].c_str(), flat_index_name.c_str(), divisor.c_str(), modulator.c_str()));
        }
    }
    return result;
}

std::string iterations_loop_variable(LEVEL level, const dimension_t &dimension) {
    return stringf("%c_iteration_%c_%d", lower_case(level), lower_case(dimension.type), dimension.nr);
}
std::vector<std::string> iterations_loop_variable(LEVEL level, const std::vector<dimension_t> &dimensions) {
    std::vector<std::string> variables;
    for (const auto &dimension : dimensions) {
        variables.push_back(iterations_loop_variable(level, dimension));
    }
    return variables;
}
std::vector<std::string> iterations_loop_variable(LEVEL level, const std::vector<dimension_t> &used_dimensions,
                                                  unsigned int l_dims, unsigned int r_dims) {
    std::vector<std::string> indices;
    for (unsigned int i = 0; i < l_dims + r_dims; ++i) {
        auto dim = std::find(used_dimensions.begin(), used_dimensions.end(), (i < l_dims ? L(i + 1) : R(i - l_dims + 1)));
        if (dim != used_dimensions.end()) {
            indices.push_back(iterations_loop_variable(level, *dim));
        } else {
            indices.emplace_back(1, '0');
        }
    }
    return indices;
}

std::vector<dimension_t> reverse(const std::vector<dimension_t> &dimensions) {
    std::vector<dimension_t> reversed_dimensions(dimensions.size());
    std::reverse_copy(dimensions.begin(), dimensions.end(), reversed_dimensions.begin());
    return reversed_dimensions;
}

bool contains(const std::vector<dimension_t> &dimensions, dimension_t val) {
    for (const auto dim : dimensions)
        if (dim.type == val.type && dim.nr == val.nr)
            return true;
    return false;
}

bool contains(const std::vector<LEVEL> &levels, LEVEL val) {
    for (const auto level : levels)
        if (level == val)
            return true;
    return false;
}

std::string barrier(LEVEL level, bool cuda) {
    std::stringstream code;
    if (cuda) {
        code << "__syncthreads();";
    } else {
        code << stringf("barrier(CLK_%s_MEM_FENCE);", upper_case(long_level(level)));
    }
#ifdef PRINT_BARRIER
    std::string command = code.str();
    code << stringf(" DEBUG_PRINT(\"%s\\n\");", command);
#endif
    return code.str();
}

}
}
