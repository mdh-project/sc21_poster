//
//  ocal_kernel.hpp
//  ocal
//
//  Created by Ari Rasch on 25.01.18.
//  Copyright © 2018 Ari Rasch. All rights reserved.
//

#ifndef ocal_kernel_h
#define ocal_kernel_h


namespace ocal
{


class kernel
{
  public:
    template< typename... Ts >
    kernel( Ts&&... args )
      : _ocl_kernel( std::forward<Ts>( args )... ), _cuda_kernel( std::forward<Ts>( args )... )
    {}
  
    auto& get_ocl_kernel()
    {
      return _ocl_kernel;
    }

    auto& get_cuda_kernel()
    {
      return _cuda_kernel;
    }


  
  private:
    core::ocl::kernel  _ocl_kernel;
    core::cuda::kernel _cuda_kernel;

};

} // namespace "ocal"

#endif /* ocal_kernel_h */
