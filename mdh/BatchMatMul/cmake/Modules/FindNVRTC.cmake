# Find the NVRTC Library from NVIDIA (looks in CUDA_TOOLKIT_ROOT_DIR)
#
#  NVRTC_FOUND - System has NVRTC
#  NVRTC_INCLUDE_DIRS - NVRTC include files directories
#  NVRTC_LIBRARY - The NVRTC library

if(CUDA_FOUND)
    find_library(CUDA_NVRTC_LIBRARY nvrtc
            PATHS ${CUDA_TOOLKIT_ROOT_DIR}
            PATH_SUFFIXES lib lib64 lib/x64)
    if(CUDA_NVRTC_LIBRARY)
        message(STATUS "Found libnvrtc: ${CUDA_NVRTC_LIBRARY}")
    else()
        message(FATAL_ERROR "Cannot find libnvrtc.so")
    endif()
endif()