//
//  ocl_local_buffer.hpp
//  ocal_local
//
//  Created by Ari Rasch on 20.09.17.
//  Copyright © 2017 Ari Rasch. All rights reserved.
//

#ifndef ocl_local_buffer_hpp
#define ocl_local_buffer_hpp


namespace ocal
{

namespace core
{

namespace ocl
{


template< typename T >
class local_buffer : public abstract::local_or_shared_buffer< T >
{
//  // friend class parent
//  friend class abstract::local_or_shared_buffer< T >;
  
  public:
    template< typename... Ts >
    local_buffer( Ts... args )
      : abstract::local_or_shared_buffer< T >( args... )
    {}
};


} // namespace "ocl"

} // namespace "core"

} // namespace "ocal"


#endif /* ocl_local_buffer_hpp */
