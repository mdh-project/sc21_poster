//
//  ocal_device.hpp
//  ocal_ocal
//
//  Created by Ari Rasch on 20.09.17.
//  Copyright © 2017 Ari Rasch. All rights reserved.
//

#ifndef ocal_device_hpp
#define ocal_device_hpp


namespace ocal
{


template< typename DEV_TYPE = core::ocl::device >
class device : public core::ocl::device
{
  public:
    // when a platform is used then this is an OpenCL device
    template< typename... Ts >
    device( const Ts&... args )
      : core::ocl::device( args... )
    {}
};


} // namespace "ocal"


#endif /* ocal_device_hpp */
