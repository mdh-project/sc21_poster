//
// Created by Richard Schulze on 30.10.2017.
//

#ifndef MD_HOM_GENERATOR_CONFIGURATION_CLASS_HPP
#define MD_HOM_GENERATOR_CONFIGURATION_CLASS_HPP

#include <string>
#include <tuple>

#include "scalar_function.hpp"
#include "types.hpp"
#include "helper.hpp"

namespace md_hom {

template <unsigned int L_DIMS, unsigned int R_DIMS, typename TOs, typename... TIs>
class md_hom_class {
public:
    md_hom_class(const std::string              &routine_name,
                 const std::tuple<TIs...>       &inputs,
                 const scalar_function          &scalar_function,
                 const TOs                      &outputs,
                 const std::string              &prepend_code,
                 const std::vector<dimension_t> &dimension_order) :
            _routine_name(routine_name), _inputs(inputs),
            _scalar_function(scalar_function), _outputs(outputs),
            _prepend_code(prepend_code), _dimension_order(dimension_order) {
    }

    const std::string &routine_name() const {
        return _routine_name;
    }
    const std::tuple<TIs...> &inputs() const {
        return _inputs;
    }
    const std::string &prepend_code() const {
        return _prepend_code;
    }
    const scalar_function &get_scalar_function() const {
        return _scalar_function;
    }
    const TOs &outputs() const {
        return _outputs;
    }
    const std::vector<dimension_t> dimension_order() const {
        return _dimension_order;
    }
private:
    const std::string              _routine_name;
    const std::tuple<TIs...>       _inputs;
    const scalar_function          _scalar_function;
    const TOs                      _outputs;
    const std::string              _prepend_code;
    const std::vector<dimension_t> _dimension_order;
};

template <unsigned int L_DIMS, unsigned int R_DIMS, typename... TIs, typename... TOs>
auto md_hom(const std::string        routine_name,
            const std::tuple<TIs...> &inputs,
            const scalar_function    scalar_function,
            const std::tuple<TOs...> &outputs,
            const std::string        prepend_code = "",
            const std::vector<dimension_t> &dimension_order = generator::dim_range(L_DIMS, R_DIMS)) {
    return md_hom_class<L_DIMS, R_DIMS, std::tuple<TOs...>, TIs...>(routine_name, inputs, scalar_function, outputs, prepend_code, dimension_order);
}

}


#endif //MD_HOM_GENERATOR_CONFIGURATION_CLASS_HPP
