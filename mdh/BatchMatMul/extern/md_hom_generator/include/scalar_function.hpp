//
// Created by Richard Schulze on 01.11.2017.
//

#ifndef MD_BLAS_SCALAR_FUNCTION_HPP
#define MD_BLAS_SCALAR_FUNCTION_HPP

#include <string>
#include <functional>

namespace md_hom {

class scalar_function {
public:
    explicit scalar_function(const std::function<std::string(bool)> &function_body);

    const std::string function_body(bool reduce) const;
private:
    const std::function<std::string(bool)> _function_body;
};

}

#endif // MD_BLAS_SCALAR_FUNCTION_HPP
