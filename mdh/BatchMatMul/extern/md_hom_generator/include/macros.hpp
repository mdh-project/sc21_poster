//
// Created by Richard Schulze on 03.11.17.
//

#ifndef MD_BLAS_MACROS_HPP
#define MD_BLAS_MACROS_HPP

namespace md_hom {
namespace generator {

#include <string>

#include "types.hpp"

template <unsigned int L_DIMS, unsigned int R_DIMS>
class macros {
public:
    explicit macros(bool runtime_inputs) : _runtime_inputs(runtime_inputs) {
        // functional unit id
        for (unsigned int kernel = 1; kernel <= 2; ++kernel) {
            for (LEVEL level : {LEVEL::GLOBAL, LEVEL::LOCAL, LEVEL::PRIVATE}) {
                for (DIM_TYPE dim_type : {DIM_TYPE::L, DIM_TYPE::R}) {
                    for (unsigned int dim_nr = 1; dim_nr <= (dim_type == DIM_TYPE::L ? L_DIMS : R_DIMS); ++dim_nr) {
                        std::string name = stringf("K%d_%c_FU_ID_%c_%d", kernel, level, dim_type, dim_nr);
                        std::string def = stringf("#define %s %s", name,
                                                  level == LEVEL::PRIVATE
                                                  ? "0"
                                                  : stringf("i_%s_%c_%d", level == LEVEL::LOCAL ? "wi" : "wg", lower_case(dim_type), dim_nr));
                        resolve_helper(_fu_id, kernel, level, {dim_type, dim_nr}) = name;
                        resolve_helper(_def_fu_id, kernel, level, {dim_type, dim_nr}) = def;
                    }
                }
            }
        }

        // number of functional units
        for (unsigned int kernel = 1; kernel <= 2; ++kernel) {
            for (LEVEL level : {LEVEL::GLOBAL, LEVEL::LOCAL, LEVEL::PRIVATE}) {
                for (DIM_TYPE dim_type : {DIM_TYPE::L, DIM_TYPE::R}) {
                    for (unsigned int dim_nr = 1; dim_nr <= (dim_type == DIM_TYPE::L ? L_DIMS : R_DIMS); ++dim_nr) {
                        std::string name = stringf("K%d_%c_NUM_FU_%c_%d", kernel, level, dim_type, dim_nr);
                        std::string def;
                        if (kernel == 1 || dim_type == DIM_TYPE::L || level == LEVEL::LOCAL) {
                            def = stringf("#define %s %s", name,
                                                      level == LEVEL::PRIVATE
                                                      ? "1"
                                                      : stringf("NUM_%s_%c_%d", level == LEVEL::GLOBAL ? "WG" : "WI", dim_type, dim_nr));
                        } else {
                            def = stringf("#define %s 1", name);
                        }
                        resolve_helper(_num_fu, kernel, level, {dim_type, dim_nr}) = name;
                        resolve_helper(_def_num_fu, kernel, level, {dim_type, dim_nr}) = def;
                    }
                }
            }
        }

        // ---------- initialize macro names ----------
        // cache block size
        for (unsigned int kernel = 1; kernel <= 2; ++kernel) {
            for (LEVEL level : {LEVEL::GLOBAL, LEVEL::LOCAL, LEVEL::PRIVATE}) {
                for (DIM_TYPE dim_type : {DIM_TYPE::L, DIM_TYPE::R}) {
                    for (unsigned int dim_nr = 1; dim_nr <= (dim_type == DIM_TYPE::L ? L_DIMS : R_DIMS); ++dim_nr) {
                        std::string name = stringf("K%d_%c_CB_SIZE_%c_%d", kernel, level, dim_type, dim_nr);
                        resolve_helper(_cb_size, kernel, level, {dim_type, dim_nr}) = name;
                    }
                }
            }
        }

        // number of steps per FU
        for (unsigned int kernel = 1; kernel <= 2; ++kernel) {
            for (LEVEL level : {LEVEL::GLOBAL, LEVEL::LOCAL, LEVEL::PRIVATE}) {
                for (DIM_TYPE dim_type : {DIM_TYPE::L, DIM_TYPE::R}) {
                    for (unsigned int dim_nr = 1; dim_nr <= (dim_type == DIM_TYPE::L ? L_DIMS : R_DIMS); ++dim_nr) {
                        std::string name = stringf("K%d_%c_NUM_STEPS_%c_%d", kernel, level, dim_type, dim_nr);
                        resolve_helper(_num_steps, kernel, level, {dim_type, dim_nr}) = name;
                    }
                }
            }
        }

        // number of cached iterations per FU
        for (unsigned int kernel = 1; kernel <= 2; ++kernel) {
            for (LEVEL level : {LEVEL::GLOBAL, LEVEL::LOCAL, LEVEL::PRIVATE}) {
                for (DIM_TYPE dim_type : {DIM_TYPE::L, DIM_TYPE::R}) {
                    for (unsigned int dim_nr = 1; dim_nr <= (dim_type == DIM_TYPE::L ? L_DIMS : R_DIMS); ++dim_nr) {
                        std::string name = stringf("K%d_%c_NUM_CACHED_ITERATIONS_%c_%d", kernel, level, dim_type, dim_nr);
                        resolve_helper(_num_cached_iterations, kernel, level, {dim_type, dim_nr}) = name;
                    }
                }
            }
        }

        // number of extra cached iterations per FU
        for (unsigned int kernel = 1; kernel <= 2; ++kernel) {
            for (LEVEL level : {LEVEL::GLOBAL, LEVEL::LOCAL, LEVEL::PRIVATE}) {
                for (DIM_TYPE dim_type : {DIM_TYPE::L, DIM_TYPE::R}) {
                    for (unsigned int dim_nr = 1; dim_nr <= (dim_type == DIM_TYPE::L ? L_DIMS : R_DIMS); ++dim_nr) {
                        std::string name = stringf("K%d_%c_NUM_EXTRA_CACHED_ITERATIONS_%c_%d", kernel, level, dim_type, dim_nr);
                        resolve_helper(_num_extra_cached_iterations, kernel, level, {dim_type, dim_nr}) = name;
                    }
                }
            }
        }

        // number of extra elements
        for (unsigned int kernel = 1; kernel <= 2; ++kernel) {
            for (LEVEL level : {LEVEL::GLOBAL, LEVEL::LOCAL, LEVEL::PRIVATE}) {
                for (DIM_TYPE dim_type : {DIM_TYPE::L, DIM_TYPE::R}) {
                    for (unsigned int dim_nr = 1; dim_nr <= (dim_type == DIM_TYPE::L ? L_DIMS : R_DIMS); ++dim_nr) {
                        std::string name = stringf("K%d_%c_NUM_EXTRA_ELEMENTS_%c_%d", kernel, level, dim_type, dim_nr);
                        resolve_helper(_num_extra_elements, kernel, level, {dim_type, dim_nr}) = name;
                    }
                }
            }
        }

        // number of processed elements
        for (unsigned int kernel = 1; kernel <= 2; ++kernel) {
            for (LEVEL level : {LEVEL::GLOBAL, LEVEL::LOCAL, LEVEL::PRIVATE}) {
                for (DIM_TYPE dim_type : {DIM_TYPE::L, DIM_TYPE::R}) {
                    for (unsigned int dim_nr = 1; dim_nr <= (dim_type == DIM_TYPE::L ? L_DIMS : R_DIMS); ++dim_nr) {
                        std::string name = stringf("K%d_%c_NUM_PROCESSED_ELEMENTS_%c_%d", kernel, level, dim_type, dim_nr);
                        resolve_helper(_num_processed_elements, kernel, level, {dim_type, dim_nr}) = name;
                    }
                }
            }
        }

        // incomplete cache block size
        for (unsigned int kernel = 1; kernel <= 2; ++kernel) {
            for (LEVEL cb_size_level : {LEVEL::PRIVATE, LEVEL::LOCAL, LEVEL::GLOBAL}) {
                for (LEVEL first_complete_level : parent_levels(cb_size_level, false, ORDER::ASCENDING)) {
                    for (DIM_TYPE dim_type : {DIM_TYPE::L, DIM_TYPE::R}) {
                        for (unsigned int dim_nr = 1; dim_nr <= (dim_type == DIM_TYPE::L ? L_DIMS : R_DIMS); ++dim_nr) {
                            std::string name = stringf("K%d_%c_INCOMPLETE_CB_SIZE_IN_COMPLETE_%c_CB_%c_%d", kernel, cb_size_level, first_complete_level, dim_type, dim_nr);
                            resolve_helper(_incomplete_cb_size, kernel, cb_size_level, {dim_type, dim_nr}, first_complete_level) = name;
                        }
                    }
                }
            }
        }

        // number of steps per FU in incomplete cache blocks
        for (unsigned int kernel = 1; kernel <= 2; ++kernel) {
            for (LEVEL level : {LEVEL::PRIVATE, LEVEL::LOCAL}) {
                for (LEVEL first_complete_level : parent_levels(parent_level(level), false, ORDER::ASCENDING)) {
                    for (DIM_TYPE dim_type : {DIM_TYPE::L, DIM_TYPE::R}) {
                        for (unsigned int dim_nr = 1; dim_nr <= (dim_type == DIM_TYPE::L ? L_DIMS : R_DIMS); ++dim_nr) {
                            std::string name = stringf("K%d_%c_NUM_STEPS_IN_COMPLETE_%c_CB_%c_%d", kernel, level, first_complete_level, dim_type, dim_nr);
                            resolve_helper(_num_steps_in_incomplete_cb, kernel, level, {dim_type, dim_nr}, first_complete_level) = name;
                        }
                    }
                }
            }
        }

        // number of cached iterations per FU in incomplete cache blocks
        for (unsigned int kernel = 1; kernel <= 2; ++kernel) {
            for (LEVEL level : {LEVEL::PRIVATE, LEVEL::LOCAL}) {
                for (LEVEL first_complete_level : parent_levels(level, false, ORDER::ASCENDING)) {
                    for (DIM_TYPE dim_type : {DIM_TYPE::L, DIM_TYPE::R}) {
                        for (unsigned int dim_nr = 1; dim_nr <= (dim_type == DIM_TYPE::L ? L_DIMS : R_DIMS); ++dim_nr) {
                            std::string name = stringf("K%d_%c_NUM_CACHED_ITERATIONS_IN_COMPLETE_%c_CB_%c_%d", kernel, level, first_complete_level, dim_type, dim_nr);
                            resolve_helper(_num_cached_iterations_in_incomplete_cb, kernel, level, {dim_type, dim_nr}, first_complete_level) = name;
                        }
                    }
                }
            }
        }

        // number of extra cached iterations per FU in incomplete cache blocks
        for (unsigned int kernel = 1; kernel <= 2; ++kernel) {
            for (LEVEL level : {LEVEL::PRIVATE, LEVEL::LOCAL}) {
                for (LEVEL first_complete_level : parent_levels(parent_level(level), false, ORDER::ASCENDING)) {
                    for (DIM_TYPE dim_type : {DIM_TYPE::L, DIM_TYPE::R}) {
                        for (unsigned int dim_nr = 1; dim_nr <= (dim_type == DIM_TYPE::L ? L_DIMS : R_DIMS); ++dim_nr) {
                            std::string name = stringf("K%d_%c_NUM_EXTRA_CACHED_ITERATIONS_IN_COMPLETE_%c_CB_%c_%d", kernel, level, first_complete_level,  dim_type, dim_nr);
                            resolve_helper(_num_extra_cached_iterations_in_incomplete_cb, kernel, level, {dim_type, dim_nr}, first_complete_level) = name;
                        }
                    }
                }
            }
        }

        // number of extra elements in incomplete cache blocks
        for (unsigned int kernel = 1; kernel <= 2; ++kernel) {
            for (LEVEL level : {LEVEL::PRIVATE, LEVEL::LOCAL}) {
                for (LEVEL first_complete_level : parent_levels(parent_level(level), false, ORDER::ASCENDING)) {
                    for (DIM_TYPE dim_type : {DIM_TYPE::L, DIM_TYPE::R}) {
                        for (unsigned int dim_nr = 1; dim_nr <= (dim_type == DIM_TYPE::L ? L_DIMS : R_DIMS); ++dim_nr) {
                            std::string name = stringf("K%d_%c_NUM_EXTRA_ELEMENTS_IN_COMPLETE_%c_CB_%c_%d", kernel, level, first_complete_level,  dim_type, dim_nr);
                            resolve_helper(_num_extra_elements_in_incomplete_cb, kernel, level, {dim_type, dim_nr}, first_complete_level) = name;
                        }
                    }
                }
            }
        }

        // number of processed elements in incomplete cache blocks
        for (unsigned int kernel = 1; kernel <= 2; ++kernel) {
            for (LEVEL level : {LEVEL::PRIVATE, LEVEL::LOCAL}) {
                for (LEVEL first_complete_level : parent_levels(level, false, ORDER::ASCENDING)) {
                    for (DIM_TYPE dim_type : {DIM_TYPE::L, DIM_TYPE::R}) {
                        for (unsigned int dim_nr = 1; dim_nr <= (dim_type == DIM_TYPE::L ? L_DIMS : R_DIMS); ++dim_nr) {
                            std::string name = stringf("K%d_%c_NUM_PROCESSED_ELEMENTS_IN_COMPLETE_%c_CB_%c_%d", kernel, level, first_complete_level,  dim_type, dim_nr);
                            resolve_helper(_num_processed_elements_in_incomplete_cb, kernel, level, {dim_type, dim_nr}, first_complete_level) = name;
                        }
                    }
                }
            }
        }
        // ---------- end of initialize macro names ----------

        // cache block size
        for (unsigned int kernel = 1; kernel <= 2; ++kernel) {
            for (LEVEL level : {LEVEL::GLOBAL, LEVEL::LOCAL, LEVEL::PRIVATE}) {
                for (DIM_TYPE dim_type : {DIM_TYPE::L, DIM_TYPE::R}) {
                    for (unsigned int dim_nr = 1; dim_nr <= (dim_type == DIM_TYPE::L ? L_DIMS : R_DIMS); ++dim_nr) {
                        std::string def;
                        if (level == LEVEL::GLOBAL) {
                            if (kernel == 1 || dim_type == DIM_TYPE::L) {
                                def = stringf("#define %s INPUT_SIZE_%c_%d",
                                              cb_size(kernel, level, {dim_type, dim_nr}),
                                              dim_type, dim_nr
                                );
                            } else {
                                def = stringf("#define %s NUM_WG_R_%d", cb_size(kernel, level, {dim_type, dim_nr}), dim_nr);
                            }
                        } else {
                            def = stringf("#define %s %c_CB_SIZE_%c_%d",
                                          cb_size(kernel, level, {dim_type, dim_nr}),
                                          level, dim_type, dim_nr
                            );
                        }
                        resolve_helper(_def_cb_size, kernel, level, {dim_type, dim_nr}) = def;
                    }
                }
            }
        }

        // number of steps per FU
        for (unsigned int kernel = 1; kernel <= 2; ++kernel) {
            for (LEVEL level : {LEVEL::GLOBAL, LEVEL::LOCAL, LEVEL::PRIVATE}) {
                for (DIM_TYPE dim_type : {DIM_TYPE::L, DIM_TYPE::R}) {
                    for (unsigned int dim_nr = 1; dim_nr <= (dim_type == DIM_TYPE::L ? L_DIMS : R_DIMS); ++dim_nr) {
                        std::string def = stringf("#define %s %s", num_steps(kernel, level, {dim_type, dim_nr}),
                                                  level == LEVEL::GLOBAL
                                                  ? stringf("1")
                                                  : stringf("(%s / %s)",
                                                            num_cached_iterations(kernel, parent_level(level), {dim_type, dim_nr}),
                                                            num_cached_iterations(kernel, level, {dim_type, dim_nr})));
                        resolve_helper(_def_num_steps, kernel, level, {dim_type, dim_nr}) = def;
                    }
                }
            }
        }

        // number of cached iterations per FU
        for (unsigned int kernel = 1; kernel <= 2; ++kernel) {
            for (LEVEL level : {LEVEL::GLOBAL, LEVEL::LOCAL, LEVEL::PRIVATE}) {
                for (DIM_TYPE dim_type : {DIM_TYPE::L, DIM_TYPE::R}) {
                    for (unsigned int dim_nr = 1; dim_nr <= (dim_type == DIM_TYPE::L ? L_DIMS : R_DIMS); ++dim_nr) {
                        std::string def = stringf("#define %s %s", num_cached_iterations(kernel, level, {dim_type, dim_nr}), stringf(
                                "(%s%s)",
                                cb_size(kernel, level, {dim_type, dim_nr}),
                                (level == LEVEL::GLOBAL ? stringf(" / GET_GLOBAL_SIZE_%c_%d", dim_type, dim_nr)
                                                        : (level == LEVEL::LOCAL ? stringf(" / GET_LOCAL_SIZE_%c_%d", dim_type, dim_nr)
                                                                                 : ""))
                        ));
                        resolve_helper(_def_num_cached_iterations, kernel, level, {dim_type, dim_nr}) = def;
                    }
                }
            }
        }

        // number of extra cached iterations per FU
        for (unsigned int kernel = 1; kernel <= 2; ++kernel) {
            for (LEVEL level : {LEVEL::GLOBAL, LEVEL::LOCAL, LEVEL::PRIVATE}) {
                for (DIM_TYPE dim_type : {DIM_TYPE::L, DIM_TYPE::R}) {
                    for (unsigned int dim_nr = 1; dim_nr <= (dim_type == DIM_TYPE::L ? L_DIMS : R_DIMS); ++dim_nr) {
                        std::string def;
                        if (level == LEVEL::GLOBAL) {
                            def = stringf("#define %s 0", num_extra_cached_iterations(kernel, level, {dim_type, dim_nr}));
                        } else {
                            def = stringf("#define %s (%s %% (GET_%s_SIZE_%c_%d * %s) / GET_%s_SIZE_%c_%d)", num_extra_cached_iterations(kernel, level, {dim_type, dim_nr}),
                                          level == LEVEL::LOCAL ? cb_size(kernel, parent_level(level), {dim_type, dim_nr}) : stringf("(GET_LOCAL_SIZE_%c_%d * %s)", dim_type, dim_nr, num_cached_iterations(kernel, parent_level(level), {dim_type, dim_nr})),
                                          level == LEVEL::LOCAL ? "GLOBAL" : "LOCAL", dim_type, dim_nr,
                                          num_cached_iterations(kernel, level, {dim_type, dim_nr}),
                                          level == LEVEL::LOCAL ? "GLOBAL" : "LOCAL", dim_type, dim_nr);
                        }
                        resolve_helper(_def_num_extra_cached_iterations, kernel, level, {dim_type, dim_nr}) = def;
                    }
                }
            }
        }

        // number of extra elements
        for (unsigned int kernel = 1; kernel <= 2; ++kernel) {
            for (LEVEL level : {LEVEL::GLOBAL, LEVEL::LOCAL, LEVEL::PRIVATE}) {
                for (DIM_TYPE dim_type : {DIM_TYPE::L, DIM_TYPE::R}) {
                    for (unsigned int dim_nr = 1; dim_nr <= (dim_type == DIM_TYPE::L ? L_DIMS : R_DIMS); ++dim_nr) {
                        std::string def;
                        if (level == LEVEL::GLOBAL) {
                            def = stringf("#define %s 0", num_extra_elements(kernel, level, {dim_type, dim_nr}));
                        } else {
                            def = stringf("#define %s (%s %% (GET_%s_SIZE_%c_%d * %s) %% GET_%s_SIZE_%c_%d)", num_extra_elements(kernel, level, {dim_type, dim_nr}),
                                          level == LEVEL::LOCAL ? cb_size(kernel, parent_level(level), {dim_type, dim_nr}) : stringf("(GET_LOCAL_SIZE_%c_%d * %s)", dim_type, dim_nr, num_cached_iterations(kernel, parent_level(level), {dim_type, dim_nr})),
                                          level == LEVEL::LOCAL ? "GLOBAL" : "LOCAL", dim_type, dim_nr,
                                          num_cached_iterations(kernel, level, {dim_type, dim_nr}),
                                          level == LEVEL::LOCAL ? "GLOBAL" : "LOCAL", dim_type, dim_nr);
                        }
                        resolve_helper(_def_num_extra_elements, kernel, level, {dim_type, dim_nr}) = def;
                    }
                }
            }
        }

        // number of processed elements
        for (unsigned int kernel = 1; kernel <= 2; ++kernel) {
            for (LEVEL level : {LEVEL::GLOBAL, LEVEL::LOCAL, LEVEL::PRIVATE}) {
                for (DIM_TYPE dim_type : {DIM_TYPE::L, DIM_TYPE::R}) {
                    for (unsigned int dim_nr = 1; dim_nr <= (dim_type == DIM_TYPE::L ? L_DIMS : R_DIMS); ++dim_nr) {
                        std::string def;
                        if (level == LEVEL::PRIVATE) {
                            def = stringf("#define %s %s",
                                          num_processed_elements(kernel, level, {dim_type, dim_nr}),
                                          num_cached_iterations(kernel, level, {dim_type, dim_nr})
                            );
                        } else {
                            def = stringf("#define %s ((%s * %s + %s) * GET_%s_SIZE_%c_%d + %s)",
                                          num_processed_elements(kernel, level, {dim_type, dim_nr}),
                                          num_steps(kernel, sub_level(level), {dim_type, dim_nr}),
                                          num_cached_iterations(kernel, sub_level(level), {dim_type, dim_nr}),
                                          num_extra_cached_iterations(kernel, sub_level(level), {dim_type, dim_nr}),
                                          level == LEVEL::GLOBAL ? "GLOBAL" : "LOCAL", dim_type, dim_nr,
                                          num_extra_elements(kernel, sub_level(level), {dim_type, dim_nr})
                            );
                        }

                        resolve_helper(_def_num_processed_elements, kernel, level, {dim_type, dim_nr}) = def;
                    }
                }
            }
        }

        // incomplete cache block size
        for (unsigned int kernel = 1; kernel <= 2; ++kernel) {
            for (LEVEL cb_size_level : {LEVEL::PRIVATE, LEVEL::LOCAL, LEVEL::GLOBAL}) {
                for (LEVEL first_complete_level : parent_levels(cb_size_level, false, ORDER::ASCENDING)) {
                    for (DIM_TYPE dim_type : {DIM_TYPE::L, DIM_TYPE::R}) {
                        for (unsigned int dim_nr = 1; dim_nr <= (dim_type == DIM_TYPE::L ? L_DIMS : R_DIMS); ++dim_nr) {
                            std::string def = stringf("#define %s (%s%s)", incomplete_cb_size(kernel, cb_size_level, {dim_type, dim_nr}, first_complete_level),
                                                      parent_level(cb_size_level) == first_complete_level
                                                      ? num_extra_cached_iterations(kernel, cb_size_level, {dim_type, dim_nr})
                                                      : num_extra_cached_iterations_in_incomplete_cb(kernel, cb_size_level, {dim_type, dim_nr}, first_complete_level),
                                                      cb_size_level == LEVEL::LOCAL ? stringf(" * GET_LOCAL_SIZE_%c_%d", dim_type, dim_nr) : ""
                            );

                            resolve_helper(_def_incomplete_cb_size, kernel, cb_size_level, {dim_type, dim_nr}, first_complete_level) = def;
                        }
                    }
                }
            }
        }

        // number of steps per FU in incomplete cache blocks
        for (unsigned int kernel = 1; kernel <= 2; ++kernel) {
            for (LEVEL level : {LEVEL::PRIVATE, LEVEL::LOCAL}) {
                for (LEVEL first_complete_level : parent_levels(parent_level(level), false, ORDER::ASCENDING)) {
                    for (DIM_TYPE dim_type : {DIM_TYPE::L, DIM_TYPE::R}) {
                        for (unsigned int dim_nr = 1; dim_nr <= (dim_type == DIM_TYPE::L ? L_DIMS : R_DIMS); ++dim_nr) {
                            std::string def = stringf("#define %s %s", num_steps_in_incomplete_cb(kernel, level, {dim_type, dim_nr}, first_complete_level),
                                                      stringf("(%s / %s)",
                                                              num_cached_iterations_in_incomplete_cb(kernel, parent_level(level), {dim_type, dim_nr}, first_complete_level),
                                                              num_cached_iterations(kernel, level, {dim_type, dim_nr})));
                            resolve_helper(_def_num_steps_in_incomplete_cb, kernel, level, {dim_type, dim_nr}, first_complete_level) = def;
                        }
                    }
                }
            }
        }

        // number of cached iterations per FU in incomplete cache blocks
        for (unsigned int kernel = 1; kernel <= 2; ++kernel) {
            for (LEVEL level : {LEVEL::PRIVATE, LEVEL::LOCAL}) {
                for (LEVEL first_complete_level : parent_levels(level, false, ORDER::ASCENDING)) {
                    for (DIM_TYPE dim_type : {DIM_TYPE::L, DIM_TYPE::R}) {
                        for (unsigned int dim_nr = 1; dim_nr <= (dim_type == DIM_TYPE::L ? L_DIMS : R_DIMS); ++dim_nr) {
                            std::string def = stringf("#define %s (%s%s)", num_cached_iterations_in_incomplete_cb(kernel, level, {dim_type, dim_nr}, first_complete_level),
                                                      incomplete_cb_size(kernel, level, {dim_type, dim_nr}, first_complete_level),
                                                      level == LEVEL::LOCAL ? stringf(" / GET_LOCAL_SIZE_%c_%d", dim_type, dim_nr) : "");
                            resolve_helper(_def_num_cached_iterations_in_incomplete_cb, kernel, level, {dim_type, dim_nr}, first_complete_level) = def;
                        }
                    }
                }
            }
        }

        // number of extra cached iterations per FU in incomplete cache blocks
        for (unsigned int kernel = 1; kernel <= 2; ++kernel) {
            for (LEVEL level : {LEVEL::PRIVATE, LEVEL::LOCAL}) {
                for (LEVEL first_complete_level : parent_levels(parent_level(level), false, ORDER::ASCENDING)) {
                    for (DIM_TYPE dim_type : {DIM_TYPE::L, DIM_TYPE::R}) {
                        for (unsigned int dim_nr = 1; dim_nr <= (dim_type == DIM_TYPE::L ? L_DIMS : R_DIMS); ++dim_nr) {
                            std::string def = stringf("#define %s %s", num_extra_cached_iterations_in_incomplete_cb(kernel, level, {dim_type, dim_nr}, first_complete_level),
                                                      stringf("((GET_%s_SIZE_%c_%d * %s) %% (GET_%s_SIZE_%c_%d * %s) / GET_%s_SIZE_%c_%d)",
                                                              level == LEVEL::PRIVATE ? "LOCAL" : "GLOBAL", dim_type, dim_nr,
                                                              parent_level(level) == first_complete_level
                                                              ? num_cached_iterations(kernel, parent_level(level), {dim_type, dim_nr})
                                                              : num_cached_iterations_in_incomplete_cb(kernel, parent_level(level), {dim_type, dim_nr}, first_complete_level),
                                                              level == LEVEL::PRIVATE ? "LOCAL" : "GLOBAL", dim_type, dim_nr,
                                                              num_cached_iterations(kernel, level, {dim_type, dim_nr}),
                                                              level == LEVEL::PRIVATE ? "LOCAL" : "GLOBAL", dim_type, dim_nr));
                            resolve_helper(_def_num_extra_cached_iterations_in_incomplete_cb, kernel, level, {dim_type, dim_nr}, first_complete_level) = def;
                        }
                    }
                }
            }
        }

        // number of extra elements in incomplete cache blocks
        for (unsigned int kernel = 1; kernel <= 2; ++kernel) {
            for (LEVEL level : {LEVEL::PRIVATE, LEVEL::LOCAL}) {
                for (LEVEL first_complete_level : parent_levels(parent_level(level), false, ORDER::ASCENDING)) {
                    for (DIM_TYPE dim_type : {DIM_TYPE::L, DIM_TYPE::R}) {
                        for (unsigned int dim_nr = 1; dim_nr <= (dim_type == DIM_TYPE::L ? L_DIMS : R_DIMS); ++dim_nr) {
                            std::string def = stringf("#define %s %s", num_extra_elements_in_incomplete_cb(kernel, level, {dim_type, dim_nr}, first_complete_level),
                                                      stringf("((GET_%s_SIZE_%c_%d * %s) %% (GET_%s_SIZE_%c_%d * %s) %% GET_%s_SIZE_%c_%d)",
                                                              level == LEVEL::PRIVATE ? "LOCAL" : "GLOBAL", dim_type, dim_nr,
                                                              parent_level(level) == first_complete_level
                                                              ? num_cached_iterations(kernel, parent_level(level), {dim_type, dim_nr})
                                                              : num_cached_iterations_in_incomplete_cb(kernel, parent_level(level), {dim_type, dim_nr}, first_complete_level),
                                                              level == LEVEL::PRIVATE ? "LOCAL" : "GLOBAL", dim_type, dim_nr,
                                                              num_cached_iterations(kernel, level, {dim_type, dim_nr}),
                                                              level == LEVEL::PRIVATE ? "LOCAL" : "GLOBAL", dim_type, dim_nr));
                            resolve_helper(_def_num_extra_elements_in_incomplete_cb, kernel, level, {dim_type, dim_nr}, first_complete_level) = def;
                        }
                    }
                }
            }
        }

        // number of processed elements in incomplete cache blocks
        for (unsigned int kernel = 1; kernel <= 2; ++kernel) {
            for (LEVEL level : {LEVEL::PRIVATE, LEVEL::LOCAL}) {
                for (LEVEL first_complete_level : parent_levels(level, false, ORDER::ASCENDING)) {
                    for (DIM_TYPE dim_type : {DIM_TYPE::L, DIM_TYPE::R}) {
                        for (unsigned int dim_nr = 1; dim_nr <= (dim_type == DIM_TYPE::L ? L_DIMS : R_DIMS); ++dim_nr) {
                            std::string def;
                            if (level == LEVEL::PRIVATE) {
                                def = stringf("#define %s %s",
                                              num_processed_elements_in_incomplete_cb(kernel, level, {dim_type, dim_nr}, first_complete_level),
                                              level == first_complete_level
                                              ? num_cached_iterations(kernel, level, {dim_type, dim_nr})
                                              : num_cached_iterations_in_incomplete_cb(kernel, level, {dim_type, dim_nr}, first_complete_level)
                                );
                            } else {
                                def = stringf("#define %s ((%s * %s + %s) * GET_%s_SIZE_%c_%d + %s)",
                                              num_processed_elements_in_incomplete_cb(kernel, level, {dim_type, dim_nr}, first_complete_level),
                                              num_steps_in_incomplete_cb(kernel, sub_level(level), {dim_type, dim_nr}, first_complete_level),
                                              num_cached_iterations(kernel, sub_level(level), {dim_type, dim_nr}),
                                              num_extra_cached_iterations_in_incomplete_cb(kernel, sub_level(level), {dim_type, dim_nr}, first_complete_level),
                                              level == LEVEL::GLOBAL ? "GLOBAL" : "LOCAL", dim_type, dim_nr,
                                              num_extra_elements_in_incomplete_cb(kernel, sub_level(level), {dim_type, dim_nr}, first_complete_level)
                                );
                            }
                            resolve_helper(_def_num_processed_elements_in_incomplete_cb, kernel, level, {dim_type, dim_nr}, first_complete_level) = def;
                        }
                    }
                }
            }
        }

        // index conversion
        for (unsigned int kernel = 1; kernel <= 2; ++kernel) {
            for (LEVEL level : {LEVEL::LOCAL, LEVEL::PRIVATE}) {
                for (DIM_TYPE dim_type : {DIM_TYPE::L, DIM_TYPE::R}) {
                    for (unsigned int dim_nr = 1; dim_nr <= (dim_type == DIM_TYPE::L ? L_DIMS : R_DIMS); ++dim_nr) {
                        std::string name = stringf_p("K%d_%c_TO_%c_INDEX_%c_%d", kernel, level, parent_level(level), dim_type, dim_nr);
                        name += "(%s)";
                        std::string def = stringf("#define %s %s", stringf(name, "i"),
                                                  stringf("((%s * %s + (i) / %s) * GET_%s_SIZE_%c_%d + %s * %s + ((i) %% %s))",
                                                          stringf("%c_step_%c_%d", lower_case(level), lower_case(dim_type), dim_nr),
                                                          num_cached_iterations(kernel, level, {dim_type, dim_nr}),
                                                          num_fu(kernel, level, {dim_type, dim_nr}),
                                                          level == LEVEL::LOCAL ? "GLOBAL" : "LOCAL", dim_type, dim_nr,
                                                          fu_id(kernel, parent_level(level), {dim_type, dim_nr}),
                                                          num_fu(kernel, level, {dim_type, dim_nr}),
                                                          num_fu(kernel, level, {dim_type, dim_nr})
                                                          ));
                        resolve_helper(_index_conversion, kernel, level, {dim_type, dim_nr}) = name;
                        resolve_helper(_def_index_conversion, kernel, level, {dim_type, dim_nr}) = def;
                    }
                }
            }
        }

        // index conversion in phase 3
        for (unsigned int kernel = 1; kernel <= 2; ++kernel) {
            for (LEVEL level : {LEVEL::LOCAL, LEVEL::PRIVATE}) {
                for (DIM_TYPE dim_type : {DIM_TYPE::L, DIM_TYPE::R}) {
                    for (unsigned int dim_nr = 1; dim_nr <= (dim_type == DIM_TYPE::L ? L_DIMS : R_DIMS); ++dim_nr) {
                        for (bool my_level_in_phase3 : level == LEVEL::PRIVATE ? V(true, false) : V(true)) {
                            for (bool parent_level_complete : ((level == LEVEL::PRIVATE && my_level_in_phase3) ? V(true, false) : V(true))) {
                                std::string name = stringf_p("K%d_%sP3_%c_TO_%c_INDEX%s_%c_%d",
                                                             kernel, my_level_in_phase3 ? "" : "P",
                                                             level, parent_level(level),
                                                             parent_level_complete ? "" : stringf("_IN_COMPLETE_%c_CB", parent_level(parent_level(level))),
                                                             dim_type, dim_nr);
                                name += "(%s)";
                                std::string def = stringf("#define %s %s", stringf(name, "i"),
                                                          stringf("(%s%s * %s + (i))",
                                                                  my_level_in_phase3
                                                                  ? stringf("%s / GET_%s_SIZE_%c_%d * GET_%s_SIZE_%c_%d + ",
                                                                            parent_level_complete
                                                                            ? cb_size(kernel, parent_level(level), {dim_type, dim_nr})
                                                                            : incomplete_cb_size(kernel, parent_level(level), {dim_type, dim_nr}, parent_level(parent_level(level))),
                                                                            level == LEVEL::LOCAL ? "GLOBAL" : "LOCAL", dim_type, dim_nr,
                                                                            level == LEVEL::LOCAL ? "GLOBAL" : "LOCAL", dim_type, dim_nr)
                                                                  : "",
                                                                  fu_id(kernel, parent_level(level), {dim_type, dim_nr}),
                                                                  num_fu(kernel, level, {dim_type, dim_nr})
                                                          ));
                                resolve_helper(_index_conversion_in_phase3, kernel, level, {dim_type, dim_nr}, parent_level_complete, my_level_in_phase3) = name;
                                resolve_helper(_def_index_conversion_in_phase_3, kernel, level, {dim_type, dim_nr}, parent_level_complete, my_level_in_phase3) = def;
                            }
                        }
                    }
                }
            }
        }

        // index conversion per FU
        for (unsigned int kernel = 1; kernel <= 2; ++kernel) {
            for (LEVEL level : {LEVEL::LOCAL, LEVEL::PRIVATE}) {
                for (DIM_TYPE dim_type : {DIM_TYPE::L, DIM_TYPE::R}) {
                    for (unsigned int dim_nr = 1; dim_nr <= (dim_type == DIM_TYPE::L ? L_DIMS : R_DIMS); ++dim_nr) {
                        std::string name = stringf_p("K%d_FU_%c_TO_%c_INDEX_%c_%d", kernel, level, parent_level(level), dim_type, dim_nr);
                        name += "(%s)";
                        std::string def = stringf("#define %s %s", stringf(name, "i"),
                                                  stringf("(%s + ((i) / %s) * GET_%s_SIZE_%c_%d + %s)",
                                                          stringf("%c_cb_offset_%c_%d", lower_case(level), lower_case(dim_type), dim_nr),
//                                                          num_cached_iterations(kernel, level, {dim_type, dim_nr}),
                                                          num_fu(kernel, level, {dim_type, dim_nr}),
                                                          level == LEVEL::LOCAL ? "GLOBAL" : "LOCAL", dim_type, dim_nr,
//                                                          fu_id(kernel, parent_level(level), {dim_type, dim_nr}),
//                                                          num_fu(kernel, level, {dim_type, dim_nr}),
                                                          fu_id(kernel, level, {dim_type, dim_nr})
                                                  ));
                        resolve_helper(_fu_index_conversion, kernel, level, {dim_type, dim_nr}) = name;
                        resolve_helper(_def_fu_index_conversion, kernel, level, {dim_type, dim_nr}) = def;
                    }
                }
            }
        }

        // index conversion per FU in phase 3
        for (unsigned int kernel = 1; kernel <= 2; ++kernel) {
            for (LEVEL level : {LEVEL::LOCAL, LEVEL::PRIVATE}) {
                for (DIM_TYPE dim_type : {DIM_TYPE::L, DIM_TYPE::R}) {
                    for (unsigned int dim_nr = 1; dim_nr <= (dim_type == DIM_TYPE::L ? L_DIMS : R_DIMS); ++dim_nr) {
                        for (bool my_level_in_phase3 : level == LEVEL::PRIVATE ? V(true, false) : V(true)) {
                            for (bool parent_level_complete : ((level == LEVEL::PRIVATE && my_level_in_phase3) ? V(true, false) : V(true))) {
                                std::string name = stringf_p("K%d_FU_%sP3_%c_TO_%c_INDEX%s_%c_%d",
                                                             kernel, my_level_in_phase3 ? "" : "P",
                                                             level, parent_level(level),
                                                             parent_level_complete ? "" : stringf("_IN_COMPLETE_%c_CB", parent_level(parent_level(level))),
                                                             dim_type, dim_nr);
                                name += "(%s)";
                                std::string def = stringf("#define %s (%c_cb_offset_%c_%d + (i))",
                                                          stringf(name, "i"),
                                                          lower_case(level), lower_case(dim_type), dim_nr
                                );
                                resolve_helper(_fu_index_conversion_in_phase3, kernel, level, {dim_type, dim_nr}, parent_level_complete, my_level_in_phase3) = name;
                                resolve_helper(_def_fu_index_conversion_in_phase_3, kernel, level, {dim_type, dim_nr}, parent_level_complete, my_level_in_phase3) = def;
                            }
                        }
                    }
                }
            }
        }

        // iteration to index conversion
        for (unsigned int kernel = 1; kernel <= 2; ++kernel) {
            for (LEVEL level : {LEVEL::LOCAL, LEVEL::PRIVATE}) {
                for (DIM_TYPE dim_type : {DIM_TYPE::L, DIM_TYPE::R}) {
                    for (unsigned int dim_nr = 1; dim_nr <= (dim_type == DIM_TYPE::L ? L_DIMS : R_DIMS); ++dim_nr) {
                        std::string name = stringf_p("K%d_%c_ITERATION_TO_%c_INDEX_%c_%d", kernel, level, level, dim_type, dim_nr);
                        name += "(%s)";
                        std::string def;
                        if (level == LEVEL::PRIVATE) {
                            def = stringf("#define %s (i)", stringf(name, "i"));
                        } else {
                            def = stringf("#define %s %s", stringf(name, "i"),
                                          stringf("((i) * %s + %s)",
                                                  num_fu(kernel, level, {dim_type, dim_nr}),
                                                  fu_id(kernel, level, {dim_type, dim_nr})
                                          ));
                        }
                        resolve_helper(_iteration_to_index_conversion, kernel, level, {dim_type, dim_nr}) = name;
                        resolve_helper(_def_iteration_to_index_conversion, kernel, level, {dim_type, dim_nr}) = def;
                    }
                }
            }
        }

        // flat WI ids
        for (unsigned int kernel = 1; kernel <= 2; ++kernel) {
            for (LEVEL level : {LEVEL::GLOBAL, LEVEL::LOCAL, LEVEL::PRIVATE}) {
                std::string name = stringf("K%d_%c_FLAT_WI_ID", kernel, level);
                std::string def = stringf("#define %s (%s)", name,
                                          level == LEVEL::PRIVATE
                                          ? "0"
                                          : stringf("FLAT_INDEX_IN_DESCENDING_OCL_ORDER(%s, %s)",
                                                    concat(multi_stringf("GET_%s_ID_%c_%d", V(level == LEVEL::GLOBAL ? "GLOBAL" : "LOCAL"), dim_range_types(L_DIMS, R_DIMS), dim_range_nrs(L_DIMS, R_DIMS)), ", "),
                                                    concat(multi_stringf("GET_%s_SIZE_%c_%d", V(level == LEVEL::GLOBAL ? "GLOBAL" : "LOCAL"), dim_range_types(L_DIMS, R_DIMS), dim_range_nrs(L_DIMS, R_DIMS)), ", ")
                                          ));
                resolve_helper(_flat_wi_id, kernel, level) = name;
                resolve_helper(_def_flat_wi_id, kernel, level) = def;
            }
        }

        // flat number of WIs
        for (unsigned int kernel = 1; kernel <= 2; ++kernel) {
            for (LEVEL level : {LEVEL::GLOBAL, LEVEL::LOCAL, LEVEL::PRIVATE}) {
                std::string name = stringf("K%d_%c_FLAT_NUM_WI", kernel, level);
                std::string def = stringf("#define %s (%s)", name,
                                          level == LEVEL::PRIVATE
                                          ? "1"
                                          : level == LEVEL::LOCAL
                                            ? concat(num_fu(V(kernel), V(level), dim_range(L_DIMS, R_DIMS)), " * ")
                                            : concat(multi_stringf("%s * %s", num_fu(V(kernel), V(level), dim_range(L_DIMS, R_DIMS)), num_fu(V(kernel), V(sub_level(level)), dim_range(L_DIMS, R_DIMS))), " * "));
                resolve_helper(_flat_num_wi, kernel, level) = name;
                resolve_helper(_def_flat_num_wi, kernel, level) = def;
            }
        }
    }

    const std::string fu_id(unsigned int kernel, LEVEL level, dimension_t dimension) const {
        return resolve_helper(_fu_id, kernel, level, dimension);
    }
    const std::string num_fu(unsigned int kernel, LEVEL level, dimension_t dimension) const {
        return resolve_helper(_num_fu, kernel, level, dimension);
    }
    const std::string cb_size(unsigned int kernel, LEVEL level, dimension_t dimension) const {
        return resolve_helper(_cb_size, kernel, level, dimension);
    }
    const std::string num_steps(unsigned int kernel, LEVEL level, dimension_t dimension) const {
        return resolve_helper(_num_steps, kernel, level, dimension);
    }
    const std::string num_cached_iterations(unsigned int kernel, LEVEL level, dimension_t dimension) const {
        return resolve_helper(_num_cached_iterations, kernel, level, dimension);
    }
    const std::string num_extra_cached_iterations(unsigned int kernel, LEVEL level, dimension_t dimension) const {
        return resolve_helper(_num_extra_cached_iterations, kernel, level, dimension);
    }
    const std::string num_extra_elements(unsigned int kernel, LEVEL level, dimension_t dimension) const {
        return resolve_helper(_num_extra_elements, kernel, level, dimension);
    }
    const std::string num_processed_elements(unsigned int kernel, LEVEL level, dimension_t dimension) const {
        return resolve_helper(_num_processed_elements, kernel, level, dimension);
    }
    const std::string incomplete_cb_size(unsigned int kernel, LEVEL level, dimension_t dimension, LEVEL first_complete_level) const {
        return resolve_helper(_incomplete_cb_size, kernel, level, dimension, first_complete_level);
    }
    const std::string num_steps_in_incomplete_cb(unsigned int kernel, LEVEL level, dimension_t dimension, LEVEL first_complete_level) const {
        return resolve_helper(_num_steps_in_incomplete_cb, kernel, level, dimension, first_complete_level);
    }
    const std::string num_cached_iterations_in_incomplete_cb(unsigned int kernel, LEVEL level, dimension_t dimension, LEVEL first_complete_level) const {
        return resolve_helper(_num_cached_iterations_in_incomplete_cb, kernel, level, dimension, first_complete_level);
    }
    const std::string num_extra_cached_iterations_in_incomplete_cb(unsigned int kernel, LEVEL level, dimension_t dimension, LEVEL first_complete_level) const {
        return resolve_helper(_num_extra_cached_iterations_in_incomplete_cb, kernel, level, dimension, first_complete_level);
    }
    const std::string num_extra_elements_in_incomplete_cb(unsigned int kernel, LEVEL level, dimension_t dimension, LEVEL first_complete_level) const {
        return resolve_helper(_num_extra_elements_in_incomplete_cb, kernel, level, dimension, first_complete_level);
    }
    const std::string num_processed_elements_in_incomplete_cb(unsigned int kernel, LEVEL level, dimension_t dimension, LEVEL first_complete_level) const {
        return resolve_helper(_num_processed_elements_in_incomplete_cb, kernel, level, dimension, first_complete_level);
    }
    const std::string index_conversion(unsigned int kernel, LEVEL level, dimension_t dimension, std::string index, char *phase = nullptr) const {
        bool dim_in_phase3 = false;
        bool my_level_in_phase3 = false;
        if (phase != nullptr) {
            for (LEVEL p_level : parent_levels(level, true)) {
                if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(p_level, dimension)] == 3) {
                    dim_in_phase3 = true;
                    break;
                }
            }
            my_level_in_phase3 = phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(level, dimension)] == 3;
        }
        if (!dim_in_phase3) {
            return stringf(resolve_helper(_index_conversion, kernel, level, dimension), index);
        } else {
            bool parent_level_incomplete = (level == LEVEL::PRIVATE) && my_level_in_phase3 && (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dimension)] == 2);
            return stringf(resolve_helper(_index_conversion_in_phase3, kernel, level, dimension, !parent_level_incomplete, my_level_in_phase3), index);
        }
    }
    const std::string fu_index_conversion(unsigned int kernel, LEVEL level, dimension_t dimension, std::string index, char *phase = nullptr) const {
        bool dim_in_phase3 = false;
        bool my_level_in_phase3 = false;
        if (phase != nullptr) {
            for (LEVEL p_level : parent_levels(level, true)) {
                if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(p_level, dimension)] == 3) {
                    dim_in_phase3 = true;
                    break;
                }
            }
            my_level_in_phase3 = phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(level, dimension)] == 3;
        }
        if (!dim_in_phase3) {
            return stringf(resolve_helper(_fu_index_conversion, kernel, level, dimension), index);
        } else {
            bool parent_level_incomplete = (level == LEVEL::PRIVATE) && my_level_in_phase3 && (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dimension)] == 2);
            return stringf(resolve_helper(_fu_index_conversion_in_phase3, kernel, level, dimension, !parent_level_incomplete, my_level_in_phase3), index);
        }
    }
    const std::string iteration_to_index_conversion(unsigned int kernel, LEVEL level, dimension_t dimension, std::string index) const {
        return stringf(resolve_helper(_iteration_to_index_conversion, kernel, level, dimension), index);
    }
    const std::string flat_wi_id(unsigned int kernel, LEVEL level) const {
        return resolve_helper(_flat_wi_id, kernel, level);
    }
    const std::string flat_num_wi(unsigned int kernel, LEVEL level) const {
        return resolve_helper(_flat_num_wi, kernel, level);
    }

    const std::vector<std::string> fu_id(const std::vector<unsigned int> &kernel,
                                         const std::vector<LEVEL> &level,
                                         const std::vector<dimension_t> &dimension) const {
        std::vector<std::string> results;
        if (kernel.empty() || level.empty() || dimension.empty()) return results;
        for (size_t i = 0; i < std::max(kernel.size(), std::max(level.size(), dimension.size())); ++i) {
            results.push_back(resolve_helper(_fu_id, kernel[std::min(i, kernel.size() - 1)], level[std::min(i, level.size() - 1)], dimension[std::min(i, dimension.size() - 1)]));
        }
        return results;
    }
    const std::vector<std::string> num_fu(const std::vector<unsigned int> &kernel,
                                          const std::vector<LEVEL> &level,
                                          const std::vector<dimension_t> &dimension) const {
        std::vector<std::string> results;
        if (kernel.empty() || level.empty() || dimension.empty()) return results;
        for (size_t i = 0; i < std::max(kernel.size(), std::max(level.size(), dimension.size())); ++i) {
            results.push_back(resolve_helper(_num_fu, kernel[std::min(i, kernel.size() - 1)], level[std::min(i, level.size() - 1)], dimension[std::min(i, dimension.size() - 1)]));
        }
        return results;
    }
    const std::vector<std::string> cb_size(const std::vector<unsigned int> &kernel,
                                                  const std::vector<LEVEL> &level,
                                                  const std::vector<dimension_t> &dimension) const {
        std::vector<std::string> results;
        if (kernel.empty() || level.empty() || dimension.empty()) return results;
        for (size_t i = 0; i < std::max(kernel.size(), std::max(level.size(), dimension.size())); ++i) {
            results.push_back(resolve_helper(_cb_size, kernel[std::min(i, kernel.size() - 1)], level[std::min(i, level.size() - 1)], dimension[std::min(i, dimension.size() - 1)]));
        }
        return results;
    }
    const std::vector<std::string> num_steps(const std::vector<unsigned int> &kernel,
                                             const std::vector<LEVEL> &level,
                                             const std::vector<dimension_t> &dimension) const {
        std::vector<std::string> results;
        if (kernel.empty() || level.empty() || dimension.empty()) return results;
        for (size_t i = 0; i < std::max(kernel.size(), std::max(level.size(), dimension.size())); ++i) {
            results.push_back(resolve_helper(_num_steps, kernel[std::min(i, kernel.size() - 1)], level[std::min(i, level.size() - 1)], dimension[std::min(i, dimension.size() - 1)]));
        }
        return results;
    }
    const std::vector<std::string> num_cached_iterations(const std::vector<unsigned int> &kernel,
                                                         const std::vector<LEVEL> &level,
                                                         const std::vector<dimension_t> &dimension) const {
        std::vector<std::string> results;
        if (kernel.empty() || level.empty() || dimension.empty()) return results;
        for (size_t i = 0; i < std::max(kernel.size(), std::max(level.size(), dimension.size())); ++i) {
            results.push_back(resolve_helper(_num_cached_iterations, kernel[std::min(i, kernel.size() - 1)], level[std::min(i, level.size() - 1)], dimension[std::min(i, dimension.size() - 1)]));
        }
        return results;
    }
    const std::vector<std::string> num_extra_cached_iterations(const std::vector<unsigned int> &kernel,
                                                               const std::vector<LEVEL> &level,
                                                               const std::vector<dimension_t> &dimension) const {
        std::vector<std::string> results;
        if (kernel.empty() || level.empty() || dimension.empty()) return results;
        for (size_t i = 0; i < std::max(kernel.size(), std::max(level.size(), dimension.size())); ++i) {
            results.push_back(resolve_helper(_num_extra_cached_iterations, kernel[std::min(i, kernel.size() - 1)], level[std::min(i, level.size() - 1)], dimension[std::min(i, dimension.size() - 1)]));
        }
        return results;
    }
    const std::vector<std::string> num_extra_elements(const std::vector<unsigned int> &kernel,
                                                      const std::vector<LEVEL> &level,
                                                      const std::vector<dimension_t> &dimension) const {
        std::vector<std::string> results;
        if (kernel.empty() || level.empty() || dimension.empty()) return results;
        for (size_t i = 0; i < std::max(kernel.size(), std::max(level.size(), dimension.size())); ++i) {
            results.push_back(resolve_helper(_num_extra_elements, kernel[std::min(i, kernel.size() - 1)], level[std::min(i, level.size() - 1)], dimension[std::min(i, dimension.size() - 1)]));
        }
        return results;
    }
    const std::vector<std::string> num_processed_elements(const std::vector<unsigned int> &kernel,
                                                          const std::vector<LEVEL> &level,
                                                          const std::vector<dimension_t> &dimension) const {
        std::vector<std::string> results;
        if (kernel.empty() || level.empty() || dimension.empty()) return results;
        for (size_t i = 0; i < std::max(kernel.size(), std::max(level.size(), dimension.size())); ++i) {
            results.push_back(resolve_helper(_num_processed_elements, kernel[std::min(i, kernel.size() - 1)], level[std::min(i, level.size() - 1)], dimension[std::min(i, dimension.size() - 1)]));
        }
        return results;
    }
    const std::vector<std::string> incomplete_cb_size(const std::vector<unsigned int> &kernel,
                                                      const std::vector<LEVEL> &level,
                                                      const std::vector<dimension_t> &dimension,
                                                      const std::vector<LEVEL> &first_complete_level) const {
        std::vector<std::string> results;
        if (kernel.empty() || level.empty() || dimension.empty() || first_complete_level.empty()) return results;
        for (size_t i = 0; i < std::max(kernel.size(), std::max(level.size(), std::max(dimension.size(), first_complete_level.size()))); ++i) {
            results.push_back(resolve_helper(_incomplete_cb_size, kernel[std::min(i, kernel.size() - 1)], level[std::min(i, level.size() - 1)], dimension[std::min(i, dimension.size() - 1)], first_complete_level[std::min(i, first_complete_level.size() - 1)]));
        }
        return results;
    }
    const std::vector<std::string> num_steps_in_incomplete_cb(const std::vector<unsigned int> &kernel,
                                                              const std::vector<LEVEL> &level,
                                                              const std::vector<dimension_t> &dimension,
                                                              const std::vector<LEVEL> &first_complete_level) const {
        std::vector<std::string> results;
        if (kernel.empty() || level.empty() || dimension.empty() || first_complete_level.empty()) return results;
        for (size_t i = 0; i < std::max(kernel.size(), std::max(level.size(), std::max(dimension.size(), first_complete_level.size()))); ++i) {
            results.push_back(resolve_helper(_num_steps_in_incomplete_cb, kernel[std::min(i, kernel.size() - 1)], level[std::min(i, level.size() - 1)], dimension[std::min(i, dimension.size() - 1)], first_complete_level[std::min(i, first_complete_level.size() - 1)]));
        }
        return results;
    }
    const std::vector<std::string> num_cached_iterations_in_incomplete_cb(const std::vector<unsigned int> &kernel,
                                                                          const std::vector<LEVEL> &level,
                                                                          const std::vector<dimension_t> &dimension,
                                                                          const std::vector<LEVEL> &first_complete_level) const {
        std::vector<std::string> results;
        if (kernel.empty() || level.empty() || dimension.empty() || first_complete_level.empty()) return results;
        for (size_t i = 0; i < std::max(kernel.size(), std::max(level.size(), std::max(dimension.size(), first_complete_level.size()))); ++i) {
            results.push_back(resolve_helper(_num_cached_iterations_in_incomplete_cb, kernel[std::min(i, kernel.size() - 1)], level[std::min(i, level.size() - 1)], dimension[std::min(i, dimension.size() - 1)], first_complete_level[std::min(i, first_complete_level.size() - 1)]));
        }
        return results;
    }
    const std::vector<std::string> num_extra_cached_iterations_in_incomplete_cb(const std::vector<unsigned int> &kernel,
                                                                                const std::vector<LEVEL> &level,
                                                                                const std::vector<dimension_t> &dimension,
                                                                                const std::vector<LEVEL> &first_complete_level) const {
        std::vector<std::string> results;
        if (kernel.empty() || level.empty() || dimension.empty() || first_complete_level.empty()) return results;
        for (size_t i = 0; i < std::max(kernel.size(), std::max(level.size(), std::max(dimension.size(), first_complete_level.size()))); ++i) {
            results.push_back(resolve_helper(_num_extra_cached_iterations_in_incomplete_cb, kernel[std::min(i, kernel.size() - 1)], level[std::min(i, level.size() - 1)], dimension[std::min(i, dimension.size() - 1)], first_complete_level[std::min(i, first_complete_level.size() - 1)]));
        }
        return results;
    }
    const std::vector<std::string> num_extra_elements_in_incomplete_cb(const std::vector<unsigned int> &kernel,
                                                                       const std::vector<LEVEL> &level,
                                                                       const std::vector<dimension_t> &dimension,
                                                                       const std::vector<LEVEL> &first_complete_level) const {
        std::vector<std::string> results;
        if (kernel.empty() || level.empty() || dimension.empty() || first_complete_level.empty()) return results;
        for (size_t i = 0; i < std::max(kernel.size(), std::max(level.size(), std::max(dimension.size(), first_complete_level.size()))); ++i) {
            results.push_back(resolve_helper(_num_extra_elements_in_incomplete_cb, kernel[std::min(i, kernel.size() - 1)], level[std::min(i, level.size() - 1)], dimension[std::min(i, dimension.size() - 1)], first_complete_level[std::min(i, first_complete_level.size() - 1)]));
        }
        return results;
    }
    const std::vector<std::string> num_processed_elements_in_incomplete_cb(const std::vector<unsigned int> &kernel,
                                                                           const std::vector<LEVEL> &level,
                                                                           const std::vector<dimension_t> &dimension,
                                                                           const std::vector<LEVEL> &first_complete_level) const {
        std::vector<std::string> results;
        if (kernel.empty() || level.empty() || dimension.empty() || first_complete_level.empty()) return results;
        for (size_t i = 0; i < std::max(kernel.size(), std::max(level.size(), std::max(dimension.size(), first_complete_level.size()))); ++i) {
            results.push_back(resolve_helper(_num_processed_elements_in_incomplete_cb, kernel[std::min(i, kernel.size() - 1)], level[std::min(i, level.size() - 1)], dimension[std::min(i, dimension.size() - 1)], first_complete_level[std::min(i, first_complete_level.size() - 1)]));
        }
        return results;
    }
    const std::vector<std::string> index_conversion(const std::vector<unsigned int> &kernel,
                                                    const std::vector<LEVEL> &level,
                                                    const std::vector<dimension_t> &dimension,
                                                    const std::vector<std::string> &index,
                                                    const std::vector<char*> phase) const {
        std::vector<std::string> results;
        if (kernel.empty() || level.empty() || dimension.empty() || index.empty() || phase.empty()) return results;
        for (size_t i = 0; i < std::max(kernel.size(), std::max(level.size(), std::max(dimension.size(), std::max(index.size(), phase.size())))); ++i) {
            results.push_back(index_conversion(kernel[std::min(i, kernel.size() - 1)], level[std::min(i, level.size() - 1)], dimension[std::min(i, dimension.size() - 1)], index[std::min(i, index.size() - 1)], phase[std::min(i, phase.size() - 1)]));
        }
        return results;
    }
    const std::vector<std::string> fu_index_conversion(const std::vector<unsigned int> &kernel,
                                                    const std::vector<LEVEL> &level,
                                                    const std::vector<dimension_t> &dimension,
                                                    const std::vector<std::string> &index,
                                                    const std::vector<char*> phase) const {
        std::vector<std::string> results;
        if (kernel.empty() || level.empty() || dimension.empty() || index.empty() || phase.empty()) return results;
        for (size_t i = 0; i < std::max(kernel.size(), std::max(level.size(), std::max(dimension.size(), std::max(index.size(), phase.size())))); ++i) {
            results.push_back(fu_index_conversion(kernel[std::min(i, kernel.size() - 1)], level[std::min(i, level.size() - 1)], dimension[std::min(i, dimension.size() - 1)], index[std::min(i, index.size() - 1)], phase[std::min(i, phase.size() - 1)]));
        }
        return results;
    }
    const std::vector<std::string> iteration_to_index_conversion(const std::vector<unsigned int> &kernel,
                                                                 const std::vector<LEVEL> &level,
                                                                 const std::vector<dimension_t> &dimension,
                                                                 const std::vector<std::string> &index) const {
        std::vector<std::string> results;
        if (kernel.empty() || level.empty() || dimension.empty() || index.empty()) return results;
        for (size_t i = 0; i < std::max(kernel.size(), std::max(level.size(), std::max(dimension.size(), index.size()))); ++i) {
            results.push_back(iteration_to_index_conversion(kernel[std::min(i, kernel.size() - 1)], level[std::min(i, level.size() - 1)], dimension[std::min(i, dimension.size() - 1)], index[std::min(i, index.size() - 1)]));
        }
        return results;
    }
    const std::vector<std::string> flat_wi_id(const std::vector<unsigned int> &kernel,
                                              const std::vector<LEVEL> &level) const {
        std::vector<std::string> results;
        if (kernel.empty() || level.empty()) return results;
        for (size_t i = 0; i < std::max(kernel.size(), level.size()); ++i) {
            results.push_back(resolve_helper(_flat_wi_id, kernel[std::min(i, kernel.size() - 1)], level[std::min(i, level.size() - 1)]));
        }
        return results;
    }
    const std::vector<std::string> flat_num_wi(const std::vector<unsigned int> &kernel,
                                               const std::vector<LEVEL> &level) const {
        std::vector<std::string> results;
        if (kernel.empty() || level.empty()) return results;
        for (size_t i = 0; i < std::max(kernel.size(), level.size()); ++i) {
            results.push_back(resolve_helper(_flat_num_wi, kernel[std::min(i, kernel.size() - 1)], level[std::min(i, level.size() - 1)]));
        }
        return results;
    }

    std::string cb_size(unsigned int kernel, LEVEL level, const dimension_t &dimension, const char *phase) const {
        if (phase == nullptr) {
            return cb_size(kernel, level, dimension);
        } else {
            for (LEVEL p_level : parent_levels(level, true)) {
                if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(p_level, dimension)] == 3) {
                    if (p_level == level) {
                        return stringf(
                                "((%s < (%s + %s - 1) / %s) * ("
                                "(((%s - %s * %s) <= %s) * (%s - %s * %s)) + "
                                "(((%s - %s * %s) > %s) * %s)))",
                                fu_id(kernel, parent_level(level), dimension),
                                num_extra_elements(kernel, level, dimension, phase),
                                num_fu(kernel, level, dimension),
                                num_fu(kernel, level, dimension),

                                num_extra_elements(kernel, level, dimension, phase),
                                fu_id(kernel, parent_level(level), dimension),
                                num_fu(kernel, level, dimension),
                                num_fu(kernel, level, dimension),
                                num_extra_elements(kernel, level, dimension, phase),
                                fu_id(kernel, parent_level(level), dimension),
                                num_fu(kernel, level, dimension),

                                num_extra_elements(kernel, level, dimension, phase),
                                fu_id(kernel, parent_level(level), dimension),
                                num_fu(kernel, level, dimension),
                                num_fu(kernel, level, dimension),
                                num_fu(kernel, level, dimension)
                        );
                    } else {
                        return stringf("1");
                    }
                }
            }
            if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(level, dimension)] == 2) {
                for (LEVEL first_complete_level : parent_levels(level, false, ORDER::ASCENDING)) {
                    if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(first_complete_level, dimension)] == 1) {
                        return incomplete_cb_size(kernel, level, dimension, first_complete_level);
                    }
                }
            }
            return cb_size(kernel, level, dimension);
        }
        exit(1);
    }

    std::string num_steps(unsigned int kernel, LEVEL level, const dimension_t &dimension, const char *phase) const {
        if (phase == nullptr) {
            return num_steps(kernel, level, dimension);
        } else {
            for (LEVEL p_level : parent_levels(level, false)) {
                if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(p_level, dimension)] == 3) {
                    return "1";
                }
            }
            if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dimension)] == 2) {
                for (LEVEL first_complete_level : parent_levels(parent_level(level), false, ORDER::ASCENDING)) {
                    if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(first_complete_level, dimension)] == 1) {
                        return num_steps_in_incomplete_cb(kernel, level, dimension, first_complete_level);
                    }
                }
            }
            return num_steps(kernel, level, dimension);
        }
        exit(1);
    }

    std::string num_cached_iterations(unsigned int kernel, LEVEL level, const dimension_t &dimension, const char *phase) const {
        if (phase == nullptr) {
            return num_cached_iterations(kernel, level, dimension);
        } else {
            for (LEVEL p_level : parent_levels(level, false)) {
                if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(p_level, dimension)] == 3) {
                    return "1";
                }
            }
            if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(level, dimension)] == 2) {
                for (LEVEL first_complete_level : parent_levels(level, false, ORDER::ASCENDING)) {
                    if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(first_complete_level, dimension)] == 1) {
                        return num_cached_iterations_in_incomplete_cb(kernel, level, dimension, first_complete_level);
                    }
                }
            }
            return num_cached_iterations(kernel, level, dimension);
        }
        exit(1);
    }

    std::string num_extra_cached_iterations(unsigned int kernel, LEVEL level, const dimension_t &dimension, const char *phase) const {
        if (phase == nullptr) {
            return num_extra_cached_iterations(kernel, level, dimension);
        } else {
            for (LEVEL p_level : parent_levels(level, false)) {
                if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(p_level, dimension)] == 3) {
                    return "0";
                }
            }
            if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dimension)] == 2) {
                for (LEVEL first_complete_level : parent_levels(parent_level(level), false, ORDER::ASCENDING)) {
                    if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(first_complete_level, dimension)] == 1) {
                        return num_extra_cached_iterations_in_incomplete_cb(kernel, level, dimension,
                                                                            first_complete_level);
                    }
                }
            }
            return num_extra_cached_iterations(kernel, level, dimension);
        }
        exit(1);
    }

    std::string num_extra_elements(unsigned int kernel, LEVEL level, const dimension_t &dimension, const char *phase) const {
        if (phase == nullptr) {
            return num_extra_elements(kernel, level, dimension);
        } else {
            for (LEVEL p_level : parent_levels(level, false)) {
                if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(p_level, dimension)] == 3) {
                    return "0";
                }
            }
            if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(parent_level(level), dimension)] == 2) {
                for (LEVEL first_complete_level : parent_levels(parent_level(level), false, ORDER::ASCENDING)) {
                    if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(first_complete_level, dimension)] == 1) {
                        return num_extra_elements_in_incomplete_cb(kernel, level, dimension, first_complete_level);
                    }
                }
            }
            return num_extra_elements(kernel, level, dimension);
        }
        exit(1);
    }

    std::string num_processed_elements(unsigned int kernel, LEVEL level, const dimension_t &dimension, const char *phase) const {
        if (phase == nullptr) {
            return num_processed_elements(kernel, level, dimension);
        } else {
            for (LEVEL p_level : parent_levels(level, true)) {
                if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(p_level, dimension)] == 3) {
                    if (level == LEVEL::PRIVATE) {
                        return "1";
                    } else {
                        return stringf(
                                "((%s < (%s + %s - 1) / %s) * ("
                                "(((%s - %s * %s) <= %s) * (%s - %s * %s)) + "
                                "(((%s - %s * %s) > %s) * %s)))",
                                fu_id(kernel, parent_level(level), dimension),
                                num_extra_elements(kernel, level, dimension, phase),
                                num_fu(kernel, level, dimension),
                                num_fu(kernel, level, dimension),

                                num_extra_elements(kernel, level, dimension, phase),
                                fu_id(kernel, parent_level(level), dimension),
                                num_fu(kernel, level, dimension),
                                num_fu(kernel, level, dimension),
                                num_extra_elements(kernel, level, dimension, phase),
                                fu_id(kernel, parent_level(level), dimension),
                                num_fu(kernel, level, dimension),

                                num_extra_elements(kernel, level, dimension, phase),
                                fu_id(kernel, parent_level(level), dimension),
                                num_fu(kernel, level, dimension),
                                num_fu(kernel, level, dimension),
                                num_fu(kernel, level, dimension)
                        );
                    }
                }
            }
            if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(level, dimension)] == 2) {
                for (LEVEL first_complete_level : parent_levels(level, false, ORDER::ASCENDING)) {
                    if (phase[CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(first_complete_level, dimension)] == 1) {
                        return num_processed_elements_in_incomplete_cb(kernel, level, dimension, first_complete_level);
                    }
                }
            }
            return num_processed_elements(kernel, level, dimension);
        }
        exit(1);
    }

    std::vector<std::string> cb_size(const std::vector<unsigned int> &kernel, const std::vector<LEVEL> &level, const std::vector<dimension_t> &dimension, const std::vector<char *> &phase) const {
        std::vector<std::string> results;
        if (kernel.empty() || level.empty() || dimension.empty() || phase.empty()) return results;
        for (size_t i = 0; i < std::max(kernel.size(), std::max(level.size(), std::max(dimension.size(), phase.size()))); ++i) {
            results.push_back(cb_size(kernel[std::min(i, kernel.size() - 1)], level[std::min(i, level.size() - 1)], dimension[std::min(i, dimension.size() - 1)], phase[std::min(i, phase.size() - 1)]));
        }
        return results;
    }

    std::vector<std::string> num_steps(const std::vector<unsigned int> &kernel, const std::vector<LEVEL> &level, const std::vector<dimension_t> &dimension, const std::vector<char *> &phase) const {
        std::vector<std::string> results;
        if (kernel.empty() || level.empty() || dimension.empty() || phase.empty()) return results;
        for (size_t i = 0; i < std::max(kernel.size(), std::max(level.size(), std::max(dimension.size(), phase.size()))); ++i) {
            results.push_back(num_steps(kernel[std::min(i, kernel.size() - 1)], level[std::min(i, level.size() - 1)], dimension[std::min(i, dimension.size() - 1)], phase[std::min(i, phase.size() - 1)]));
        }
        return results;
    }

    std::vector<std::string> num_cached_iterations(const std::vector<unsigned int> &kernel, const std::vector<LEVEL> &level, const std::vector<dimension_t> &dimension, const std::vector<char *> &phase) const {
        std::vector<std::string> results;
        if (kernel.empty() || level.empty() || dimension.empty() || phase.empty()) return results;
        for (size_t i = 0; i < std::max(kernel.size(), std::max(level.size(), std::max(dimension.size(), phase.size()))); ++i) {
            results.push_back(num_cached_iterations(kernel[std::min(i, kernel.size() - 1)], level[std::min(i, level.size() - 1)], dimension[std::min(i, dimension.size() - 1)], phase[std::min(i, phase.size() - 1)]));
        }
        return results;
    }

    std::vector<std::string> num_extra_cached_iterations(const std::vector<unsigned int> &kernel, const std::vector<LEVEL> &level, const std::vector<dimension_t> &dimension, const std::vector<char *> &phase) const {
        std::vector<std::string> results;
        if (kernel.empty() || level.empty() || dimension.empty() || phase.empty()) return results;
        for (size_t i = 0; i < std::max(kernel.size(), std::max(level.size(), std::max(dimension.size(), phase.size()))); ++i) {
            results.push_back(num_extra_cached_iterations(kernel[std::min(i, kernel.size() - 1)], level[std::min(i, level.size() - 1)], dimension[std::min(i, dimension.size() - 1)], phase[std::min(i, phase.size() - 1)]));
        }
        return results;
    }

    std::vector<std::string> num_extra_elements(const std::vector<unsigned int> &kernel, const std::vector<LEVEL> &level, const std::vector<dimension_t> &dimension, const std::vector<char *> &phase) const {
        std::vector<std::string> results;
        if (kernel.empty() || level.empty() || dimension.empty() || phase.empty()) return results;
        for (size_t i = 0; i < std::max(kernel.size(), std::max(level.size(), std::max(dimension.size(), phase.size()))); ++i) {
            results.push_back(num_extra_elements(kernel[std::min(i, kernel.size() - 1)], level[std::min(i, level.size() - 1)], dimension[std::min(i, dimension.size() - 1)], phase[std::min(i, phase.size() - 1)]));
        }
        return results;
    }

    std::vector<std::string> num_processed_elements(const std::vector<unsigned int> &kernel, const std::vector<LEVEL> &level, const std::vector<dimension_t> &dimension, const std::vector<char *> &phase) const {
        std::vector<std::string> results;
        if (kernel.empty() || level.empty() || dimension.empty() || phase.empty()) return results;
        for (size_t i = 0; i < std::max(kernel.size(), std::max(level.size(), std::max(dimension.size(), phase.size()))); ++i) {
            results.push_back(num_processed_elements(kernel[std::min(i, kernel.size() - 1)], level[std::min(i, level.size() - 1)], dimension[std::min(i, dimension.size() - 1)], phase[std::min(i, phase.size() - 1)]));
        }
        return results;
    }

    std::string definitions(unsigned int kernel) const {
        std::stringstream ss;

        bool first_dim = true;
        for (DIM_TYPE dim_type : {DIM_TYPE::L, DIM_TYPE::R}) {
            for (unsigned int dim_nr = 1; dim_nr <= (dim_type == DIM_TYPE::L ? L_DIMS : R_DIMS); ++dim_nr) {
                if (!first_dim) {
                    ss << std::endl;
                    ss << std::endl;
                }
                ss << stringf("// -------------------- %c_%d --------------------", dim_type, dim_nr).c_str() << std::endl;
                ss << std::endl;
                ss << "// functional unit ids" << std::endl;
                for (LEVEL level : {LEVEL::GLOBAL, LEVEL::LOCAL, LEVEL::PRIVATE}) {
                    ss << resolve_helper(_def_fu_id, kernel, level, {dim_type, dim_nr}) << std::endl;
                }
                ss << std::endl;
                ss << "// number of functional units" << std::endl;
                for (LEVEL level : {LEVEL::GLOBAL, LEVEL::LOCAL, LEVEL::PRIVATE}) {
                    ss << resolve_helper(_def_num_fu, kernel, level, {dim_type, dim_nr}) << std::endl;
                }
                ss << std::endl;
                ss << "// cache block sizes" << std::endl;
                for (LEVEL level : {LEVEL::GLOBAL, LEVEL::LOCAL, LEVEL::PRIVATE}) {
                    if (kernel == 2 && dim_type == DIM_TYPE::R && level == LEVEL::GLOBAL) {
                        if (!_runtime_inputs) {
                            ss << stringf("#if NUM_WG_R_%d * NUM_WI_R_%d > INPUT_SIZE_R_%d", dim_nr, dim_nr, dim_nr).c_str() << std::endl;
                            ss << stringf("#define %s ((INPUT_SIZE_R_%d + NUM_WI_R_%d - 1) / NUM_WI_R_%d)", cb_size(kernel, level, {dim_type, dim_nr}), dim_nr, dim_nr, dim_nr).c_str() << std::endl;
                            ss << "#else" << std::endl;
                            ss << stringf("#define %s NUM_WG_R_%d", cb_size(kernel, level, {dim_type, dim_nr}), dim_nr).c_str() << std::endl;
                            ss << "#endif" << std::endl;
                        }
                    } else {
                        ss << resolve_helper(_def_cb_size, kernel, level, {dim_type, dim_nr}) << std::endl;
                    }
                }
                ss << std::endl;
                ss << "// number of steps per FU" << std::endl;
                for (LEVEL level : {LEVEL::GLOBAL, LEVEL::LOCAL, LEVEL::PRIVATE}) {
                    ss << resolve_helper(_def_num_steps, kernel, level, {dim_type, dim_nr}) << std::endl;
                }
                ss << std::endl;
                ss << "// number of cached iterations per FU" << std::endl;
                for (LEVEL level : {LEVEL::GLOBAL, LEVEL::LOCAL, LEVEL::PRIVATE}) {
                    ss << resolve_helper(_def_num_cached_iterations, kernel, level, {dim_type, dim_nr}) << std::endl;
                }
                ss << std::endl;
                ss << "// number of extra cached iterations per FU" << std::endl;
                for (LEVEL level : {LEVEL::GLOBAL, LEVEL::LOCAL, LEVEL::PRIVATE}) {
                    ss << resolve_helper(_def_num_extra_cached_iterations, kernel, level, {dim_type, dim_nr}) << std::endl;
                }
                ss << std::endl;
                ss << "// number of extra elements" << std::endl;
                for (LEVEL level : {LEVEL::GLOBAL, LEVEL::LOCAL, LEVEL::PRIVATE}) {
                    ss << resolve_helper(_def_num_extra_elements, kernel, level, {dim_type, dim_nr}) << std::endl;
                }
                ss << std::endl;
                ss << "// number of processed elements" << std::endl;
                for (LEVEL level : {LEVEL::GLOBAL, LEVEL::LOCAL, LEVEL::PRIVATE}) {
                    ss << resolve_helper(_def_num_processed_elements, kernel, level, {dim_type, dim_nr}) << std::endl;
                }
                ss << std::endl;
                ss << "// incomplete cache block sizes" << std::endl;
                for (LEVEL cb_size_level : {LEVEL::PRIVATE, LEVEL::LOCAL, LEVEL::GLOBAL}) {
                    for (LEVEL first_complete_level : parent_levels(cb_size_level, false, ORDER::ASCENDING)) {
                        ss << resolve_helper(_def_incomplete_cb_size, kernel, cb_size_level, {dim_type, dim_nr}, first_complete_level) << std::endl;
                    }
                }
                ss << std::endl;
                ss << "// number of steps per FU in incomplete cache blocks" << std::endl;
                for (LEVEL level : {LEVEL::PRIVATE, LEVEL::LOCAL}) {
                    for (LEVEL first_complete_level : parent_levels(parent_level(level), false, ORDER::ASCENDING)) {
                        ss << resolve_helper(_def_num_steps_in_incomplete_cb, kernel, level, {dim_type, dim_nr}, first_complete_level) << std::endl;
                    }
                }
                ss << std::endl;
                ss << "// number of cached iterations per FU in incomplete cache blocks" << std::endl;
                for (LEVEL level : {LEVEL::PRIVATE, LEVEL::LOCAL}) {
                    for (LEVEL first_complete_level : parent_levels(level, false, ORDER::ASCENDING)) {
                        ss << resolve_helper(_def_num_cached_iterations_in_incomplete_cb, kernel, level, {dim_type, dim_nr}, first_complete_level) << std::endl;
                    }
                }
                ss << std::endl;
                ss << "// number of extra cached iterations per FU in incomplete cache blocks" << std::endl;
                for (LEVEL level : {LEVEL::PRIVATE, LEVEL::LOCAL}) {
                    for (LEVEL first_complete_level : parent_levels(parent_level(level), false, ORDER::ASCENDING)) {
                        ss << stringf("#if %s > 0", num_cached_iterations(kernel, level, {dim_type, dim_nr})).c_str() << std::endl;
                        ss << resolve_helper(_def_num_extra_cached_iterations_in_incomplete_cb, kernel, level, {dim_type, dim_nr}, first_complete_level) << std::endl;
                        ss << "#else" << std::endl;
                        ss << stringf("#define %s 0", num_extra_cached_iterations_in_incomplete_cb(kernel, level, {dim_type, dim_nr}, first_complete_level)).c_str() << std::endl;
                        ss << "#endif" << std::endl;
                    }
                }
                ss << std::endl;
                ss << "// number of extra elements in incomplete cache blocks" << std::endl;
                for (LEVEL level : {LEVEL::PRIVATE, LEVEL::LOCAL}) {
                    for (LEVEL first_complete_level : parent_levels(parent_level(level), false, ORDER::ASCENDING)) {
                        ss << stringf("#if %s > 0", num_cached_iterations(kernel, level, {dim_type, dim_nr})).c_str() << std::endl;
                        ss << resolve_helper(_def_num_extra_elements_in_incomplete_cb, kernel, level, {dim_type, dim_nr}, first_complete_level) << std::endl;
                        ss << "#else" << std::endl;
                        ss << stringf("#define %s 0", num_extra_elements_in_incomplete_cb(kernel, level, {dim_type, dim_nr}, first_complete_level)).c_str() << std::endl;
                        ss << "#endif" << std::endl;
                    }
                }
                ss << std::endl;
                ss << "// number of processed elements in incomplete cache blocks" << std::endl;
                for (LEVEL level : {LEVEL::PRIVATE, LEVEL::LOCAL}) {
                    for (LEVEL first_complete_level : parent_levels(level, false, ORDER::ASCENDING)) {
                        ss << resolve_helper(_def_num_processed_elements_in_incomplete_cb, kernel, level, {dim_type, dim_nr}, first_complete_level) << std::endl;
                    }
                }
                ss << std::endl;
                ss << "// index conversion" << std::endl;
                for (LEVEL level : {LEVEL::LOCAL, LEVEL::PRIVATE}) {
                    ss << resolve_helper(_def_index_conversion, kernel, level, {dim_type, dim_nr}) << std::endl;
                }
                ss << std::endl;
                ss << "// index conversion in phase 3" << std::endl;
                for (LEVEL level : {LEVEL::LOCAL, LEVEL::PRIVATE}) {
                    for (bool my_level_in_phase3 : level == LEVEL::PRIVATE ? V(true, false) : V(true)) {
                        for (bool parent_level_complete : ((level == LEVEL::PRIVATE && my_level_in_phase3) ? V(true, false) : V(true))) {
                            ss << resolve_helper(_def_index_conversion_in_phase_3, kernel, level, {dim_type, dim_nr}, parent_level_complete, my_level_in_phase3) << std::endl;
                        }
                    }
                }
                ss << std::endl;
                ss << "#if defined(CACHE_CB_OFFSET) && CACHE_CB_OFFSET != 0" << std::endl;
                ss << "// index conversion per FU" << std::endl;
                for (LEVEL level : {LEVEL::LOCAL, LEVEL::PRIVATE}) {
                    ss << resolve_helper(_def_fu_index_conversion, kernel, level, {dim_type, dim_nr}) << std::endl;
                }
                ss << std::endl;
                ss << "// index conversion per FU in phase 3" << std::endl;
                for (LEVEL level : {LEVEL::LOCAL, LEVEL::PRIVATE}) {
                    for (bool my_level_in_phase3 : level == LEVEL::PRIVATE ? V(true, false) : V(true)) {
                        for (bool parent_level_complete : ((level == LEVEL::PRIVATE && my_level_in_phase3) ? V(true, false) : V(true))) {
                            ss << resolve_helper(_def_fu_index_conversion_in_phase_3, kernel, level, {dim_type, dim_nr}, parent_level_complete, my_level_in_phase3) << std::endl;
                        }
                    }
                }
                ss << std::endl;
                ss << "#else" << std::endl;
                ss << "// index conversion per FU" << std::endl;
                for (LEVEL level : {LEVEL::LOCAL, LEVEL::PRIVATE}) {
                    ss << "#define " << stringf(resolve_helper(_fu_index_conversion, kernel, level, {dim_type, dim_nr}), "i")
                       << " " << stringf(resolve_helper(_index_conversion, kernel, level, {dim_type, dim_nr}), "i") << std::endl;
                }
                ss << std::endl;
                ss << "// index conversion per FU in phase 3" << std::endl;
                for (LEVEL level : {LEVEL::LOCAL, LEVEL::PRIVATE}) {
                    for (bool my_level_in_phase3 : level == LEVEL::PRIVATE ? V(true, false) : V(true)) {
                        for (bool parent_level_complete : ((level == LEVEL::PRIVATE && my_level_in_phase3) ? V(true, false) : V(true))) {
                            ss << "#define " << stringf(resolve_helper(_fu_index_conversion_in_phase3, kernel, level, {dim_type, dim_nr}, parent_level_complete, my_level_in_phase3), "i")
                               << " " << stringf(resolve_helper(_index_conversion_in_phase3, kernel, level, {dim_type, dim_nr}, parent_level_complete, my_level_in_phase3), "i") << std::endl;
                        }
                    }
                }
                ss << "#endif" << std::endl;
                ss << std::endl;
                ss << "// iteration to index conversion" << std::endl;
                for (LEVEL level : {LEVEL::LOCAL, LEVEL::PRIVATE}) {
                    ss << resolve_helper(_def_iteration_to_index_conversion, kernel, level, {dim_type, dim_nr}) << std::endl;
                }

                first_dim = false;
            }
        }

        ss << std::endl;
        ss << std::endl;
        ss << "// -------------------- combined over dimensions --------------------" << std::endl;
        ss << std::endl;
        ss << "// flat WI ids" << std::endl;
        for (LEVEL level : {LEVEL::GLOBAL, LEVEL::LOCAL, LEVEL::PRIVATE}) {
            ss << resolve_helper(_def_flat_wi_id, kernel, level) << std::endl;
        }
        ss << std::endl;
        ss << "// flat number of WIs" << std::endl;
        for (LEVEL level : {LEVEL::GLOBAL, LEVEL::LOCAL, LEVEL::PRIVATE}) {
            ss << resolve_helper(_def_flat_num_wi, kernel, level) << std::endl;
        }

        // erase last new line character
        return ss.str().erase(ss.str().length() - 1);
    }
private:
    const bool _runtime_inputs;

    // macro names for each kernel, level and dimension
    std::string _fu_id                                        [2][3][L_DIMS + R_DIMS];
    std::string _num_fu                                       [2][3][L_DIMS + R_DIMS];
    std::string _cb_size                                      [2][3][L_DIMS + R_DIMS];
    std::string _num_steps                                    [2][3][L_DIMS + R_DIMS];
    std::string _num_cached_iterations                        [2][3][L_DIMS + R_DIMS];
    std::string _num_extra_cached_iterations                  [2][3][L_DIMS + R_DIMS];
    std::string _num_extra_elements                           [2][3][L_DIMS + R_DIMS];
    std::string _incomplete_cb_size                           [2][3][L_DIMS + R_DIMS][3];
    std::string _num_steps_in_incomplete_cb                   [2][3][L_DIMS + R_DIMS][3];
    std::string _num_cached_iterations_in_incomplete_cb       [2][3][L_DIMS + R_DIMS][3];
    std::string _num_extra_cached_iterations_in_incomplete_cb [2][3][L_DIMS + R_DIMS][3];
    std::string _num_extra_elements_in_incomplete_cb          [2][3][L_DIMS + R_DIMS][3];
    std::string _index_conversion                             [2][3][L_DIMS + R_DIMS];
    std::string _index_conversion_in_phase3                   [2][3][L_DIMS + R_DIMS][2][2];
    std::string _fu_index_conversion                          [2][3][L_DIMS + R_DIMS];
    std::string _fu_index_conversion_in_phase3                [2][3][L_DIMS + R_DIMS][2][2];
    std::string _iteration_to_index_conversion                [2][3][L_DIMS + R_DIMS];
    std::string _num_processed_elements                       [2][3][L_DIMS + R_DIMS];
    std::string _num_processed_elements_in_incomplete_cb      [2][3][L_DIMS + R_DIMS][3];
    // macro names for each kernel and level
    std::string _flat_wi_id [2][3];
    std::string _flat_num_wi[2][3];

    // macro definitions for each kernel, level and dimension
    std::string _def_fu_id                                        [2][3][L_DIMS + R_DIMS];
    std::string _def_num_fu                                       [2][3][L_DIMS + R_DIMS];
    std::string _def_cb_size                                      [2][3][L_DIMS + R_DIMS];
    std::string _def_num_steps                                    [2][3][L_DIMS + R_DIMS];
    std::string _def_num_cached_iterations                        [2][3][L_DIMS + R_DIMS];
    std::string _def_num_extra_cached_iterations                  [2][3][L_DIMS + R_DIMS];
    std::string _def_num_extra_elements                           [2][3][L_DIMS + R_DIMS];
    std::string _def_incomplete_cb_size                           [2][3][L_DIMS + R_DIMS][3];
    std::string _def_num_steps_in_incomplete_cb                   [2][3][L_DIMS + R_DIMS][3];
    std::string _def_num_cached_iterations_in_incomplete_cb       [2][3][L_DIMS + R_DIMS][3];
    std::string _def_num_extra_cached_iterations_in_incomplete_cb [2][3][L_DIMS + R_DIMS][3];
    std::string _def_num_extra_elements_in_incomplete_cb          [2][3][L_DIMS + R_DIMS][3];
    std::string _def_index_conversion                             [2][3][L_DIMS + R_DIMS];
    std::string _def_index_conversion_in_phase_3                  [2][3][L_DIMS + R_DIMS][2][2];
    std::string _def_fu_index_conversion                          [2][3][L_DIMS + R_DIMS];
    std::string _def_fu_index_conversion_in_phase_3               [2][3][L_DIMS + R_DIMS][2][2];
    std::string _def_iteration_to_index_conversion                [2][3][L_DIMS + R_DIMS];
    std::string _def_num_processed_elements                       [2][3][L_DIMS + R_DIMS];
    std::string _def_num_processed_elements_in_incomplete_cb      [2][3][L_DIMS + R_DIMS][3];
    // macro definitions for each kernel and level
    std::string _def_flat_wi_id [2][3];
    std::string _def_flat_num_wi[2][3];

    std::string& resolve_helper(std::string (&data)[2][3][L_DIMS + R_DIMS],
                                unsigned int kernel, LEVEL level, dimension_t dimension) {
        return data[kernel - 1][LEVEL_ID(level)][CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(dimension)];
    }
    const std::string& resolve_helper(const std::string (&data)[2][3][L_DIMS + R_DIMS],
                                      unsigned int kernel, LEVEL level, dimension_t dimension) const {
        return data[kernel - 1][LEVEL_ID(level)][CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(dimension)];
    }

    std::string& resolve_helper(std::string (&data)[2][3], unsigned int kernel, LEVEL level) {
        return data[kernel - 1][LEVEL_ID(level)];
    }
    const std::string& resolve_helper(const std::string (&data)[2][3], unsigned int kernel, LEVEL level) const {
        return data[kernel - 1][LEVEL_ID(level)];
    }

    std::string& resolve_helper(std::string (&data)[2][3][L_DIMS + R_DIMS][3],
                                unsigned int kernel, LEVEL level, dimension_t dimension, LEVEL first_complete_level) {
        return data[kernel - 1][LEVEL_ID(level)][CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(dimension)][LEVEL_ID(first_complete_level)];
    }
    const std::string& resolve_helper(const std::string (&data)[2][3][L_DIMS + R_DIMS][3],
                                      unsigned int kernel, LEVEL level, dimension_t dimension, LEVEL first_complete_level) const {
        return data[kernel - 1][LEVEL_ID(level)][CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(dimension)][LEVEL_ID(first_complete_level)];
    }

    std::string& resolve_helper(std::string (&data)[2][3][L_DIMS + R_DIMS][2][2],
                                unsigned int kernel, LEVEL level, dimension_t dimension, bool parent_level_complete, bool my_level_in_phase3) {
        return data[kernel - 1][LEVEL_ID(level)][CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(dimension)][parent_level_complete ? 0 : 1][my_level_in_phase3 ? 0 : 1];
    }
    const std::string& resolve_helper(const std::string (&data)[2][3][L_DIMS + R_DIMS][2][2],
                                      unsigned int kernel, LEVEL level, dimension_t dimension, bool parent_level_complete, bool my_level_in_phase3) const {
        return data[kernel - 1][LEVEL_ID(level)][CONTINUOUS_DIM_ID<L_DIMS, R_DIMS>(dimension)][parent_level_complete ? 0 : 1][my_level_in_phase3 ? 0 : 1];
    }
};

}
}

#endif //MD_BLAS_MACROS_HPP
