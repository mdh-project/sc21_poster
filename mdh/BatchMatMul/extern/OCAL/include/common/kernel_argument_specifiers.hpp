//
//  kernel_argument_specifiers_hpp.hpp
//  ocal
//
//  Created by Ari Rasch on 04.10.17.
//  Copyright © 2017 Ari Rasch. All rights reserved.
//

#ifndef kernel_argument_specifiers_hpp
#define kernel_argument_specifiers_hpp


namespace ocal
{

namespace common
{


template< typename T>
class scalar_class
{
public:
    scalar_class() = default; // make read class default constructable for md_hom wrapper
    scalar_class( const T& t ) : _t(t) {}

    auto& get()
    {
        return _t;
    }

    const auto& get() const
    {
        return _t;
    }

private:
    T _t;
};


template< typename T>
class read_class
{
public:
    read_class() {
        // make read class default constructable for md_hom wrapper
        _t = new T(0);
    }

    read_class( T& t )
            : _t( &t )
    {}

    auto& get()
    {
        return *_t;
    }

    const auto& get() const
    {
        return *_t;
    }

private:
    T* _t;
};


template< typename T>
class write_class
{
  public:
    write_class() {
        // make write class default constructable for md_hom wrapper
        _t = new T(0);
    }

    write_class( T& t )
      : _t( &t )
    {}

    auto& get()
    {
      return *_t;
    }

    const auto& get() const
    {
      return *_t;
    }

  private:
    T* _t;
};


template< typename T>
class read_write_class
{
public:
    read_write_class() {
        // make read_write class default constructable for md_hom wrapper
        _t = new T(0);
    }

    read_write_class( T& t )
            : _t( &t )
    {}

    auto& get()
    {
        return *_t;
    }

    const auto& get() const
    {
        return *_t;
    }

private:
    T* _t;
};
  

// factories
template< typename T >
auto scalar( const T& scalar )
{
  return scalar_class<T>( scalar );
}

template< typename T >
auto read( T& buffer ) // Note: buffers are assumed to not be r-values
{
  return read_class<T>( buffer );
}

template< typename T >
auto write( T& buffer ) // Note: buffers are assumed to not be r-values
{
  return write_class<T>( buffer );
}


template< typename T >
auto read_write( T& buffer ) // Note: buffers are assumed to not be r-values
{
  return read_write_class<T>( buffer );
}


} // namespace "common"

} // namespace "ocal"

#endif /* kernel_argument_specifiers_hpp */
