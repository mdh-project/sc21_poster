//
//  event_manager.hpp
//  ocal
//
//  Created by Ari Rasch on 03.10.17.
//  Copyright © 2017 Ari Rasch. All rights reserved.
//

#ifndef event_manager_hpp
#define event_manager_hpp

namespace ocal
{

namespace core
{

namespace common
{


template< typename T >
class event_manager
{
  public:
    using event_type = T;
    
    event_manager() = default;

  
    void set_event_device_buffer_of_device_is_read( const T& event, int unique_device_id )
    {
      _unique_device_id_to_read_events_on_device_buffer[ unique_device_id ].push_back( event ); // Note: "[...]" instead of ".at" to generate event vector when accessed the first time
    }


    void set_event_device_buffer_of_device_is_written( const T& event, int unique_device_id )
    {
      _unique_device_id_to_write_events_on_device_buffer[ unique_device_id ].push_back( event ); // Note: "[...]" instead of ".at" to generate event vector when accessed the first time
    }


    void set_event_host_memory_is_read( const T& event )
    {
      _read_events_on_host_memory.push_back( event );
    }


    void set_event_host_memory_is_written( const T& event )
    {
      _write_events_on_host_memory.push_back( event );
    }
  
  
    // waits on host thread until all events for device with id "unique_device_id" have finished
    virtual void sync_read_events_on_device_buffer( int unique_device_id ) // TODO: warum virtual (auch bei den Funktionen unten)
    {
      auto& events = _unique_device_id_to_read_events_on_device_buffer[ unique_device_id ]; // Note: "[...]" instead of ".at" to generate event vector when accessed the first time
      
      for( auto& event : events )
        event.wait();
      
      events.clear();
    }


    virtual void sync_write_events_on_device_buffer( int unique_device_id )
    {
      auto& events = _unique_device_id_to_write_events_on_device_buffer[ unique_device_id ]; // Note: "[...]" instead of ".at" to generate event vector when accessed the first time
      
      for( auto& event : events )
        event.wait();
      
      events.clear();
    }

  
    virtual void sync_read_events_on_host_memory()
    {
      auto& events = _read_events_on_host_memory;

      for( auto& event : events )
        event.wait();
      
      events.clear();
    }
  
  
    virtual void sync_write_events_on_host_memory()
    {
      auto& events = _write_events_on_host_memory;
      
      for( auto& event : events )
        event.wait();
      
      events.clear();
    }

  
  protected:
    std::unordered_map< int, std::vector< T > > _unique_device_id_to_read_events_on_device_buffer;
    std::unordered_map< int, std::vector< T > > _unique_device_id_to_write_events_on_device_buffer;
    std::vector< T >                            _read_events_on_host_memory;
    std::vector< T >                            _write_events_on_host_memory;
};


class event_wrapper
{
  public:
    enum STATUS {
        RUNNING,
        COMPLETE
    };
    virtual void wait() const = 0;
    virtual STATUS status() const = 0;
};


} // namespace "common"

} // namespace "core"

} // namespace "ocal"

#endif /* event_manager_hpp */
