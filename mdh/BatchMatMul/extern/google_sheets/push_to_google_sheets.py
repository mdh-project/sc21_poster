import argparse
import pickle
import os.path
from google.auth.transport.requests import Request
from googleapiclient.discovery import build

def main():
    parser = argparse.ArgumentParser(description='Push json object to Google Sheets API.')
    parser.add_argument('pickle',        type=str, nargs='?', help='Pickle file to use.')
    parser.add_argument('spreadsheetId', type=str, nargs='?', help='The spreadsheet to update.')
    parser.add_argument('sheetName',     type=str, nargs='?', help='The sheet to update.')
    parser.add_argument('entryId',       type=str, nargs='?', help='The entry id to update (is searched for in first column of sheet).')
    parser.add_argument('data',          type=str, nargs='?', help='The json data to push.')
    args = parser.parse_args()

    pickle_arg = args.pickle
    if pickle_arg is None:
        pickle_arg = os.environ['GSHEETS_PICKLE']
    spreadsheetId_arg = args.spreadsheetId
    if spreadsheetId_arg is None:
        spreadsheetId_arg = os.environ['GSHEETS_SPREADSHEETID']
    sheetName_arg = args.sheetName
    if sheetName_arg is None:
        sheetName_arg = os.environ['GSHEETS_SHEETNAME']
    entryId_arg = args.entryId
    if entryId_arg is None:
        entryId_arg = os.environ['GSHEETS_ENTRYID']
    data_arg = args.data
    if data_arg is None:
        data_arg = os.environ['GSHEETS_DATA']

    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists(pickle_arg):
        with open(pickle_arg, 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, refresh or fail.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            print('not yet authorized. execute "google_sheets_login.py".')
            exit(1)
        # Save the credentials for the next run
        with open(pickle_arg, 'wb') as token:
            pickle.dump(creds, token)

    # get sheets
    service = build('sheets', 'v4', credentials=creds)
    sheet = service.spreadsheets()

    # get entry ids
    result = sheet.values().get(spreadsheetId=spreadsheetId_arg, range='\'{0}\'!A:A'.format(sheetName_arg)).execute()
    values = result.get('values', [])

    # find row for entry id
    row = 1
    if values:
        for value in values:
            if len(value) > 0 and value[0] == entryId_arg:
                break
            row = row + 1
    if (row > len(values)):
        print('entry id "{}" not found'.format(entryId_arg))
        exit(1)

    # write json data to entry row
    values = [[data_arg]]
    body = {
        'values': values
    }
    result = service.spreadsheets().values().update(
        spreadsheetId=spreadsheetId_arg, range='\'{0}\'!B{1}:B{1}'.format(sheetName_arg, row),
        valueInputOption='RAW', body=body).execute()

if __name__ == '__main__':
    main()