import datetime
import sys
import csv

tuning_log_file_path = sys.argv[1]

tuning_start = None
data = None
time_to_reach = {}
percentages = [10, 5, 1, 0.1]
for percent in percentages:
    time_to_reach[percent] = None

with open(tuning_log_file_path, 'r') as tuning_log_file:
    tuning_log_reader = csv.reader(tuning_log_file, delimiter=';')
    next(tuning_log_reader, None)  # skip the headers
    for row in tuning_log_reader:
        timestamp = datetime.datetime.strptime(row[0], '%Y-%m-%d %H:%M:%S.%f')
        runtime = int(row[5])
        if data is None:
            tuning_start = timestamp
            data = [[0, runtime]]
        elif runtime < data[-1][1]:
            elapsed_ms = (timestamp - tuning_start).total_seconds()
            data.append([elapsed_ms, runtime])

for improvement in data:
    for percent in percentages:
        if not time_to_reach[percent] and (float(improvement[1]) / data[-1][1]) <= (1 + percent / 100.0):
            time_to_reach[percent] = improvement[0]

for percent in percentages:
    print("Time to get into {}% of best result: {}s".format(percent, time_to_reach[percent]))
