void calculate_gold(const std::string &data_dir, int input_size_l_1, int input_size_l_2, int input_size_l_3, int input_size_l_4, int input_size_r_1) {
    std::ofstream gold_file(data_dir + "/gold.tsv", std::ios::out | std::ios::trunc);
    gold_file << std::fixed << std::setprecision(0);
    for (size_t b_1 = 0; b_1 < input_size_l_1; ++b_1) {
        for (size_t b_2 = 0; b_2 < input_size_l_2; ++b_2) {
            for (size_t i = 0; i < input_size_l_3; ++i) {
                for (size_t j = 0; j < input_size_l_4; ++j) {
                    float acc = 0;
                    for (size_t k = 0; k < input_size_r_1; ++k) {
                        acc += a[b_1 * input_size_l_2 * input_size_l_3 * input_size_r_1 + b_2 * input_size_l_3 * input_size_r_1 + i * input_size_r_1 + k] *
                               b[b_1 * input_size_l_2 * input_size_r_1 * input_size_l_4 + b_2 * input_size_r_1 * input_size_l_4 + k * input_size_l_4 + j];
                    }
                    gold_file << acc << "\t";
                }
                gold_file << std::endl;
            }
            gold_file << std::endl;
        }
        gold_file << std::endl;
    }
    gold_file.close();
}