# Code Generation & Optimization for Deep-Learning Computations on GPUs via Multi-Dimensional Homorphisms

This artifact contains the workflow to reproduce the experiments presented on the poster *Code Generation & Optimization for Deep-Learning Computations on GPUs via Multi-Dimensional Homorphisms* under review at [The International Conference for High Performance Computing, Networking, Storage, and Analysis (SC)](https://sc21.supercomputing.org/submit/poster-submissions/research-poster-submissions/). The user is invited to perform the steps described below.

All experiments on our poster haven been conducted on NVIDIA Tesla V100-SXM2-16GB GPU.

In case of **any problems**, please feel free to **open an issue** to get in touch with the authors.

## Software Requirements

- a compiler supporting C++17 or higher
- [CMake 3.3 or higher](https://cmake.org/)
- [OpenTuner](http://opentuner.org/)
- [NVIDIA CUDA Toolkit](https://developer.nvidia.com/cuda-toolkit)
- [Tensor Comprehensions](https://facebookresearch.github.io/TensorComprehensions/)
- [Apache TVM](https://tvm.apache.org/)
- [TensorFlow](https://www.tensorflow.org/)
- [`tabulate` Python package](https://pypi.org/project/tabulate/)

## Workflow

The workflow of this artifact is divided into two main steps: **1) Building the artifact**, **2) Benchmarking MDH and its competitors**. After step 2), the benchmarking results will be printed to the screen and can also be found in file `results/summary.txt`. 

### 1) **Building the artifact**

   Clone the repository:
   
   `$ git clone https://gitlab.com/mdh-project/sc21_poster.git`
   
   Change into the artifact directory:
   
   `$ cd sc21_poster`
   
   Initialize the necessary environment variables:
   
   `$ source environment.sh`
   
   Compile artifact's source code:
   
   `$ ./build.sh`
   
   All arguments provided to script `build.sh` will be directly forwarded to CMake when building the binaries. For example, forwarding arguments can be required if some dependencies cannot be found automatically by CMake (e.g., when dependencies are not installed in their default locations).

### 2) **Benchmarking MDH and the competitors**

   The experiments are started by executing:

   `$ ./bench.sh`

### Optional: Auto-Tuning

We provide implementations that are optimized for our specific GPU (NVIDIA Tesla V100-SMX2-16GB). This may prevent the user from reproducing the results presented on the poster for other GPUs.

For other GPUs, auto-tuning needs to be re-run before step 2) via:

`$ ./tune.sh`

Note that this step may take a long time (~9 days), because for each routine both MDH and some of the competitors are auto-tuned for several hours. The user may edit the TUNING_TIME_IN_MINUTES variable in file tune.sh to decrease the overall auto-tuning time. Note that low tuning times might affect performance!